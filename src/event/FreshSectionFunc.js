import ContainerMapUtils from '../utils/ContainerMapUtils';

module.exports = {
    refreshSection: function(containerCode, policy) {
        if (containerCode instanceof Array) {
            containerCode.forEach(code => {
                let container = ContainerMapUtils.get(code);
                container.onChangeListener({policy});
            })
        } else {
            let container = ContainerMapUtils.get(containerCode);
            console.assert(container,"Container is undefined : "+containerCode)
            if(container){
                container.onChangeListener({policy});
            }
        }
    }
};