import CodeTableCache from '../utils/CodeTableCache';
import CodeTableAction from '../action/CodeTableAction';
import ConstantKey from '../constant/ConstantKey';
import {CodeTable} from 'rainbowui-desktop-core';

module.exports = {

    buildStandardCodeTable : function (orgCodeTable) {
    	if(Array.isArray(orgCodeTable)){
            return new CodeTable(orgCodeTable)
		}else{
    		return new CodeTable([])
		}
    },

	getCodeTable: function(codeTableId) {
        let result  = new CodeTableAction().getCodeTableById(codeTableId);
        return result;
    },

    getCodeTableByName: function(codeTableName, productId) {
        let result;
        if (productId) {
            result = new CodeTableAction().getCodeTableByNameProduct(codeTableName, productId);
            return result;
        }
        result = new CodeTableAction().getCodeTableByNameContext(codeTableName, ConstantKey.DEFAULT_CONTEXT);
        return result;
    },
    
	transformCodeTable: function(codeTableInfo,conditionFieldInfo){
		let codeTableData = [];
		if(typeof(codeTableInfo) === 'string'){
			codeTableData = new CodeTableAction().getCodeTableByNameContext(codeTableInfo, ConstantKey.DEFAULT_CONTEXT,conditionFieldInfo);
		}else{
			console.log("Current codeTableInfo error : "+codeTableInfo)
		}
		return new CodeTable(codeTableData);
	},

	makeUpCodeTable:function(codeTableName,conditionFieldArray){
		let codeTableData;
		codeTableData = new CodeTableAction().getCodeTableByNameContext(codeTableName, ConstantKey.DEFAULT_CONTEXT);
		let conditionFieldKeys = conditionFieldArray ? Object.keys(conditionFieldArray) : [];
		let newCodeTable = [];
		let isTrans,isKey;
		if(conditionFieldKeys){
			if(codeTableData&&codeTableData[0].ConditionFields){
			codeTableData.forEach(codeTableFilterMap =>{
				isTrans = false;
				let ConditionFieldList = codeTableFilterMap.ConditionFields;
				ConditionFieldList.find(ConditionFieldMap => {
					isKey = false;
				conditionFieldKeys.find(conditionFieldKey => {
					let conditionFieldKey_array = conditionFieldKey
					if(conditionFieldKey&&conditionFieldKey.indexOf("$") > -1){
						let conditionFieldKeyMap = conditionFieldKey.split("$");
						conditionFieldKey_array = conditionFieldKeyMap[0];
					}
        			if(ConditionFieldMap[conditionFieldKey_array] == conditionFieldArray[conditionFieldKey]){
           				isKey = true;
					}
				});
					if (isKey)isTrans = true;
					if(isTrans)return isTrans;
				});
				if(isTrans)newCodeTable.push(codeTableFilterMap);
			});
			}
		}
		return new CodeTable(newCodeTable);
	},
	
	getDataTableTargetByName(dataTableName,conditionInfo){
		let dataTableData = new CodeTableAction().getDataTableRecordListByName(dataTableName);
		let fieldKeys = conditionInfo ? Object.keys(conditionInfo) : [];
		let resultData=[];
		let isPush = false;
		if(dataTableData){
			dataTableData.find(dataTableTargetData=>{
				fieldKeys.find(fieldkeyMap=>{
					if(dataTableTargetData[fieldkeyMap] == conditionInfo[fieldkeyMap]){
						resultData.push(dataTableTargetData)
					}
				})
			})
		}
		return resultData;
	},

	generateConditionMap(conditionMapInfo){
		let conditionInfo={}
		if(conditionMapInfo){
			conditionInfo = conditionMapInfo;
		}
		UIC.dynamicSectionConditionMap = conditionInfo;
	}

	 // transformCodeTable: function(codeTableInfo,conditionFieldInfo){
	// 	let codeTableData = [];
	// 	if(typeof(codeTableInfo) === 'number'){
	// 		codeTableData = new CodeTableAction().getCodeTableById(codeTableInfo);
	// 	}else if(typeof(codeTableInfo) === 'string'){
	// 		codeTableData = new CodeTableAction().getCodeTableByNameContext(codeTableInfo, ConstantKey.DEFAULT_CONTEXT);
	// 	}
	// 	let newCodeTable = [];
	// 	let isTrans,isKey,isFields;
	// 	let conditionFieldKeys = conditionFieldInfo ? Object.keys(conditionFieldInfo) : [];
	// 	if(conditionFieldKeys.length>0){
	// 		isFields = 0;
	// 		codeTableData.forEach(codeTableFilterMap =>{
	// 			if(codeTableFilterMap&&codeTableFilterMap.ConditionFields&&codeTableFilterMap.ConditionFields.length > 1){
	// 				++isFields;
	// 			}
	// 		})
	// 		if(isFields!=0){
	// 		codeTableData.forEach(codeTableFilterMap =>{
	// 			isTrans = false;
	// 			let ConditionFieldList = codeTableFilterMap.ConditionFields;
	// 			ConditionFieldList.find(ConditionFieldMap => {
	// 				isKey = false;
	// 			conditionFieldKeys.find(conditionFieldKey => {
    //     			if(ConditionFieldMap[conditionFieldKey] == conditionFieldInfo[conditionFieldKey]){
    //        				isKey = true;
	// 				}
	// 			});
	// 				if (isKey)isTrans = true;
	// 				if(isTrans)return isTrans;
	// 			});
	// 			if(isTrans)newCodeTable.push(codeTableFilterMap);
	// 		});			
	// 		}else{
	// 		codeTableData.forEach(codeTableFilterMap =>{
	// 			isTrans = true;
	// 			if(codeTableFilterMap&&codeTableFilterMap.ConditionFields){
	// 				if (_.find(conditionFieldKeys, function (conditionFieldKey) {
    //     			return codeTableFilterMap.ConditionFields[0][conditionFieldKey]!=conditionFieldInfo[conditionFieldKey]
	// 			}.bind(this))){
	// 				isTrans = false;	
	// 			}
	// 			}
	// 			console.assert(codeTableFilterMap.ConditionFields,'ConditionField is null.CodeTable is '+ codeTableInfo);
	// 			if(isTrans)newCodeTable.push(codeTableFilterMap);
	// 		});
	// 		}
	// 	}else{
	// 		//if(Array.isArray(codeTableData))newCodeTable.push(codeTableData);
	// 		newCodeTable = codeTableData
	// 	}
	// 	return new CodeTable(newCodeTable);
	// }

};