import StoreAdapter from '../utils/StoreAdapter';
import ConstantKey from '../constant/ConstantKey';
import {PolicyStore,EndorsementStore} from 'rainbow-desktop-sdk';
import {PageContext} from "rainbow-desktop-cache";
var _ = require("lodash");

module.exports = {
    isEndo:function(){
        let isEndorsement = false;
        var endo = null;
        try {
            endo = Endorsement;
        } catch (e) {
            endo = null;
        }
        if(endo){
            isEndorsement = true;
        }
        // let EndoMap =[];
        //  _.forEach(ConstantKey.ENDORSEMENT_CHECK_KEY, function (endorKey){
        //     if(Policy[endorKey])EndoMap.push(Policy[endorKey]);
        // })
        // if(EndoMap.length > 0)isEndorsement = true;
        return isEndorsement;
    },
    getEndoType:function(){
        let endorsementId = PageContext.get("endorsementId");
        let endoType;
        if (endorsementId) {
            const endoStore = EndorsementStore.getEndorsement(endorsementId);
            endoType = endoStore["EndoType"];
        }else{
            endoType = null;
            console.assert(endoType !== null, "endorsementId is undefined");
        }
        return endoType;
    }
}