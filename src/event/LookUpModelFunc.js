//import {PolicyStore} from 'unicorn-ui-policy-logic';
import StoreAdapter from '../utils/StoreAdapter';
import ConstantKey from '../constant/ConstantKey';

module.exports = {
	lookupModel: function(modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject, policy) {
		let scopeObject = policy;
		// if (scopeModelName && scopeObjectCode) {
		// 	scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlways(scopeModelName, scopeObjectCode, policy, policy);
		// }
		if (scopeModelName && scopeObjectCode) {
			let scopeBusinessObjectId = scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
            let scopeBusinessObject = StoreAdapter.getBusinessObjectByObjectId(scopeBusinessObjectId);	
			// let targetBusinessObject = StoreAdapter.getBusinessObjectByModelNameAndCodeInScopeBusinessObject(scopeModelName, scopeObjectCode, scopeBusinessObject,nodeObject);
			let targetBusinessObject = StoreAdapter.getBusinessObjectByModelNameAndCodeOnlyOne(scopeModelName, scopeObjectCode, scopeBusinessObject,nodeObject);
			let matchedDomainInstance = StoreAdapter.getDomainObjectByBusinessObjectInScopeDomainObject(targetBusinessObject, scopeObject, scopeBusinessObject, StoreAdapter.getAllSchemaObjects());
            if (matchedDomainInstance) {
                scopeObject =  matchedDomainInstance;
            }
		}
		let parentObject = scopeObject;
		return StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCode, parentObject, scopeObject);
	},
	getCurrentTableRecord: function (policy) {
		let targetDomData = $(event.srcElement.parentElement).closest("tr").attr("data-value")
		if(!targetDomData) targetDomData = $('label[for$="'+ event.srcElement.parentElement.id.split('-')[1] +'"]').closest("tr").attr("data-value")
		if(!targetDomData){
			let elementId = event.srcElement.id||'';
			if(elementId&&elementId.indexOf('react-select')>-1){
				let positionElementArray = event.srcElement.parentElement.parentElement.id.split('-');
				positionElementArray.splice(-1,2,"value") ;
				let positionElementId = positionElementArray.join("-");
				targetDomData = $('span[id$="'+ positionElementId +'"]').closest("tr").attr("data-value");
			}else{
				let positionElementArray = event.srcElement.parentElement.id.split('-');
				positionElementArray.splice(-1,2,"value") ;
				let positionElementId = positionElementArray.join("-");
				targetDomData = $('span[id$="'+ positionElementId +'"]').closest("tr").attr("data-value");
			}
		}
		console.assert(targetDomData,'Please check for current event for getCurrentTableRecord()')
		let targetData = JSON.parse(targetDomData)
		let isTarget = true;
		if(_.isArray(targetData)&&targetData.length<1){
			isTarget = false;
		}
		if(targetData&&isTarget){
			let currentModelName = targetData["@type"].split("-")[0]
            let currentObjectCode = targetData["@type"].split("-")[1]
			let allData = StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(currentModelName,currentObjectCode,policy,policy)
			return _.find(allData,function (each) {
                if(targetData["@pk"]){
                	return targetData["@pk"]==each["@pk"]
				}else {
                	return targetData["BusinessUUID"]==each["BusinessUUID"]
				}
            })
		}
    }
};