import {UIMessageHelper} from 'rainbowui-desktop-core';
//import {PolicyActions, PolicyStore, PolicySchemaStore, PolicyDataUtils} from 'unicorn-ui-policy-logic';
//import {SchemaDataUtils} from 'unicorn-ui-basic-logic';
import PolicyAction from '../action/PolicyAction';
import ContainerMapUtils from '../utils/ContainerMapUtils';
import {PolicyStore} from 'rainbow-desktop-sdk';
import StoreAdapter from '../utils/StoreAdapter';
import ConstantKey from '../constant/ConstantKey';
import StoreAction from '../utils/StoreAction';
import EndorsementFunc from '../event/EndorsementFunc';
import FreshSectionFunc from '../event/FreshSectionFunc'

module.exports = {
	create: function(targetDialog, modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy) {
		let scopeObject;
		if (parentModelName && parentObjectCode) {
            scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,
                scopeModelName, scopeObjectCode);
		} else {
            scopeObject = StoreAdapter.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, policy);
        }
		let domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
        // let domainObject = StoreAdapter.createNewDomainObject(modelName, objectCode, scopeObject);
		ContainerMapUtils.get(targetDialog).showDialog(domainObject);
	},

	create_inline: function(targetDialog, targetTable, modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy, dialogObject,cuzObjectTemplate,cuzObjectList) {
		//let isEndorsement = EndorsementFunc.isEndo(policy);
		if(modelName=="Customization"&&objectCode=="Customization"){
			if(cuzObjectTemplate){
				let scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(cuzObjectTemplate["parentModelName"],cuzObjectTemplate["parentObjectCode"],policy,
				cuzObjectTemplate["scopeModelName"],cuzObjectTemplate["scopeObjectCode"])
				StoreAction.addDispatch(cuzObjectTemplate["modelName"],cuzObjectTemplate["objectCode"],scopeObject,policy);
				// if(isEndorsement){
				// 	policy[ConstantKey.POLICYSTATUS] = 1;
				// }
			}
			if(cuzObjectList&&cuzObjectList.length>0){
				let template = _.clone(cuzObjectList[0]);
				Object.keys(template).map(key => template[key] = "")
				if(template){
					delete template.PolicyId;
					delete template.PolicyElementId;
				}
				cuzObjectList.push(template);
				FreshSectionFunc.refreshSection(targetTable)
			}
		}else{
            if (!dialogObject) {
                let scopeObject;
                if (parentModelName && parentObjectCode) {
                    //scopeObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,
                    scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,
                        scopeModelName, scopeObjectCode);
                } else {
                    //scopeObject = PolicyStore.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, policy);
                    scopeObject = StoreAdapter.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, policy);
                }
                /*unchecked*/
                StoreAction.addDispatch(modelName,objectCode,scopeObject,policy);
                //PolicyActions.addDomainObjectInstantly(modelName, objectCode, scopeObject, policy);
				// if(isEndorsement){
				// 	policy[ConstantKey.POLICYSTATUS] = 1;
				// }
			} else {
                // let scopeObject = PolicyStore.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, policy);
                let scopeObject = StoreAdapter.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, policy);
                // PolicyStore.createNewInstanceByModelNameAndObjectAndAttachToParent(modelName, objectCode, dialogObject, scopeObject, policy)
                StoreAdapter.createNewInstanceByModelNameAndObjectAndAttachToParent(modelName, objectCode, dialogObject, scopeObject, policy)
                //let allBusinessObject = PolicySchemaStore.getAllSchemaObjects();
                let allBusinessObject = StoreAdapter.getAllSchemaObjects();
                //let businessObject = SchemaDataUtils.getBusinessObjectByModelNameAndCode(modelName, objectCode, allBusinessObject)[0];
                let businessObject = StoreAdapter.getBusinessObjectByModelNameAndCode(modelName, objectCode, allBusinessObject)[0];
                //let parentObject = PolicyStore.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, dialogObject);
                let parentObject = StoreAdapter.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, dialogObject);
                //PolicyDataUtils.createNewDomainChildInstanceAndAttachToParent(parentObject, businessObject, allBusinessObject);
                StoreAdapter.createNewDomainChildInstanceAndAttachToParent(parentObject, businessObject, allBusinessObject);
                delete dialogObject.versionForClient;
                ContainerMapUtils.get(targetDialog).setState({dialogObject: dialogObject});
                ContainerMapUtils.get(targetTable).onChangeListener({policy:policy});
            }
		}

	},

	edit: function(targetTable, targetDialog) {
		let records = ContainerMapUtils.get(targetTable).getSelectedRecord();
		if (!records || !records.length) {
			UIMessageHelper.info("Please select a record", "Message", UIMessageHelper.POSITION_TOP_FULL_WIDTH);
		} else {
			let domainObjectList = ContainerMapUtils.get(targetTable).state.domainObject;
			let domainObject1 = domainObjectList.find(domainObject => {
				if (domainObject.BusinessUUID && records[0].BusinessUUID)
					return domainObject.BusinessUUID === records[0].BusinessUUID;
				return _.isEqual(domainObject, records[0]);
			});
			ContainerMapUtils.get(targetDialog).showDialog(domainObject1);
		}
	},

	delete: function(targetDialog, targetTable, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy, dialogObject,cuzObjectList) {
		if(cuzObjectList&&cuzObjectList.length>0){
			let realTargetData = JSON.parse($(event.srcElement.parentElement).closest("tr").attr("data-value"))			
			let aimIndex = realTargetData.dataIndex;
			for (let i=0; i<cuzObjectList.length; i++) {
				if(cuzObjectList[i].dataIndex == aimIndex){
					cuzObjectList.splice(i,1)
				}
			}
			FreshSectionFunc.refreshSection(targetTable)
		}else{
			let records = ContainerMapUtils.get(targetTable).getSelectedRecord();
			let targetTemplate = ContainerMapUtils.get(targetTable).props.cuzObjectTemplate;
			let orgModelName = ContainerMapUtils.get(targetTable).props.node.DataModelName
			let orgObjectCode = ContainerMapUtils.get(targetTable).props.node.DataObjectCode
			//let isEndorsement = EndorsementFunc.isEndo(policy);
			if (!records || !records.length) {
				let realTargetData = JSON.parse($(event.srcElement.parentElement).closest("tr").attr("data-value"))
				if(realTargetData.length==0){
					UIMessageHelper.info("Please select a record", "Message", UIMessageHelper.POSITION_TOP_FULL_WIDTH);
				}else{
					let scopeObject = policy;
					if (dialogObject) scopeObject = dialogObject;
					if (targetTemplate&&orgModelName==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&orgObjectCode==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
						scopeModelName = targetTemplate.scopeModelName
						scopeObjectCode = targetTemplate.scopeObjectCode
						parentModelName = targetTemplate.parentModelName
						parentObjectCode = targetTemplate.parentObjectCode
					}
					if (parentModelName && parentObjectCode && scopeModelName && scopeObjectCode) {
						scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,scopeModelName, scopeObjectCode)
					} else	if (scopeModelName && scopeObjectCode) {
						scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(scopeModelName, scopeObjectCode, policy);
					}
					// if(isEndorsement){
					// 	policy[ConstantKey.POLICYSTATUS] = 4;
					// }
					StoreAction.deleteDispatch(realTargetData, scopeObject, policy);
				}
			} else {
				let scopeObject = policy;
				if (dialogObject) scopeObject = dialogObject;
				if (targetTemplate&&orgModelName==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&orgObjectCode==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
					scopeModelName = targetTemplate.scopeModelName
					scopeObjectCode = targetTemplate.scopeObjectCode
					parentModelName = targetTemplate.parentModelName
					parentObjectCode = targetTemplate.parentObjectCode
				}
				if (parentModelName && parentObjectCode && scopeModelName && scopeObjectCode) {
					//scopeObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,
					//    scopeModelName, scopeObjectCode)
					scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,scopeModelName, scopeObjectCode)
				} else	if (scopeModelName && scopeObjectCode) {
					//scopeObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(scopeModelName, scopeObjectCode, policy);
					scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(scopeModelName, scopeObjectCode, policy);
				}
				// if(isEndorsement){
				// 	policy[ConstantKey.POLICYSTATUS] = 4;
				// }
				/*unchecked*/
				//PolicyActions.deleteDomainObject(records[0], scopeObject, policy);
				StoreAction.deleteDispatch(records[0], scopeObject, policy);
			}
		}
	},
	
	saveDialog: function(targetDialog, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, domainObject, policy) {
		let scopeObject = policy;
		if (parentModelName && parentObjectCode && scopeModelName && scopeObjectCode) {
			//scopeObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,
            scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,
				scopeModelName, scopeObjectCode);
		} else if (scopeModelName && scopeObjectCode) {
			//scopeObject = PolicyStore.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
            scopeObject = StoreAdapter.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
		}
		if (!scopeObject) scopeObject = policy;
        /*unchecked*/
		//PolicyActions.saveDomainObject(domainObject, scopeObject, policy);
        StoreAction.saveDomainObjectDispatch(domainObject, scopeObject, policy);
		ContainerMapUtils.get(targetDialog).hideDialog();
	},

	savePolicy: function(policy) {
		//PolicyActions.savePolicy(policy);
		StoreAction.savePolicyDispatch(policy)
	},

	cancel: function(targetDialog) {
		ContainerMapUtils.get(targetDialog).hideDialog();
	},

	calculate: function(policy) {
		AjaxUtil.show();
		let policyAction = new PolicyAction();
		policyAction.calculatePolicyNBFee(policy).then(
			function(data){
				AjaxUtil.hide();
			},
			function(err){
				AjaxUtil.hide();
			}
		);
	}
};