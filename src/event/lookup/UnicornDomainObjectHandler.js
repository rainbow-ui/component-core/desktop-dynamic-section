import UnicornUiDomainSpecificLanguage from "./UnicornUiDomainSpecificLanguage";
//import {UnicornUiLogicConstants,PolicySchemaStore} from 'unicorn-ui-policy-logic';
import {PolicyStore} from 'rainbow-desktop-sdk';
import StoreAdapter from '../../utils/StoreAdapter';
import ConstantKey from '../../constant/ConstantKey';

/**
 * UnicornDomainObjectHandler module.
 * @module unicorn-ui-basic-logic/UnicornDomainObjectHandler
 * @see  module:unicorn-ui-basic-logic/UnicornDomainObjectHandler
 */
module.exports = {

    /**
     * filter result by condition.
     * @param  {String} filterExpression
     * @return  {Object} the wrapper value of UnicornDomainObjectHandler for chain operation.
     */
        filter(filterExpression) {
        'use strict';
        let targetDomainObjects = this.values;
        //let allSchemaObject = PolicySchemaStore.getAllSchemaObjects();
        let allSchemaObject = StoreAdapter.getAllSchemaObjects();
        let filteredResult = UnicornUiDomainSpecificLanguage.executeFilterCondition(targetDomainObjects, filterExpression, allSchemaObject);
        if (!_.isEmpty(filteredResult)) {
            let result = {'values': filteredResult};
            _.extend(result, this);
            result.values = filteredResult;
            return result;
        } else {
            return UnicornDomainObjectHandler;
        }
    },

    /**
     * filter result by condition.
     * @param  {String} filterExpression
     * @return  {Object} the wrapper value of UnicornDomainObjectHandler for chain operation.
     */
        val() {
        'use strict';
        if(this && this.values){
            if(_.isArray(this.values) && this.values.length == 1){
                return this.values[0];
            }
        }
        return this.values;
    },

    /**
     * get the min one.
     * @param  {String} fieldName
     * @return  {Object} the min value.
     */
        min(fieldName) {
        'use strict';
        let targetDomainObjects = this.values;

        if (fieldName) {
            // get minimal object by field name
            if (!_.isEmpty(targetDomainObjects)) {
                let minValueObject = _.min(targetDomainObjects, function (targetDomainObject) {
                    return targetDomainObject[fieldName];
                });
                return minValueObject[fieldName];
            }
        } else {
            return _.min(targetDomainObjects);
        }
    },

    /**
     * get the max one.
     * @param  {String} fieldName
     * @return  {Object} the max value.
     */
        max(fieldName) {
        'use strict';
        let targetDomainObjects = this.values;
        if (fieldName) {
            // get minimal object by field name
            if (!_.isEmpty(targetDomainObjects)) {
                let maxValueObject = _.max(targetDomainObjects, function (targetDomainObject) {
                    return targetDomainObject[fieldName];
                });
                return maxValueObject[fieldName];
            }
        } else {
            return _.max(targetDomainObjects);
        }
    },

    /**
     * get the sum value.
     * @param  {String} fieldName
     * @return  {Object} the sum value.
     */
        sum(fieldName) {
        'use strict';
        let targetDomainObjects = this.values;
        if (fieldName) {
            // get sum result of  object field value
            if (!_.isEmpty(targetDomainObjects)) {
                return _.reduce(targetDomainObjects, function (result, targetDomainObject) {
                    if (targetDomainObject[fieldName] && Number(targetDomainObject[fieldName])) {
                        return result + Number(targetDomainObject[fieldName]);
                    } else {
                        return result;
                    }
                }, 0);
            }
        } else {
            return _.reduce(targetDomainObjects, function (result, targetDomainObject) {
                if (Number(targetDomainObject)) {
                    return result + Number(targetDomainObject);
                } else {
                    return result;
                }
            }, 0);
        }
    },


    /**
     * get the average value.
     * @param  {String} fieldName
     * @return  {Object} the average value.
     */
        average(fieldName) {
        'use strict';
        let targetDomainObjects = this.values;
        let sumResult = this.sum(fieldName);
        let count = _.size(targetDomainObjects);
        if (count > 0) {
            return sumResult / count;
        } else {
            return 0;
        }
    },

    /**
     * get the size of value.
     * @return  {Number} the size.
     */
        count() {
        'use strict';
        let targetDomainObjects = this.values;
        return _.size(targetDomainObjects);
    }
};