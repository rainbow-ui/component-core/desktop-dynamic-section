import AggregateOperationEnum from "./AggregateOperationEnum";

/**
 * UnicornAggregate module.
 * @module unicorn-ui-basic-logic/UnicornAggregate
 * @see  module:unicorn-ui-basic-logic/UnicornAggregate
 */
module.exports = {

    /**
     * sum the field value.
     * @param  {Array} fieldValues
     * @return  {Number} the sum value.
     */
        sumUnicornAggregate(fieldValues){
        'use strict';
        if (_.isEmpty(fieldValues)) {
            return 0;
        }
        let sumResult = Number(0);
        _.each(fieldValues, function (fieldValue) {
            if (!_.isNaN(Number(fieldValue))) {
                sumResult += Number(fieldValue);
            }
        }.bind(this));

        return sumResult;
    },

    /**
     * get the max among the field value.
     * @param  {Array} fieldValues
     * @param  {String} fieldName -optional
     * @return  {Number} the max value.
     */
        maxUnicornAggregate(values, fieldName){
        'use strict';
        if (_.isEmpty(values)) {
            return null;
        }
        if (!fieldName) {
            return _.max(values);
        } else {
            return _.max(values, function (value) {
                return value[fieldName];
            });
        }

    },

    /**
     * get the min among the field value.
     * @param  {Array} fieldValues
     * @param  {String} fieldName -optional
     * @return  {Number} the min value.
     */
        minUnicornAggregate(values, fieldName){
        'use strict';
        if (_.isEmpty(values)) {
            return null;
        }

        if (!fieldName) {
            return _.min(values);
        } else {
            return _.min(values, function (value) {
                return value[fieldName];
            });
        }
    },

    /**
     * get the average of the field value.
     * @param  {Array} fieldValues
     * @return  {Number} the average value.
     */
        averageUnicornAggregate(fieldValues){
        'use strict';
        if (_.isEmpty(fieldValues)) {
            return 0;
        }

        let numberCount = Number(0);
        let sumResult = Number(0);
        _.each(fieldValues, function (fieldValue) {
            if (!_.isNaN(Number(fieldValue))) {
                sumResult += Number(fieldValue);
                numberCount++;
            }
        }.bind(this));

        if (numberCount > 0) {
            return sumResult / numberCount;
        } else {
            return 0;
        }
    },

    /**
     * get the count of the field value.
     * @param  {Array} fieldValues
     * @return  {Number} the count of field values.
     */
        countUnicornAggregate(fieldValues){
        'use strict';
        if (_.isEmpty(fieldValues)) {
            return 0;
        }

        if (_.isObject(fieldValues) && !_.isArray(fieldValues)) {
            return 1;
        } else if (_.isArray(fieldValues)) {
            return fieldValues.length;
        }
    },


    /**
     * the entrance of the aggregate operation.
     * @param  {Array} fieldValues
     * @param  {Number} aggregateOperationEnum
     * @param  {String} fieldName
     * @return  {Object} the aggregate operation result
     */
        unicornAggregateOperation(values, operation, fieldName){
        'use strict';
        console.assert(values !== null, "fieldValues cannot be null.");
        console.assert(operation !== null, "operation cannot be null.");
        switch (operation) {
            case  AggregateOperationEnum.AGGREGATE_OPERATION_SUM:
                return this.sumUnicornAggregate(values);
            case AggregateOperationEnum.AGGREGATE_OPERATION_MAX:
                return this.maxUnicornAggregate(values, fieldName);
            case AggregateOperationEnum.AGGREGATE_OPERATION_MIN:
                return this.minUnicornAggregate(values, fieldName);
            case AggregateOperationEnum.AGGREGATE_OPERATION_AVERAGE:
                return this.averageUnicornAggregate(values);
            case AggregateOperationEnum.AGGREGATE_OPERATION_COUNT:
                return this.countUnicornAggregate(values);
            default :
                throw new Error("operation is not correct.");
        }
    }
};