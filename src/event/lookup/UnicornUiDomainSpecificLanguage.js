//import {SchemaDataUtils} from 'unicorn-ui-basic-logic';
//import {UnicornUiLogicConstants,PolicySchemaStore} from 'unicorn-ui-policy-logic';
import StoreAdapter from '../../utils/StoreAdapter';
import ConstantKey from '../../constant/ConstantKey';
/**
 * UnicornUiDomainSpecificLanguage module.
 * @module unicorn-ui-basic-logic/UnicornUiDomainSpecificLanguage
 * @see  module:unicorn-ui-basic-logic/UnicornUiDomainSpecificLanguage
 */
module.exports = {

    /**
     * get the PK if there has one,or return the business UUID
     * @param  {Object} objectInstance
     * @return  {String} the PK or businessUUID
     */
        getBusinessUUID(objectInstance) {
        'use strict';
        if (objectInstance) {
            //let pk = objectInstance[UnicornUiLogicConstants.COMM_PK];
            let pk = objectInstance[ConstantKey.COMM_PK];
            if (pk) {
                return pk;
            }
            //let businessUUD = objectInstance[UnicornUiLogicConstants.COMM_UUID];
            let businessUUD = objectInstance[ConstantKey.COMM_UUID];
            
            if (businessUUD) {
                return businessUUD;
            }
            return null;
        }
    },

    /**
     * Whether the domain object with the parameter model name
     * @param  {Object} objectInstance
     * @param  {String} modelName
     * @param  {Array} allSchemaObject
     * @return  {Boolean} if including return true,or return false.
     */
        isDomainObjectWithParameterModelName(objectInstance, modelName, allSchemaObject) {
        'use strict';
        if (objectInstance) {
            //let currentModelName = SchemaDataUtils.getBusinessModelNameByBusinessObjectId(objectInstance[UnicornUiLogicConstants.DD_BUSINESS_OBJECT_ID], allSchemaObject);
            let currentModelName = StoreAdapter.getBusinessModelNameByBusinessObjectId(objectInstance[ConstantKey.DD_BUSINESS_OBJECT_ID], allSchemaObject);
            if (currentModelName === modelName) {
                return true;
            }
        }
        return false;
    },

    /**
     * Whether the src domain object is equal with the target one
     * @param  {Object} scrDomainObject
     * @param  {Object} targetDomainObject
     * @return  {Boolean} if they are equal return true or return false.
     */
        isEqual(scrDomainObject, targetDomainObject){
        'use strict';
        console.assert(scrDomainObject !== null, 'The scrDomainObject can not be null.');
        console.assert(targetDomainObject !== null, 'The targetDomainObject can not be null.');
        let scrDomainObjectBusinessUUID = this.getBusinessUUID(scrDomainObject);
        console.assert(scrDomainObjectBusinessUUID !== null, 'The scrDomainObjectBusinessUUID can not be null.');
        let targetDomainObjectBusinessUUID = this.getBusinessUUID(targetDomainObject);
        console.assert(targetDomainObjectBusinessUUID !== null, 'The targetDomainObjectBusinessUUID can not be null.');
        if (scrDomainObjectBusinessUUID === targetDomainObjectBusinessUUID) {
            return true;
        } else {
            return false;
        }
    },

    /**
     * get the parent of the parameter domain object under startDomainObject
     * @param  {Object} domainObject
     * @param  {Object} startDomainObject
     * @return  {Object}  domainObject -the parent of the parameter domain object under startDomainObject.
     */
        getParentDomainObjectByDomainObjectAndStartDomainObject(domainObject, startDomainObject){
        'use strict';
        console.assert(domainObject !== null, 'The domainObject can not be null.');
        console.assert(startDomainObject !== null, 'The startDomainObject can not be null.');
        let matchedParent = null;
        // get all array and object properties
        let beanProperties = _.chain(startDomainObject).values().filter(function (currentPropertyDomainObject) {
            return (_.isObject(currentPropertyDomainObject) || _.isArray(currentPropertyDomainObject)) && !_.isFunction(currentPropertyDomainObject);
        }).flatten(true).value();

        // remove the business object which does not include business uuid key
        let businessBeanProperties = _.filter(beanProperties, function (currentPropertyDomainObject) {
            return this.getBusinessUUID(currentPropertyDomainObject);
        }.bind(this));

        if (!_.isEmpty(businessBeanProperties)) {
            // direct child property search
            let matchedSelf = _.find(businessBeanProperties, function (currentPropertyDomainObject) {
                return this.isEqual(domainObject, currentPropertyDomainObject);
            }.bind(this));

            // locate itself in the javascript object tree data structure
            if (matchedSelf) {
                matchedParent = startDomainObject;
                return matchedParent;
            }

            // there is no matched domain object in direct child domain object,then search recursively
            if (!matchedParent) {
                for (let index = 0; index < _.size(businessBeanProperties); index++) {
                    let childElement = businessBeanProperties[index];
                    matchedParent = this.getParentDomainObjectByDomainObjectAndStartDomainObject(domainObject, childElement);
                    if (matchedParent) {
                        return matchedParent;
                    }
                }
            } else {
                return matchedParent;
            }
        }
    },

    /**
     * get the parent of the parameter domain object under deliveredRootDomainObject
     * @param  {Object} domainObject
     * @param  {Object} deliveredRootDomainObject
     * @return  {Object}  domainObject -the parent of the parameter domain object under deliveredRootDomainObject.
     */
        getParentDomainObjectByDomainObject(domainObject, deliveredRootDomainObject){
        'use strict';
        console.assert(domainObject !== null, 'The domainObject can not be found.');
        let currentContextRootDomainInstance = deliveredRootDomainObject;
        console.assert(currentContextRootDomainInstance !== null, 'The currentContextRootDomainInstance can not be found.');
        if (this.isEqual(domainObject, currentContextRootDomainInstance)) {
            return;
        }
        return this.getParentDomainObjectByDomainObjectAndStartDomainObject(domainObject, currentContextRootDomainInstance);
    },

    /**
     * get the parent of the parameter domain object with the same model name
     * @param  {Object} domainObject
     * @param  {String} parentModelName
     * @param  {Array} allSchemaObject
     * @return  {Object}  domainObject
     */
        getParentDomainObjectByDomainObjectAndParentModelName(domainObject, parentModelName, allSchemaObject){
        'use strict';
        console.assert(domainObject !== null, 'The domainObject can not be found.');
        let currentContextRootDomainInstance = global['CurrentContextRootDomainInstance'];
        console.assert(currentContextRootDomainInstance !== null, 'The currentContextRootDomainInstance can not be found.');
        if (this.isEqual(domainObject, currentContextRootDomainInstance)) {
            return;
        }
        let directParentBusinessObject = this.getParentDomainObjectByDomainObjectAndStartDomainObject(domainObject, currentContextRootDomainInstance);
        if (directParentBusinessObject) {
            if (this.isDomainObjectWithParameterModelName(directParentBusinessObject, parentModelName, allSchemaObject)) {
                return directParentBusinessObject;
            } else {
                return this.getParentDomainObjectByDomainObjectAndParentModelName(directParentBusinessObject, parentModelName, allSchemaObject);
            }
        } else {
            return null;
        }

    },

    /**
     * if the business object of the current domain object has the field
     * @param  {Object} domainObject
     * @param  {String} fieldName
     * @param  {Array} allSchemaObject
     * @return  {Boolean}  if having then return true,else return false
     */
        isIncludingFieldName(domainObject, fieldName, allSchemaObject){
        'use strict';
        //let businessObjectId = domainObject[UnicornUiLogicConstants.DD_BUSINESS_OBJECT_ID];
        let businessObjectId = domainObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
        console.assert(businessObjectId !== null, "businessObjectId is null.");
        //let matchedBusinessObject = SchemaDataUtils.getBusinessObjectById(businessObjectId, allSchemaObject);
        let matchedBusinessObject = StoreAdapter.getBusinessObjectById(businessObjectId, allSchemaObject);
        console.assert(matchedBusinessObject !== null, "matchedBusinessObject is null.");
        //let fields = matchedBusinessObject[UnicornUiLogicConstants.COMM_FIELDS];
        let fields = matchedBusinessObject[ConstantKey.COMM_FIELDS];
        if (fields) {
            let fieldNames = _.keys(fields);
            if (!_.isEmpty(fieldNames)) {
                if (_.find(fieldNames, function (tempFieldName) {
                        return tempFieldName === fieldName;
                    })) {
                    return true;
                }
            }
        }

        return false;
    },

    /**
     * lookup the value of the parameter field name start with the current domain object up to root
     * @param  {Object} currentDomainObject
     * @param  {String} fieldName
     * @param  {Array} allSchemaObject
     * @return  {Object}  the field value
     */
        lookupVariableCascadeUp(currentDomainObject, fieldName, allSchemaObject){
        'use strict';
        console.assert(currentDomainObject !== null, 'The currentDomainObject can not be null.');
        console.assert(fieldName !== null, 'The fieldName can not be null.');
        if (this.isIncludingFieldName(currentDomainObject, fieldName, allSchemaObject)) {
            return currentDomainObject[fieldName];
        } else {
            let parentDomainObject = this.getParentDomainObjectByDomainObject(currentDomainObject);
            if (parentDomainObject) {
                return this.lookupVariableCascadeUp(parentDomainObject, fieldName, allSchemaObject);
            } else {
                return null;
            }
        }
    },

    /**
     * get the parent domain object with the parameter's model name
     * @param  {String} parentModelName
     * @param  {Object} deliveredCurrentDomainObject
     * @param  {Object} deliveredRootDomainObject
     * @return  {Object}  the matched parent domain object
     */
        lookupParentDomainObjectCascadeUp(parentModelName, deliveredCurrentDomainObject, deliveredRootDomainObject){
        'use strict';
        console.assert(parentModelName !== null, 'The parentModelName can not be null.');
        let currentDomainObject = deliveredCurrentDomainObject;
        console.assert(currentDomainObject !== null, 'The currentDomainObject can not be null.');
        let allSchemaObject = PolicySchemaStore.getAllSchemaObjects();
        console.assert(allSchemaObject !== null, 'The allSchemaObject can not be null.');
        if (this.isDomainObjectWithParameterModelName(currentDomainObject, parentModelName, allSchemaObject)) {
            return currentDomainObject;
        } else {
            let parentDomainObject = this.getParentDomainObjectByDomainObject(currentDomainObject, deliveredRootDomainObject);
            while (parentDomainObject) {
                if (this.isDomainObjectWithParameterModelName(parentDomainObject, parentModelName, allSchemaObject)) {
                    return parentDomainObject;
                } else {
                    parentDomainObject = this.getParentDomainObjectByDomainObject(parentDomainObject);
                }
            }
        }
    },

    /**
     * to get the child domain object with the parameter's model name
     * @param  {String} childModelName
     * @param  {Object} deliveredCurrentDomainObject
     * @return  {Object}  the matched child domain object
     */
        lookupChildDomainObjectDown(childModelName, deliveredCurrentDomainObject){
        'use strict';
        console.assert(childModelName !== null, 'The childModelName can not be null.');
        let currentDomainObject = deliveredCurrentDomainObject;
        console.assert(currentDomainObject !== null, 'The currentDomainObject can not be null.');
        let allSchemaObject = PolicySchemaStore.getAllSchemaObjects();
        console.assert(allSchemaObject !== null, 'The allSchemaObject can not be null.');
        let allChildrenDomainObject = [];
        this.getAllChildrenDomainObject(currentDomainObject, allChildrenDomainObject);
        if (!_.isEmpty(allChildrenDomainObject)) {
            return _.filter(allChildrenDomainObject, function (childDomainObject) {
                return this.isDomainObjectWithParameterModelName(childDomainObject, childModelName, allSchemaObject);
            }.bind(this));
        }
    },

    /**
     * to get the all children  of the parameter domain object
     * @param  {Object} domainObject
     * @param  {Array} childrenDomainObjects
     */
        getAllChildrenDomainObject(domainObject, childrenDomainObjects){
        'use strict';
        if (domainObject) {
            // get all array and object properties
            let beanProperties = _.chain(domainObject).values().filter(function (currentPropertyDomainObject) {
                return (_.isObject(currentPropertyDomainObject) || _.isArray(currentPropertyDomainObject)) && !_.isFunction(currentPropertyDomainObject);
            }).flatten(true).value();

            // remove the business object which does not include business uuid key
            let businessBeanProperties = _.filter(beanProperties, function (currentPropertyDomainObject) {
                return this.getBusinessUUID(currentPropertyDomainObject);
            }.bind(this));

            if (!_.isEmpty(businessBeanProperties)) {
                _.each(businessBeanProperties, function (businessBeanProperty) {
                    childrenDomainObjects.push(businessBeanProperty);
                    this.getAllChildrenDomainObject(businessBeanProperty, childrenDomainObjects);
                }.bind(this));
            }
        }
    },

    /**
     * to get the child domain with the parameter model name
     * @param  {Object} domainObject
     * @param  {String} childModelName
     * @param  {Array} childrenDomainObjects
     * @return {Object} the matched child domain object.
     */
        getChildrenDomainObjectByDomainObjectAndChildModelName(domainObject, childModelName, allSchemaObject){
        'use strict';
        console.assert(domainObject !== null, 'The domainObject can not be null.');
        console.assert(childModelName !== null, 'The childModelName can not be null.');
        let allChildrenDomainObject = [];
        this.getAllChildrenDomainObject(domainObject, allChildrenDomainObject);
        if (!_.isEmpty(allChildrenDomainObject)) {
            return _.filter(allChildrenDomainObject, function (childDomainObject) {
                return this.isDomainObjectWithParameterModelName(childDomainObject, childModelName, allSchemaObject);
            }.bind(this));
        }
    },

    /**
     * to execute the condition  expression.
     * @param  {Array} domainObjects
     * @param  {String} conditionExpression
     * @param  {Array} allSchemaObject
     * @return {Array} the matched domain object Array
     */
        executeFilterCondition(domainObjects, conditionExpression, allSchemaObject){
        'use strict';
        console.assert(domainObjects !== null, 'The domainObjects can not be null.');
        console.assert(conditionExpression !== null, 'The conditionExpression can not be null.');
        console.assert(allSchemaObject !== null, 'The allSchemaObject can not be null.');
        if (_.isEmpty(domainObjects)) {
            return;
        }
        let filteredResult = [];
        _.each(domainObjects, function (domainObject) {
            //let businessObjectId = domainObject[UnicornUiLogicConstants.DD_BUSINESS_OBJECT_ID];
            let businessObjectId = domainObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
            console.assert(businessObjectId !== null, "businessObjectId is null.");
            //let matchedBusinessObject = SchemaDataUtils.getBusinessObjectById(businessObjectId, allSchemaObject);
            let matchedBusinessObject = StoreAdapter.getBusinessObjectById(businessObjectId, allSchemaObject);
            console.assert(matchedBusinessObject !== null, "matchedBusinessObject is null.");
            matchedBusinessObject = JSON.parse(JSON.stringify(matchedBusinessObject));
            let expressionContext = _.extend(matchedBusinessObject, domainObject);
            let propertyValues = this.buildRuntimeContextByObject(expressionContext);
            const filterConditionFunction = new Function(propertyValues + ' return ' + conditionExpression);
            if (filterConditionFunction.call(expressionContext)) {
                filteredResult.push(domainObject);
            }
        }.bind(this));

        return filteredResult;
    },

    /**
     * to build condition run time context
     * @param  {Array} objectContext
     */
        buildRuntimeContextByObject(objectContext){
        'use strict';
        let expressionRegular = /^([\w_]+)$/;
        let propertyNames = _.keys(objectContext);
        if (!_.isEmpty(propertyNames)) {
            return _.reduce(propertyNames, function (result, propertyName) {
                if (!result) {
                    result = "";
                }

                if (propertyName.match(expressionRegular)) {
                    let value = objectContext[propertyName];
                    if (_.isNumber(value)) {
                        return result + " var " + propertyName + " = " + value + ";";
                    } else if (_.isString(value)) {
                        return result + " var " + propertyName + " = '" + value + "';";
                    } else {
                        return result;
                    }
                } else {
                    return result;
                }
            }, "");
        }
    }
};