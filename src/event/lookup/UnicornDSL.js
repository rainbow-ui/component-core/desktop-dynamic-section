import UnicornUiDomainSpecificLanguage from "./UnicornUiDomainSpecificLanguage";
import UnicornDomainObjectHandler from "./UnicornDomainObjectHandler";

/**
 * UnicornDSL module.
 * @module unicorn-ui-basic-logic/UnicornDSL
 * @see  module:unicorn-ui-basic-logic/UnicornDSL
 */
module.exports = {

     /**
     * execute unicorn ui dsl to Lookup domain object  and  return the result
     * @param  {String} expression
     * @param  {Object} deliveredCurrentDomainObject
     * @param  {Object} deliveredRootDomainObject
     * @return  {Object} the wrapper value of UnicornDomainObjectHandler for chain operation.
     */
        executeUiLookupDsl(expression, deliveredCurrentDomainObject, deliveredRootDomainObject){
        'use strict';
        /*
         * executeUiLookupDsl(
         *              expression : String
         *              deliveredCurrentDomainObject: DomainObject
         *              deliveredRootDomainObject: DomainObject
         * 1. Lookup domain object

         a) @('PolicyLob')
         以current domain object 为起点一直查找parent直到追溯到root节点，查找同名的变量，来得到值 model name 为PolicyLOb的节点
         b) @('.PolicySubCover')
         以current domain object 为parent node，定位查找到PolicySubCover的所有子节点
         同时对结果进行分装包括集计函数的对象
         */
        let expressionRegular = /^([\.]?)([\w]+)$/;
        let matchedResult = expression.match(expressionRegular);
        let isLookupChild = false;
        let modelName = null;
        if (matchedResult[1] === ".") {
            console.assert(matchedResult.length > 2, "The expression's format is not correct.");
            isLookupChild = true;
            modelName = matchedResult[2];
        } else {
            modelName = matchedResult[2];
        }

        // get children case which will support chain object
        if (isLookupChild) {
            let matchedChildrenObjects = UnicornUiDomainSpecificLanguage.lookupChildDomainObjectDown(modelName, deliveredCurrentDomainObject);
            let result = null;
            if (!_.isEmpty(matchedChildrenObjects)) {
                result = {'values': matchedChildrenObjects};
                return _.extend(result, UnicornDomainObjectHandler);
            } else {
                return UnicornDomainObjectHandler;
            }
        } else {
            return UnicornUiDomainSpecificLanguage.lookupParentDomainObjectCascadeUp(modelName, deliveredCurrentDomainObject, deliveredRootDomainObject, deliveredRootDomainObject);
        }
    }
};