'use strict';
let AggregateOperationEnum = {
    AGGREGATE_OPERATION_SUM: "sum",
    AGGREGATE_OPERATION_MAX: "max",
    AGGREGATE_OPERATION_MIN: "min",
    AGGREGATE_OPERATION_AVERAGE: "average",
    AGGREGATE_OPERATION_COUNT: "count"
};

module.exports = AggregateOperationEnum;
