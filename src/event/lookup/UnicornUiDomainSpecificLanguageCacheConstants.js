'use strict';

let UnicornUiDomainSpecificLanguageCacheConstants = {
    COMMON_CONTEXT_TYPE: -1,
    COMMON_CONTEXT_REFERENCE_ID:-1,
    CONNECT_KEY:"_",
    ChildBusinessObjectArray:"ChildBusinessObjectArray",
    ChildBusinessObjectInArray:"ChildBusinessObjectInArray",
    InstanceObjectsCascadeArray:"InstanceObjectsCascadeArray",
    VERSION_FOR_CLIENT:"versionForClient",
    FIELDS:"Fields",
    DEFAULT_VALUE:"DefaultValue",
    ALL_BUSINESS_OBJECT_CACHE:"0",
    BUSINESS_MODEL_CODE_MATCHED_CACHE:"7",
    BUSINESS_OBJECT_MATCHED_CACHE:"8",
    ALL_BUSINESS_OBJECT_MATCHED_CACHE:"9"
};

module.exports = UnicornUiDomainSpecificLanguageCacheConstants;
