import style from '../css/style.css';
import loadingGif from '../css/loading.gif'

export default class LoadingLayer extends React.Component {

    render(){

         return(

             <div id="loadings" className="loadingStyle">
                 <img src={loadingGif}/>
                 <br/>
             </div>
        );
    }
};