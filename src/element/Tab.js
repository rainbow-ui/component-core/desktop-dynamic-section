import BaseElement from '../element/BaseElement';
import ConstantKey from '../constant/ConstantKey';
import ConstantEventDefinition from '../constant/ConstantEventDefinition';
import Event from '../template/Event';

export default class TabElement extends BaseElement {
    constructor(node) {
        super(node);
        this.generateEvent = this.generateEvent;
        this.onTabChangeValue = this.onTabChangeValue;
    }

    generateAttribute() {
        let result = super.generateAttribute();

        if (this.state.property && this.state.property.tabIndex) {
            result.activeIndex = this.state.property.tabIndex;
        }
        return result;
    }

    generateEvent(){
        let node = this.node, result = {}, eventProp, hasOnChangeEvent = false;
        if (node[ConstantKey.EVENT_LIST]) {
            node[ConstantKey.EVENT_LIST].forEach(event => {
                var eventKey = null, eventAction = null, value = null;
                for (let key in event) {
                    if (key === ConstantKey.EVENT_CODE_KEY) eventKey = event[key];
                    if (key === ConstantKey.EVENT_ACTION_KEY) eventAction = event[key];
                }
                if (eventKey === 'onStateChange') {
                    return;
                }
                if (eventKey && eventAction) {
                    if (eventKey === ConstantEventDefinition.ON_TAB_CHANGE) {
                        hasOnChangeEvent = true;
                    }
                    eventProp = this.callEvent(Event[eventKey], this, [eventAction]);
                    _.extend(result, eventProp);
                }
            });
        }
        if (!hasOnChangeEvent && this.node[ConstantKey.FIELD_BINDING]) {
            eventProp = {onTabChange: this.onTabChangeValue.bind(this)};
            _.extend(result, eventProp);
        }
        // this.createDefaultEvent(node, result);
        return result;
    }

    onTabChangeValue(event) {
        let fieldBinding = this.props.node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME];
        let subItemList = this.state.property.tabItemList;
        let index = event.getNewActiveIndex() - 1;
        this.state.domainObject[fieldBinding] = subItemList[index][ConstantKey.PROPERTY_MAP][ConstantKey.VALUE];
    }
};