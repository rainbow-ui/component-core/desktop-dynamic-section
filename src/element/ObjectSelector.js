import CleanFromClientConverter from '../utils/CleanFromClientCoverter';
import ConstantKey from '../constant/ConstantKey';
import BaseElement from './BaseElement';
import CommonAction from '../action/CommonAction';


export default class ObjectSelector extends BaseElement {
    generateAdditionalAttr() {
        let node = this.node, fieldBinding,
            commonAction = new CommonAction();
        let result = commonAction.generateContainerProps(this, node);
        _.assign(result, {
            modelValue: this.state.domainObject,
            parent: this,
            domainObject: this.state.domainObject,
			ref: this.generateRef(node)
        });
        if (node[ConstantKey.PROPERTY_MAP][ConstantKey.CLEAN_FROM_CLIENT]) {
            fieldBinding = {
                fieldBinding: {
                    field: ConstantKey.REAL_CLEAN_FROM_CLIENT,
                    converter: CleanFromClientConverter
                }
            };
        } else {
            if (node[ConstantKey.FIELD_BINDING] && node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD]
                && node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME]) {
                fieldBinding = {
                    fieldBinding: {field:node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME]}
                };
            }
        }
        result = _.assign(result, fieldBinding);
        return result;
    }
};