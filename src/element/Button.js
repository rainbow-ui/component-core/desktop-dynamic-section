import ConstantKey from '../constant/ConstantKey';
import ConstantEvent from '../constant/ConstantEvent';
import BaseElement from '../element/BaseElement';
import Event from '../template/Event';
import DynamicSectionUtil from '../generator/DynamicSectionUtil';
import ActionDefinition from '../event/ActionDefinition';

export default class ButtonElement extends BaseElement{
	constructor(node) {
		super(node);
		this.dynamicSectionUtil = new DynamicSectionUtil();
		this.CREATE = this.CREATE;
		this.EDIT = this.EDIT;
		this.DELETE = this.DELETE;
		this.SAVE = this.SAVE;
		this.CANCEL = this.CANCEL;
		this.CREATE_INLINE = this.CREATE_INLINE;
		this.CALCULATE = this.CALCULATE;
		this.getTargetSectionModel = this.getTargetSectionModel;
		this.modelObject = {};
	}

	getTargetSectionModel(targetSection, targetTable, node){
		let targetNode;
		if (targetTable) {
			targetNode = this.dynamicSectionUtil.findNodeRecursively(this.state.pageSchema, targetTable);
		} else if(targetSection) {
			targetNode = this.dynamicSectionUtil.findNodeRecursively(this.state.pageSchema, targetSection)
		}
		if (targetNode) {
			let [modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode] = [targetNode[ConstantKey.DATA_MODEL_NAME], targetNode[ConstantKey.DATA_OBJECT_CODE],
				targetNode[ConstantKey.SCOPE_MODEL_NAME], targetNode[ConstantKey.SCOPE_OBJECT_CODE], targetNode[ConstantKey.PARENT_MODEL_NAME], targetNode[ConstantKey.PARENT_OBJECT_CODE]];

			this.modelObject[node[ConstantKey.CODE]] = [modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode];
		} else {
			this.modelObject[node[ConstantKey.CODE]] = new Array(6);
		}
	}

	bindValue() {
		let node = this.node, result;
		if (node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_SECTION_CODE] || node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_TABLE_CODE]) {
			let targetSectionCode = node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_SECTION_CODE];
			let targetTableCode = node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_TABLE_CODE];
			if (!this.modelObject[node[ConstantKey.CODE]]) this.getTargetSectionModel(targetSectionCode, targetTableCode, node);
			result = {
				modelName: this.modelObject[node[ConstantKey.CODE]][0],
				objectCode: this.modelObject[node[ConstantKey.CODE]][1],
				scopeModelName: this.modelObject[node[ConstantKey.CODE]][2],
				scopeObjectCode: this.modelObject[node[ConstantKey.CODE]][3],
				parentModelName: this.modelObject[node[ConstantKey.CODE]][4],
				parentObjectCode: this.modelObject[node[ConstantKey.CODE]][5]
			};
			if (targetTableCode) {
				result.targetTableCode = targetTableCode;
			}
		}
		return result;
	}

	createDefaultEvent(customizedActions) {
		let func, event,
			node = this.node,
			targetTable = node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_TABLE_CODE],
			targetDialog = node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_SECTION_CODE],
			clickAction = customizedActions.onClick;
		if (clickAction && ConstantEvent.DEFAULT_EVENTS.indexOf(clickAction) >= 0) {
			func = this[clickAction](targetDialog, targetTable);
			// return {onClick: this[actionCode](node, targetDialog, targetTable)};
			event = Event.onClick.apply(this, [func]);
			return event;
		}

		if (!targetDialog || customizedActions.onClick) return;
		func = `this.refs.${targetDialog}.showDialog(null)`;
		event = Event.onClick.apply(this, [func]);
		return event;
	}

	CREATE(targetDialog, targetTable) {
		let node = this.node;
		console.assert(targetDialog != null, "targetSectionCode is not defined");
		if (!this.modelObject[node[ConstantKey.CODE]]) this.getTargetSectionModel(targetDialog, targetTable, node);
		// let targetNode = this.dynamicSectionUtil.findNodeRecursively(this.state.pageSchema, targetTable);
		let modelName =this.modelObject[node[ConstantKey.CODE]][0];
		let objectCode =this.modelObject[node[ConstantKey.CODE]][1];
		let scopeModelName = this.modelObject[node[ConstantKey.CODE]][2];
		let scopeObjectCode = this.modelObject[node[ConstantKey.CODE]][3];
		let parentModelName = this.modelObject[node[ConstantKey.CODE]][4];
		let parentObjectCode = this.modelObject[node[ConstantKey.CODE]][5];
		let func = () => ActionDefinition.create(targetDialog, modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, this.state.policy);
		return func;
	}

	CREATE_INLINE(targetDialog, targetTable) {
		let node = this.node;
		if (!this.modelObject[node[ConstantKey.CODE]]) this.getTargetSectionModel(targetDialog, targetTable, node);
		// let targetNode = this.dynamicSectionUtil.findNodeRecursively(this.state.pageSchema, targetTable);
		let modelName =this.modelObject[node[ConstantKey.CODE]][0];
		let objectCode =this.modelObject[node[ConstantKey.CODE]][1];
		let scopeModelName = this.modelObject[node[ConstantKey.CODE]][2];
		let scopeObjectCode = this.modelObject[node[ConstantKey.CODE]][3];
		let parentModelName = this.modelObject[node[ConstantKey.CODE]][4];
		let parentObjectCode = this.modelObject[node[ConstantKey.CODE]][5];
		let func = () => ActionDefinition.create_inline(targetDialog, targetTable, modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, this.state.policy, this.state.dialogObject,this.props.cuzObjectTemplate,this.props.cuzObjectList);
		return func;
	}

	EDIT(targetDialog, targetTable) {
		console.assert(targetDialog != null, "targetSectionCode is not defined");
		let func = () => ActionDefinition.edit(targetTable, targetDialog);
		return func;
	}

	DELETE(targetDialog, targetTable) {
		let node = this.node;
		console.assert(targetDialog != null, "targetSectionCode is not defined");
		if (!this.modelObject[node[ConstantKey.CODE]]) this.getTargetSectionModel(targetDialog, targetTable, node);
		// let targetNode = this.dynamicSectionUtil.findNodeRecursively(this.state.pageSchema, targetTable);
		let modelName =this.modelObject[node[ConstantKey.CODE]][0];
		let objectCode =this.modelObject[node[ConstantKey.CODE]][1];
		let scopeModelName = this.modelObject[node[ConstantKey.CODE]][2];
		let scopeObjectCode = this.modelObject[node[ConstantKey.CODE]][3];
		let parentModelName = this.modelObject[node[ConstantKey.CODE]][4];
		let parentObjectCode = this.modelObject[node[ConstantKey.CODE]][5];
		let func = () => ActionDefinition.delete(targetDialog, targetTable, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, this.state.policy, this.state.dialogObject,this.props.cuzObjectList);
		return func;
	}

	SAVE(targetDialog, targetSection) {
		let func;
		if (targetDialog) {
			func = () => ActionDefinition.saveDialog(targetDialog, this.props.modelObject.scopeModelName, this.props.modelObject.scopeObjectCode, this.props.modelObject.parentModelName,
				this.props.modelObject.parentObjectCode, this.state.domainObject, this.state.policy);
		} else {
			func = () => ActionDefinition.savePolicy(this.state.policy);
		}
		return func;
	}

	CALCULATE() {
		let func = () => ActionDefinition.calculate(this.state.policy);
		return func;
	}

	CANCEL(targetDialog) {
		let func = () => ActionDefinition.cancel(targetDialog);
		return func;
	}
};