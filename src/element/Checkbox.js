import Binder from 'react-binding';
import BaseElement from '../element/BaseElement';
import ConstantKey from '../constant/ConstantKey';
import CleanFromClientConverter from '../utils/CleanFromClientCoverter';
import StoreAdapter from '../utils/StoreAdapter';

export default class CheckboxElement extends BaseElement {
    constructor(node) {
        super(node);
        this.setDefaultValue = this.setDefaultValue;
    }

    setDefaultValue(node) {
        let isSaved = true;
        if (node[ConstantKey.PROPERTY_MAP][ConstantKey.CLEAN_FROM_CLIENT]) {
            if (this.state.domainObject && !this.state.domainObject[ConstantKey.CLEAN_FROM_CLIENT]) {
                //let parentObject = PolicyStore.getParentDomainObjectByChildDomainObject(this.state.domainObject, this.state.policy);
                let parentObject = StoreAdapter.getParentDomainObjectByChildDomainObject(this.state.domainObject, this.state.policy);
                if (parentObject && parentObject.TempData && parentObject.TempData[ConstantKey.AUTO_CREATED] === 'Y') {
                    isSaved = false;
                }
                if (isSaved) {
                    if (this.state.domainObject.TempData && this.state.domainObject.TempData[ConstantKey.AUTO_CREATED] === 'Y') {
                        this.state.domainObject[ConstantKey.CLEAN_FROM_CLIENT] = 'Y';
                    } else {
                        this.state.domainObject[ConstantKey.CLEAN_FROM_CLIENT] = 'N';
                    }
                } else {
                    if (node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE]) {
                        this.state.domainObject[ConstantKey.CLEAN_FROM_CLIENT] = CleanFromClientConverter.not(node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE]);
                    }
                }
            }
        }
    }

    bindValue() {
        let node = this.node, result;
        let propertyMap = node[ConstantKey.PROPERTY_MAP];
        let domainObject = this.state.domainObject;
        this.setDefaultValue(node);
        if (propertyMap && propertyMap[ConstantKey.CLEAN_FROM_CLIENT]) {
            result = {
                // valueLink: Binder.bindToState(this, 'domainObject', UnicornUiLogicConstants.CLEAN_FROM_CLIENT, CleanFromClientConverter.coverter)
                valueLink: Binder.bindToState(this, 'domainObject', ConstantKey.CLEAN_FROM_CLIENT, CleanFromClientConverter)
            };
        } else {
            if (node[ConstantKey.FIELD_BINDING] && node[ConstantKey.FIELD_BINDING][ConstantKey.NAME]) {
                result = {
                    //valueLink: Binder.bindToState(this, 'domainObject', node[ConstantKey.FIELD_BINDING][ConstantKey.NAME])
                    model: domainObject,
                    property: node[ConstantKey.FIELD_BINDING][ConstantKey.NAME]
                };
            } else if (node[ConstantKey.DATA_MODEL_NAME] != ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION
                && node[ConstantKey.DATA_OBJECT_CODE] != ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION
                && node[ConstantKey.FIELD_VALUEBINDING]) {
                result = {
                    //valueLink: Binder.bindToState(this, 'domainObject', node[ConstantKey.FIELD_VALUEBINDING])
                    model: domainObject,
                    property: node[ConstantKey.FIELD_VALUEBINDING]
                }
            } else if (propertyMap
                && propertyMap[ConstantKey.CUSTOMIZATION_FIELD_BINDING]
                && (propertyMap[ConstantKey.CODE_TABLE] || propertyMap[ConstantKey.SINGLE])) {
                if (propertyMap[ConstantKey.CUSTOMIZATION_OBJECT_RELATION]) {
                    let cuzObjectRelation = propertyMap[ConstantKey.CUSTOMIZATION_OBJECT_RELATION];
                    let cuzModel = this.findModelByCuzObjectRelation(domainObject, cuzObjectRelation, propertyMap[ConstantKey.CUSTOMIZATION_FIELD_BINDING]);
                    result = {
                        model: cuzModel,
                        property: propertyMap[ConstantKey.CUSTOMIZATION_FIELD_BINDING]
                    }
                } else {
                    result = {
                        model: domainObject,
                        property: propertyMap[ConstantKey.CUSTOMIZATION_FIELD_BINDING]
                    }
                }
            }
        }
        return result;
    }
};