import ConstantKey from '../constant/ConstantKey';
import ConstantNode from '../constant/ConstantNode';
import BaseElement from './BaseElement';
import BaseContainer from '../container/BaseContainer';

export default class WizardElement extends BaseElement{
	constructor(node) {
		super(node);
		this.createPropsComp = this.createPropsComp;
	}

	createSpecialProp(component) {
		let node = this.node, wizardStepButtonNode;
		let length = node[ConstantKey.NODE_LIST].length;
		let subNode;
		for (let i = 0; i < length; i++) {
			subNode = node[ConstantKey.NODE_LIST][i];
			if (subNode[ConstantKey.COMPONENT_CODE] === ConstantNode.WIZARD_STEP_BUTTON) {
				wizardStepButtonNode = subNode;
				break;
			}
		}
		if (wizardStepButtonNode) {
			let subComp = this.createPropsComp.call(this, subNode);
			component.props.wizardStepButton = subComp;
		}
	}

	createPropsComp(node) {

		class component extends BaseContainer{
			constructor(props) {
				super(props);
			}

			generateSubComponent(node) {
				let result = [];

				let ComponentFactory = require('../template/ElementFactory');

				node[ConstantKey.NODE_LIST].forEach(subNode => {
					// console.log('ComponentFactory', ComponentFactory);
					let element = ComponentFactory.getComponent(subNode, this);
					let context = this;
					_.extend(context, element);
					let component = element.getComponent.apply(context, [subNode]);
					let generator = require('../generator/DynamicSectionGenerator');

					context = _.extend(this, generator);
					let children = generator.generateSubNode.apply(context, [subNode[ConstantKey.NODE_LIST], subNode]);
					component.props.children = children;
					result.push(component);
				});
				return result;
			}

			render() {
				let result = this.generateSubComponent(node);
				return <div>{result}</div>
			}
		}
		//console.log('Special Component', result);
		return component;
	}

};