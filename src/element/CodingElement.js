import ConstantNode from '../constant/ConstantNode';
import BaseElement from './BaseElement';
import ComponentTemplate from '../template/ComponentTemplate';
import ConstantKey from './../constant/ConstantKey'

export default class CodingElement extends BaseElement{
	constructor(node) {
		super(node);
	}

	getComponent() {
		var CodingComp = this.component;
		if (!CodingComp) {
			return ComponentTemplate[ConstantNode.CELL](this.generateRef(this.node));
		}
		let result = <CodingComp componentType={ConstantKey.CODING_COMPONENT} policy={this.state.policy} domainObject={this.state.domainObject} dialogObject={this.state.dialogObject} ref={this.generateRef(this.node)}/>;
		if (this.props.codingParameter) {
			let keys = Object.keys(this.props.codingParameter);
			keys.forEach(key => result.props[key] = this.props.codingParameter[key]);
		}
		if(this.node&&this.node.PropertyMap&&this.node.PropertyMap.colspan){
			result.props.colspan = this.node.PropertyMap.colspan
		}
		return result;
	}

};