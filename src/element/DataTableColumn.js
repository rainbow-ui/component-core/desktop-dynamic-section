import BaseElement from './BaseElement';
import ConstantKey from '../constant/ConstantKey';
import LookupDSL from '../event/lookup/UnicornDSL';
import LookupModelFunc from '../event/LookUpModelFunc';
import EndorsementFunc from '../event/EndorsementFunc';

export default class DataTableColumnElement extends BaseElement {
	constructor(node) {
		super(node);
		this.renderFunction = this.renderFunction;
	}

	generateAttribute() {
		let node = this.node, result = {}, regExp = this.propertyRegExpr(), _self = this;
		for (let key in node[ConstantKey.PROPERTY_MAP]) {
			let value = node[ConstantKey.PROPERTY_MAP][key];
			if (value === null || value === undefined) {
				continue;
			}
			if (ConstantKey.SKIP_ATTRIBUTE.indexOf(key) >= 0) {
				continue;
			}
			if (typeof(value) === 'string' && regExp.test(value)) {
				let [policy, domainObject, dialogObject] = [this.state.policy, this.state.domainObject, this.state.dialogObject];
				let lookup = expression => LookupDSL.executeUiLookupDsl(expression, domainObject, policy);
				let lookupModel = (modelName, objectCode, scopeModelName, scopeObjectCode, nodeObject) => LookupModelFunc.lookupModel(modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject, policy);
				let isEndo = () => EndorsementFunc.isEndo(policy);
				let getEndoType = () =>EndorsementFunc.getEndoType();
				if (key === ConstantKey.VALUE) {
					result['render'] = this.renderFunction(node, value);
				}
				else result[key] = eval(value.match(regExp)[1]);
			} else {
				result[key] = value + '';
			}
		}
		return result;
	}

	renderFunction(node, value) {
		let Generator = require('../generator/DynamicSectionGenerator');
		let subNodeList = node[ConstantKey.NODE_LIST], regExp = this.propertyRegExpr();
		return data => {
			let record = data;
			let componentList = Generator.generateSubNode.apply(this, [subNodeList, node]);
			componentList.forEach(component => {
				component.props.value = eval(value.match(regExp)[1]);
			});
			return (
				<div>{componentList}</div>
			)
		};
	}
}