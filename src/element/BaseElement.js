import { CodeTable } from 'rainbowui-desktop-core';
import ComponentTemplate from '../template/ComponentTemplate';
import ConstantKey from '../constant/ConstantKey';
import ConstantNode from '../constant/ConstantNode';
import ConstantEventDefinition from '../constant/ConstantEventDefinition';
import Event from '../template/Event';
import LookupDSL from '../event/lookup/UnicornDSL';
import LookupModelFunc from '../event/LookUpModelFunc';
import EndorsementFunc from '../event/EndorsementFunc';
import StoreAdapter from '../utils/StoreAdapter';
import { PolicyStore, EndorsementStore } from 'rainbow-desktop-sdk';
import { UIMessageHelper } from "rainbowui-desktop-core";
import CodeTableFunc from '../event/CodeTableFunc';

export default class BaseElement {
	constructor(node) {
		this.generateProps = this.generateProps;
		this.bindValue = this.bindValue;
		this.generateCodeTable = this.generateCodeTable;
		this.generateEvent = this.generateEvent;
		this.callEvent = this.callEvent;
		this.createDefaultEvent = this.createDefaultEvent;
		this.createSpecialProp = this.createSpecialProp;
		this.propertyRegExpr = this.propertyRegExpr;
		this.generateAdditionalAttr = this.generateAdditionalAttr;
		this.generateAttribute = this.generateAttribute;
		this.findModelByCuzObjectRelation = this.findModelByCuzObjectRelation;
		this.node = node;
	}

	getComponent() {
		let node = this.node;
		let component = ComponentTemplate[node[ConstantKey.COMPONENT_CODE]]();
		let props = this.generateProps();
		return [component, props];
	}

	propertyRegExpr() {
		return /^{(.*)}$/;
	}

	//Generate attributes
	generateProps() {
        let node = this.node, result = {}, regExp = this.propertyRegExpr(),
            codeTableProp, event;

        result = this.generateAttribute();

        let additionalAttr = this.generateAdditionalAttr();
        additionalAttr && _.extend(result, additionalAttr);

        let bindValue = this.bindValue();
        bindValue && _.extend(result, bindValue);

        codeTableProp = this.generateCodeTable();
        codeTableProp && _.extend(result, codeTableProp);
        
        if (ConstantNode.COMPONENTS_HAS_CODE_TABLE.indexOf(node[ConstantKey.COMPONENT_CODE]) > -1) {
            if (result["valueOption"] && result["codeTable"] && result["codeTable"].indexOf("{") < 0) {
                delete result["codeTable"];
            }
            if ( (result[ConstantKey.CODE_TABLE.toLowerCase()] || result[ConstantKey.CODE_TABLE]) && result["codeTableName"]) {
                delete result[ConstantKey.CODE_TABLE.toLowerCase()];
                delete result[ConstantKey.CODE_TABLE];
            }
        }

        event = this.generateEvent();
        event && _.extend(result, event);

        return result;
        //_.extend(component.props, result);

        //this.createSpecialProp(component);
    }


	generateAttribute() {
		let node = this.node, result = {}, regExp = this.propertyRegExpr();
		for (let key in node[ConstantKey.PROPERTY_MAP]) {
			let value = node[ConstantKey.PROPERTY_MAP][key];
			if (value === null || value === undefined) {
				continue;
			}
			if (ConstantKey.SKIP_ATTRIBUTE.indexOf(key) >= 0) {
				continue;
			}
			if (typeof (value) === 'string' && regExp.test(value)) {
				let [policy, domainObject, dialogObject, endo] = [this.state.policy, this.state.domainObject, this.state.dialogObject, EndorsementStore];
				let lookup = expression => LookupDSL.executeUiLookupDsl(expression, domainObject, policy);
				let lookupModel = (modelName, objectCode, scopeModelName, scopeObjectCode, nodeObject) => LookupModelFunc.lookupModel(modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject, policy);
				let isEndo = () => EndorsementFunc.isEndo(policy);
				let getEndoType = () =>EndorsementFunc.getEndoType();
				let generateConditionMap = (conditionMapInfo) =>CodeTableFunc.generateConditionMap(conditionMapInfo);
				result[key] = eval('(' + value.match(regExp)[1] + ')');
			} else {
				result[key] = value + '';
			}
		}
		return result;
	}

	generateAdditionalAttr() {
		let node = this.node;
		let result = {
			id: (node.PropertyMap&&node.PropertyMap.id)?node.PropertyMap.id:this.generateId(node),
			refName: this.generateRef(node),
			modelValue: this.state.domainObject,
			ref: this.generateRef(node),
			parent: this
		};
		return result;
	}

	bindValue() {
		let node = this.node, result;
		let domainObject = this.state.domainObject;
		// if (node[ConstantKey.FIELD_BINDING] && node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD]
		// 	&& node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME]) {
		// if(node[ConstantKey.FIELD_BINDING]&&node[ConstantKey.FIELD_BINDING][ConstantKey.NAME]){
		if (node[ConstantKey.FIELD_BINDING]) {
			result = {
				model: domainObject,
				property: node[ConstantKey.FIELD_BINDING]["FieldName"]
			}
		} else {
			let propertyMap = node[ConstantKey.PROPERTY_MAP];
			if (propertyMap && propertyMap[ConstantKey.CUSTOMIZATION_FIELD_BINDING]) {
				let cuzModel = null;
				let cuzField = propertyMap[ConstantKey.CUSTOMIZATION_FIELD_BINDING];
				let cuzObjectRelation = propertyMap[ConstantKey.CUSTOMIZATION_OBJECT_RELATION];
				if (cuzObjectRelation) {
					cuzModel = this.findModelByCuzObjectRelation(domainObject, cuzObjectRelation, cuzField);
				} else {
					cuzModel = domainObject;
				}
				result = {
					model: cuzModel,
					property: cuzField
				}
			} else if (node[ConstantKey.FIELD_VALUEBINDING]) {
				result = {
					model: domainObject,
					property: node[ConstantKey.FIELD_VALUEBINDING]
				}
			}
		}
		return result;
	}

	
	findModelByCuzObjectRelation(cuzObject, cuzObjectRelation, cuzField) {
		let tempObject = cuzObject;
		let relationArray = cuzObjectRelation.split('.');
		for (let i = 0; i < relationArray.length; i++) {
			let relation = relationArray[i];
			if(tempObject){
				if (relation.indexOf("[") != -1 && relation.indexOf("]") != -1) {
					let relationName = relation.substring(0, relation.indexOf("["));
					let tempIndex = relation.substring(relation.indexOf("[") + 1, relation.indexOf("]"));
					if ((tempObject[relationName] instanceof Array ) && tempObject[relationName][tempIndex]) {
						tempObject = tempObject[relationName][tempIndex];
					} else {
						console.error(`Error in hanle relation of customization object and field, customizationObject: ${cuzObject}, customizationField: ${cuzField}, Customization Object Relation: ${relationArray.join(".")}`);
					}
				} else {
					tempObject = tempObject[relation];
				}
			}else{
				tempObject = cuzObject;
				break
			}
			if (i == relationArray.length - 1) {
				break
			}
		}
		return tempObject;
	}	

	generateCodeTable() {
		let node = this.node, scopeObject = this.state.scopeObject;
		let fieldBinding = node[ConstantKey.FIELD_BINDING];
		let propertyMap = node[ConstantKey.PROPERTY_MAP];
		let codeTableName, codeTableId, conditionMap;
		let pos = ConstantNode.COMPONENTS_HAS_CODE_TABLE.indexOf(node[ConstantKey.COMPONENT_CODE]);
		if (pos < 0) {
			return;
		}
		// codeTable
		if (propertyMap && propertyMap[ConstantKey.CODE_TABLE] && propertyMap[ConstantKey.CODE_TABLE].indexOf("{") > -1) {
			return 
		}
		//valueoption
		if (node[ConstantKey.PROPERTY_MAP] && node[ConstantKey.PROPERTY_MAP][ConstantKey.CODE_TABLE] && node[ConstantKey.PROPERTY_MAP][ConstantKey.CODE_TABLE].indexOf(",") > -1) {
			let uiTransValueOption = node[ConstantKey.PROPERTY_MAP][ConstantKey.CODE_TABLE];
			let uiValueOptionMap = uiTransValueOption.split(",");
			return { valueOption: uiValueOptionMap };
		} else if (node[ConstantKey.FIELD_BINDING] && node[ConstantKey.FIELD_BINDING][ConstantKey.VALUE_OPTIONS]) {
			let transValueOption = node[ConstantKey.FIELD_BINDING][ConstantKey.VALUE_OPTIONS]
			let valueOptionMap = transValueOption.split(",")
			return { valueOption: valueOptionMap }
		} 
		// conditionMap
		if (node[ConstantKey.PROPERTY_MAP] && node[ConstantKey.PROPERTY_MAP]["conditionMap"]) {
			conditionMap = {};
			if(UIC.dynamicSectionConditionMap){
				conditionMap = UIC.dynamicSectionConditionMap
			}
		}
		// if (node[ConstantKey.PROPERTY_MAP] && node[ConstantKey.PROPERTY_MAP]["conditionMap"]) {
		// 	conditionMap = {};
		// 	let conditionFieldMap = node[ConstantKey.PROPERTY_MAP]["conditionMap"];
		// 	let conditionTran = conditionFieldMap.replace(/\[/g, "");
		// 	conditionTran = conditionTran.replace(/\]/g, "");
		// 	conditionTran = conditionTran.replace(/\'/g, "\"");
		// 	let conditionOption;
		// 		conditionOption = JSON.stringify(conditionTran)
		// 		conditionOption = JSON.parse(conditionOption)
		// 		conditionMap = JSON.parse(conditionOption)
		// }
		//codeTableName
		if (propertyMap && propertyMap[ConstantKey.CODE_TABLE] && propertyMap[ConstantKey.CODE_TABLE].indexOf("{") == -1) {
			codeTableName = propertyMap[ConstantKey.CODE_TABLE]
			return {codeTableName:codeTableName, conditionMap: conditionMap}
		}else if (!codeTableName && propertyMap && propertyMap[ConstantKey.CODE_TABLE.toLocaleLowerCase()] && propertyMap[ConstantKey.CODE_TABLE.toLocaleLowerCase()].indexOf("{") == -1){
			codeTableName = propertyMap[ConstantKey.CODE_TABLE.toLocaleLowerCase()];
			return {codeTableName:codeTableName, conditionMap: conditionMap}
		} else if (!codeTableName && fieldBinding && fieldBinding[ConstantKey.TEMP_CODE_TABLE_NAME]){
			codeTableName = fieldBinding && fieldBinding[ConstantKey.TEMP_CODE_TABLE_NAME];
			return {codeTableName:codeTableName, conditionMap: conditionMap}
		}
		//codeTableId
		// if(fieldBinding && fieldBinding[ConstantKey.CODE_TABLE_ID]){
		// 	codeTableId = fieldBinding[ConstantKey.CODE_TABLE_ID];
		// 	return {codeTableId:codeTableId, conditionMap: conditionMap}
		// }
		//codeTableName
		if(fieldBinding && fieldBinding[ConstantKey.CODE_TABLE_NAME]){
			codeTableName = fieldBinding[ConstantKey.CODE_TABLE_NAME];
			return {codeTableName:codeTableName, conditionMap: conditionMap}
		}
		return { codeTable: this.codeTableAction.getEmptyCodeTable() };
	}

	//Generate events
	generateEvent() {
		let node = this.node, result = {}, eventProp;
		let eventKey = null, eventAction = null, hasOnChange = false;
		node[ConstantKey.EVENT_LIST] && node[ConstantKey.EVENT_LIST].forEach(event => {
			if (event[ConstantKey.COMM_PK] && event[ConstantKey.EVENT_CODE_KEY] == ConstantKey.EVENT_TABLE_TAIL_DELETE) {
				return null
			} else {
				for (let key in event) {
					if (key === ConstantKey.EVENT_CODE_KEY) eventKey = event[key];
					if (key === ConstantKey.EVENT_ACTION_KEY) eventAction = event[key];
				}
				if (ConstantEventDefinition.EVENTS_DEFINITION_FOR_CONTAINER.indexOf(eventKey) < 0) {
					if (eventKey === 'onChange') {
						hasOnChange = true;
					}
					let eventDef = {};
					eventDef[eventKey] = eventAction;
					eventProp = this.createDefaultEvent(eventDef);
					if (!eventProp) {
						if (eventKey && eventAction) {
							eventProp = this.callEvent(Event[eventKey], this, [eventAction]);
							_.extend(result, eventProp);
						}
					} else {
						_.extend(result, eventProp);
					}
				}
			}
		});
		if (!hasOnChange && ConstantNode.COMPONENTS_NO_ONCHANGE.indexOf(node[ConstantKey.COMPONENT_CODE]) < 0) {
			eventProp = this.callEvent(Event['onChangeDefault'], this);
			_.extend(result, eventProp);
		}
		// this.createDefaultEvent(node, result);
		return result;
	}

	callEvent(fn, context, args) {
		// _.extend(context, this);
		return fn.apply(context, args);
	}

	createDefaultEvent(customizedActions) {
	}

	createSpecialProp(component) {
	}
};