const EventEmitter = require('events');
const eventChangeEventEmitter = new EventEmitter();
var publicServerUrl = '';

module.exports = {
	setPublicServerUrl:function(url){
		if (url.substr(url.length - 2, 2) == '//') {
			url = url.substr(0, url.length - 1);
		}
		publicServerUrl = url;
		eventChangeEventEmitter.emit('changeEnv');
	},

	getPublicServerUrl:function(){
		return publicServerUrl;
	},

	addEventListener:function(callBack){
		eventChangeEventEmitter.on('changeEnv',callBack);
	}
};

