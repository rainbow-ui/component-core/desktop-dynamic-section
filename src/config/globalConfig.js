//import {UiConfigEnv} from 'unicorn-ui-policy-logic';
import env from './env';

function resetEnv() {
	var pubServerUrl = env.getPublicServerUrl();
	console.log("pubServerUrl...", pubServerUrl);
	Config.baseUrl = pubServerUrl;
    Config.getProduct = pubServerUrl + 'product/prd/v1/getProductIdByCodeAndVersion/';
    Config.getProductById = pubServerUrl + 'product/prd/v1/product/';
    Config.getMasterProduct = pubServerUrl + 'product/prd/v1/productMaster/productMasterId?productMasterId={productMasterId}';
    Config.nodeEventUrl = pubServerUrl + 'ui-avm/ui/v1/nodeEvent/';
    Config.getPageById = pubServerUrl + 'ui-avm/ui/page/v1/byPageId?pageId={pageId}';
    //Config.getPageByNameProduct = pubServerUrl + 'ui-avm/ui/page/v1/byNameAndContextId?pageName={pageName}&productCode={productCode}&productVersion={productVersion}&contextId={contextId}';
    Config.getPageByNameProduct = pubServerUrl + 'ui-avm/ui/page/v1/byNameAndContextId?pageName={pageName}&productCode={productCode}&productVersion={productVersion}&needTransCode={needTransCode}&contextId={contextId}';
    Config.getProductByDate = pubServerUrl + 'product/producteffective?productCode={productCode}&effDate={effectiveDate}';
    Config.getMultiCodeTables = pubServerUrl + 'dd/public/codetable/v1/data/multiple/list?codeTableId={codeTableIds}';
    Config.getCodeTableById = pubServerUrl + 'dd/public/codetable/v1/codeTableValue/byKeywordAndCodeTableId?keyword=&codeTableId={codeTableId}';
    Config.calculatePolicyNBFee = pubServerUrl + 'public/orchestration/dispatch/newbiz_calculateNBTransPremium';
    Config.groupPolicySearchCertificateUrl = pubServerUrl + 'pa/grouppolicy/inquiry/searchInsuredList';
    Config.getCodeTableByNameProduct = pubServerUrl + 'public/codetable/data/conditions';
    Config.getCodeTableByName = pubServerUrl + 'dd/public/codetable/v1/data/list/byName';
    Config.getProductElementListByProductIdAndTypeId = pubServerUrl + 'product/element/runtime/v1/getProductElementListByProductIdAndTypeId?productId={productId}&elementTypeId={elementTypeId}';
    Config.getPolicySchema = pubServerUrl + 'dd/public/dictionary/v1/generateFullUiResourceSchemaWithoutFieldNew?modelName={modelName}&objectCode={objectCode}&contextType={contextType}&referenceId={productId}';
    Config.getEntirePolicyByPolicyId = pubServerUrl + 'pa/pa/policy/v1/{policyId}';
    Config.getI18nDataByPageNameandContext = pubServerUrl + 'i18n/rb/v1/ui/load/prd';
    Config.getDataTableRecordListByName = pubServerUrl + 'dd/public/codetable/v1/findDataTableRecordListByDataTableName?dataTableName={dataTableName}';
	//UiConfigEnv.setPublicServerUrl(pubServerUrl);
}

var pubServerUrl = env.addEventListener(resetEnv);

var Config = {
};

module.exports = Config;
