import {UILabel, UICell, UILink, UITextarea, UIDialog, UIBox, UIButton, UIConfirmDialog, UISmartPanelGrid, UITab,
	UITabItem, UIRadio, UINumber, UICheckbox, UISeparator, UISelect, UIPage, UIText, UIDataTable, UIColumn, UICurrency,
	UIWizard, UIWizardStep, UIPercent, UIPassword, UIEmail, UISwitch, UIMenuBar, UISubMenu,
	UIMenuItem, UIBlank, UIUpdatePanel, UICard, UICardGroup, UIDateTimePicker, UISearch} from 'rainbowui-desktop-core';
import ObjectSelector from '../businessComponent/ObjectSelector';
// import { UIDateTimePicker } from "rainbowui-date-time-picker";
// import { UIDateTimePicker} from 'rainbowui-desktop-core';
// import {UISelect,UISearch} from 'rainbowui-search';

module.exports = {
	/*************************************************************/
	/* Basic Input */
	/*************************************************************/
	TEXT: function(nodeCode){
		return UIText;
	},

	PASSWORD: function(nodeCode){
		return UIPassword;
	},

	EMAIL:function(nodeCode){
		return UIEmail;
	},

	DATE:function(nodeCode){
		return UIDateTimePicker;
	},

	NUMBER:function(nodeCode){
		return UINumber;
	},

	CURRENCY:function(nodeCode){
		return UICurrency;
	},

	PERCENT:function(nodeCode){
		return UIPercent;
	},

	TEXTAREA:function(nodeCode){
		return UITextarea;
	},

	SELECT:function(nodeCode){
		return UISelect;
	},

	RADIO:function(nodeCode){
		return UIRadio;
	},

	CHECKBOX:function(nodeCode){
		return UICheckbox;
	},
	// SEARCH_CODE_TABLE:function(nodeCode){
	// 	return UISelect;
	// },
	UI_SEARCH:function(nodeCode){
		return UISearch;
	},

	/*************************************************************/
	/* Button */
	/*************************************************************/

	BUTTON: function(nodeCode){
		return UIButton;
	},

	LINK:function(nodeCode){
		return UILink;
	},

	BUTTON_FOR_TABLE: function(nodeCode){
		return UIButton;
	},

	/*************************************************************/
	/* AdvancedInput */
	/************************************************************/


	SWITCH:function(nodeCode){
		return UISwitch;
	},

	PANEL: function(nodeCode){
		return UICard;
	},

	SMART_PANEL_GRID: function(nodeCode){
		return UISmartPanelGrid;
	},

	PAGE: function(nodeCode){
		return UIPage;
	},

	WIZARD: function(nodeCode){
		return UIWizard;
	},

	WIZARD_STEP: function(nodeCode){
		return UIWizardStep;
	},

	CELL:function(nodeCode){
		return UICell;
	},

	TAB:function(nodeCode){
		return UITab;
	},

	TAB_ITEM:function(nodeCode){
		return UITabItem;
	},

	BOX:function(nodeCode){
		return UIBox;
	},

	CARD: function(nodeCode){
		return UICard;
	},

	CARD_GROUP: function(nodeCode){
		return UICardGroup;
	},


	/***************************************************/
	/* Data */
	/***************************************************/

	DATA_TABLE:function(nodeCode){
		return UIDataTable;
	},

	DATA_TABLE_COLUMN:function(nodeCode){
		return UIColumn;
	},

	/***************************************************/
	/* Menu */
	/**************************************************/

	MENU_BAR:function(nodeCode){
		return UIMenuBar;
	},

	SUB_MENU:function(nodeCode){
		return UISubMenu;
	},

	MENU_ITEM:function(nodeCode){
		return UIMenuItem;
	},

	/*****************************************************/
	/* Integration */
	/****************************************************/

	/****************************************************/
	/* Overlay */
	/***************************************************/

	DIALOG:function(nodeCode){
		return UIDialog;
	},

	CONFIRM_DIALOG:function(nodeCode){
		return UIConfirmDialog;
	},

	/****************************************************/
	/* Other */
	/***************************************************/

	LABEL:function(nodeCode){
		return UILabel;
	},

	PAGINATION:function(nodeCode){
		return (<div ref={nodeCode}></div>);
	},

	PROGRESS:function(nodeCode){
		return (<div ref={nodeCode}></div>);
	},

	CODE:function(nodeCode){
		return (<div ref={nodeCode}></div>);
	},

	CODE_MIRRIR:function(nodeCode){
		return (<div ref={nodeCode}></div>);
	},

	PARAM:function(nodeCode){
		return (<div ref={nodeCode}></div>);
	},

	SEPARATOR:function(nodeCode){
		return UISeparator;
	},

	OUTPUT_TEXT:function(nodeCode){
		return (<div ref={nodeCode}></div>);
	},

	BLANK: function(nodeCode){
		return UIBlank;
	},

	/*******************************************************/
	/* API */
	/******************************************************/

	UPDATE_PANEL:function(nodeCode){
		return UIUpdatePanel;
	},

    /*******************************************************/
    /* Business Component */
    /******************************************************/
    OBJECT_SELECTOR: function(nodeCode){
        return ObjectSelector;
    },

	SELECTOR: function(nodeCode){
		return ObjectSelector;
	},

	SUB_PAGE: function(nodeCode){
		return (<div ref={nodeCode}></div>)
	}
};
