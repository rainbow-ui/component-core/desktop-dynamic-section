import ConstantNode from '../constant/ConstantNode';
import ConstantKey from '../constant/ConstantKey';
import BaseElement from '../element/BaseElement';
import WizardElement from '../element/Wizard';
import CodingElement from '../element/CodingElement';
import ButtonElement from '../element/Button';
import CheckboxElement from '../element/Checkbox';
import SwitchElement from '../element/Switch';
import TabElement from '../element/Tab';
import ObjectSelector from '../element/ObjectSelector';
import DataTableColumn from '../element/DataTableColumn';

export default class ElementFactory {
	static getComponent(node, context) {
		var componentCode = node[ConstantKey.COMPONENT_CODE];
		var result;
		switch (componentCode) {
			case ConstantNode.WIZARD:
				result = new WizardElement(node);
				break;
			case ConstantNode.BUTTON:
				result = new ButtonElement(node);
				break;
			case ConstantNode.LINK:
				result = new ButtonElement(node);
				break;
			case ConstantNode.CHECKBOX:
				result = new CheckboxElement(node);
				break;
			case ConstantNode.SWITCH:
				result = new SwitchElement(node);
				break;
			case ConstantNode.OBJECT_SELECTOR:
				result = new ObjectSelector(node);
				break;
			case ConstantNode.SELECTOR:
				result = new ObjectSelector(node);
				break;
			case ConstantNode.TAB:
				result = new TabElement(node);
				break;
			case ConstantNode.DATA_TABLE_COLUMN:
				result = new DataTableColumn(node);
				break;
			case ConstantNode.CODING_COMPONENT:
				result = new CodingElement(node);
				let name = node[ConstantKey.PROPERTY_MAP][ConstantKey.PROP_NAME];
				if (context.props.codingComponent == null) {
					console.error('CodingComponent is null');
				} else {
					result.component = context.props.codingComponent[name];
					if (context.props.codingParameter) {
						let keys = Object.keys(context.props.codingParameter);
						if (keys.indexOf(name) >= 0) {
							let parameters = context.props.codingParameter[name];
							let paramKeys = Object.keys(parameters);
							result.parameters = {};
							paramKeys.forEach(paramKey => result.parameters[paramKey] = parameters[paramKey]);
						}
					}
					if (!result.component) {
						console.error('Cannot find coding component:' + name);
					}
				}
				break;
			default:
				result = new BaseElement(node);
		}
		return result;
	}
}