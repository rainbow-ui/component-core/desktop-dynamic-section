import ConstantNode from '../constant/ConstantNode';
import ConstantKey from '../constant/ConstantKey';
import BaseContainer from '../container/BaseContainer';
import DataTable from '../container/DataTable';
import Wizard from '../container/Wizard';
import Dialog from '../container/Dialog';
import Tab from '../container/Tab';
import SubPage from '../container/SubPage';

export default class ContainerFactory {
	static getContainer(node, context) {
		var componentCode = node[ConstantKey.COMPONENT_CODE];
		var result;
		let attribute = context.commonAction.generateContainerProps(context, node);
		switch (componentCode) {
			case ConstantNode.DATA_TABLE:
				result = <DataTable {...attribute} />;
				break;
			case ConstantNode.WIZARD:
				result = <Wizard {...attribute} />;
				break;
			case ConstantNode.DIALOG:
				result = <Dialog {...attribute} />;
				break;
            case ConstantNode.TAB:
                result = <Tab {...attribute} />;
                break;
			case ConstantNode.SUB_PAGE:
				result = <SubPage {...attribute} />;
				break;
			default:
				result = <BaseContainer {...attribute} />;
		}
		return result;
	}
}