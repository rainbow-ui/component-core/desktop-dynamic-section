module.exports = {
	onClick: function(func){
		return this.callElementEvent('onClick', func)
	},

	onChange: function(func){
		return this.callElementEvent('onChange', func + '\nthis.setState({});');
	},

	//on change default function
	onChangeDefault: function(){
		return this.callElementEvent('onChange', 'this.setState({})');
	},

	onTabChange: function(func){
		return this.callElementEvent('onTabChange', func)
	},

	onBeforeTabChange: function(func){
		return this.callElementEvent('onBeforeTabChange', func)
	},

	onAfterTabChange: function(func){
		return this.callElementEvent('onAfterTabChange', func)
	},

	onSelectAll: function(func){
		return this.callElementEvent('onSelectAll', func)
	},

	onBlur: function(func){
		return this.callElementEvent('onBlur', func + '\nthis.setState({});')
	},

	onFocus: function(func){
		return this.callElementEvent('onFocus', func)
	},

	onRowSelect: function(func){
		return this.callElementEvent('onRowSelect', func)
	},

	onClose: function(func){
		return this.callElementEvent('onClose', func)
	},

	onToggle: function(func) {
		return this.callElementEvent('onToggle', func)
	},

	onHeaderTitleClick: function(func){
		return this.callElementEvent('onHeaderTitleClick', func)
	}
};