var Ajax = require("./Ajax");

module.exports = {

    /**
     * http post
     * @param url
     * @param data
     * @param settings optional
     * @returns {*}
     */
    doPost: function (url, data, settings) {
        Ajax.ajax($.extend({
            method: "POST",
            dataType: "json",
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=UTF-8"
        }, settings, {
            url: url,
            data: JSON.stringify(data)
        }));
    },

    /**
     * http put
     * @param url
     * @param data
     * @param settings optional
     * @returns {*}
     */
    doPut: function (url, data, settings) {
        Ajax.ajax($.extend({
            method: "PUT",
            dataType: "json",
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=UTF-8"
        }, settings, {
            url: url,
            data: JSON.stringify(data)
        }));
    },

    /**
     * http get
     * @param url
     * @param data
     * @param settings
     * @returns {*}
     */
    doGet: function (url, data, settings) {
        Ajax.ajax($.extend({
            method: "GET",
            dataType: "json",
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=UTF-8"
        }, settings, {
            url: url,
            data: data
        }));
    },

    /**
     * http delete
     * @param url
     * @param data
     * @param settings optional
     * @returns {*}
     */
    doDelete: function (url, data, settings) {
        Ajax.ajax($.extend({
            method: "DELETE",
            dataType: "json",
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=UTF-8"
        }, settings, {
            url: url,
            data: data
        }));
    },

    getJSON: function(url){
        var json = null;
        $.ajax({
            type: "get",
            async: false,
            url: url,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                json = data;
            },
            error: function (err) {

            }
        });
        return json;
    }
};