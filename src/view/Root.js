import DynamicSection from "../generator/DynamicSection";

export default class Login extends React.Component {

    constructor(props) {
        super(props);
    }

    mockJson() {
        return {
            "ComponentCode": "PAGE",
            "Code": "11111",
            "ContextParamMap": {
                "ProductCode": "CLL_V1",
                "ProductVersion": "1.0"
            },
            "NodeList": [{
                "ComponentCode": "WIZARD",
                "Code": "WIZARD_CODE",
                "NodeList": [{
                    "ComponentCode": "WIZARD_STEP",
                    "Code": "WIZARD_STEP_CODE",
                    "DataModelName": "Policy",
                    "DataObjectCode": "POLICY",
                    "PropertyMap": {
                        "title": "step1"
                    },
                    "NodeList": [{
                        "ComponentCode": "PANEL",
                        "Code": "PANEL_CODE",
                        "DataModelName": "Policy",
                        "DataObjectCode": "POLICY",
                        "PropertyMap": {
                            "headerable": "true",
                            "panelTitle": "{this.state.test}",
                            "toggleable": "true",
                            "panelHeader": "GeneralInfo",
                            "panelFooter": null,
                            "styleClass": "primary",
                            "expendOnPageLoad": "true"
                        },
                        "NodeList": [{
                            "ComponentCode": "SMART_PANEL_GRID",
                            "Code": "SMART_PANEL_GRID_CODE",
                            "DataModelName": "Policy",
                            "DataObjectCode": "POLICY",
                            "NodeList": [{
                                "ComponentCode": "TEXT",
                                "Code": "TEXT_NAME_CODE",
                                "PropertyMap": {
                                    "label": "name"
                                }
                            }, {
                                "ComponentCode": "TEXT",
                                "Code": "TEXT_ADDRESS_CODE",
                                "PropertyMap": {
                                    "label": "address"
                                }
                            }]
                        }]
                    }]
                }, {
                    "ComponentCode": "WIZARD_STEP",
                    "Code": "WIZARD_STEP2_CODE",
                    "PropertyMap": {
                        "title": "step2"
                    },
                    "NodeList": [{
                        "ComponentCode": "PANEL",
                        "Code": "PANEL2_CODE",
                        "DataModelName": "Policy",
                        "DataObjectCode": "POLICY",
                        "PropertyMap": {
                            "headerable": "true",
                            "panelTitle": "panel step2",
                            "toggleable": "true",
                            "panelHeader": "GeneralInfo",
                            "panelFooter": null,
                            "styleClass": "primary",
                            "expendOnPageLoad": "true"
                        },
                        "NodeList": [{
                            "ComponentCode": "SMART_PANEL_GRID",
                            "Code": "SMART_PANEL_GRID2_CODE",
                            "DataModelName": "Policy",
                            "DataObjectCode": "POLICY",
                            "NodeList": [{
                                "ComponentCode": "TEXT",
                                "Code": "TEXT_NAME2_CODE",
                                "PropertyMap": {
                                    "label": "name"
                                }
                            }, {
                                "ComponentCode": "TEXT",
                                "Code": "TEXT_ADDRESS2_CODE",
                                "PropertyMap": {
                                    "label": "address"
                                }
                            }]
                        }]
                    }]
                }, {
                    "ComponentCode": "WIZARD_STEP_BUTTON",
                    "NodeList": [{
                        "ComponentCode": "PANEL",
                        "PropertyMap": {
                            "headerable": "true",
                            "panelTitle": "button title",
                            "toggleable": "true",
                            "panelHeader": "GeneralInfo",
                            "panelFooter": null,
                            "styleClass": "primary",
                            "expendOnPageLoad": "true"
                        },
                        "NodeList": [{
                            "ComponentCode": "BUTTON",
                            "PropertyMap": {
                                "value": "next"
                            }
                        }]
                    }]
                }]
            }, {
                "ComponentCode": "PANEL",
                "NodeList": [{
                    "ComponentCode": "TEXT",
                    "PropertyMap": {
                        "label": "test"
                    }
                }, {
                    "ComponentCode": "TEXT",
                    "PropertyMap": {
                        "label": "book"
                    }
                }, {
                    "ComponentCode": "BUTTON",
                    "PropertyMap": {
                        "value": "test"
                    }
                }]
            }]
        };
    }


    render() {
        //schema={this.mockJson()}
        return (
            <DynamicSection pageId="5269581"/>
        );
    }
};
