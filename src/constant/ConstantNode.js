var ConstantNode = {
	//component code
	WIZARD_STEP_BUTTON: 'WIZARD_STEP_BUTTON',
	WIZARD_STEP: 'WIZARD_STEP',
	WIZARD: 'WIZARD',
	BUTTON_FOR_TABLE: 'BUTTON_FOR_TABLE',
	BUTTON: 'BUTTON',
	LINK: 'LINK', 
	PANEL: 'PANEL',
	SMART_PANEL_GRID: 'SMART_PANEL_GRID',
	TAB: 'TAB',
	TAB_ITEM: 'TAB_ITEM',
	CELL: 'CELL',
	DIALOG: 'DIALOG',
	DATA_TABLE: 'DATA_TABLE',
	DATA_TABLE_COLUMN: 'DATA_TABLE_COLUMN',
	CODING_COMPONENT: 'CODING_COMPONENT',
	BLANK: 'BLANK',
	DATA_TABLE_FUNCTION: 'DATA_TABLE_FUNCTION',
	SELECT: 'SELECT',
	CHECKBOX: 'CHECKBOX',
	RADIO: 'RADIO',
	FIELDSET: 'FIELDSET',
	SWITCH: 'SWITCH',
	OBJECT_SELECTOR: 'OBJECT_SELECTOR',
	SELECTOR: 'SELECTOR',
	SUB_PAGE: 'SUB_PAGE',
	CARD: 'CARD',
	CARD_GROUP: 'CARD_GROUP',
	// SEARCH_CODE_TABLE:'SEARCH_CODE_TABLE',
	CONFIRM_DIALOG:'CONFIRM_DIALOG'
};

//following components have subcomponents as props
ConstantNode.COMPONENTS_HAS_SUB_COMP = [ConstantNode.WIZARD];

//following components are only subcomponents as props
ConstantNode.COMPONENTS_AS_SUB_COMP = [ConstantNode.WIZARD_STEP_BUTTON, ConstantNode.DATA_TABLE_FUNCTION];

//following components have default event
ConstantNode.COMPONENTS_HAVE_DEFAULT_EVENT = [ConstantNode.BUTTON_FOR_TABLE];

//following components are container and should created as a react component
ConstantNode.CONTAINER_COMPONENT = [ConstantNode.PANEL, ConstantNode.SMART_PANEL_GRID, ConstantNode.TAB, ConstantNode.CELL, ConstantNode.DIALOG,
	ConstantNode.WIZARD, ConstantNode.DATA_TABLE, ConstantNode.OBJECT_SELECTOR, ConstantNode.SELECTOR, ConstantNode.CARD, ConstantNode.CARD_GROUP, ConstantNode.CONFIRM_DIALOG];

ConstantNode.COMPONENTS_HAS_CODE_TABLE = [ConstantNode.SELECT, ConstantNode.CHECKBOX, ConstantNode.RADIO];

ConstantNode.COMPONENTS_NO_ONCHANGE = [ConstantNode.OBJECT_SELECTOR];

module.exports = ConstantNode;