var ConstantEventDefinition = {
    COMPONENT_WILL_UPDATE: 'componentWillUpdate',
    COMPONENT_WILL_MOUNT: 'componentWillMount',
    COMPONENT_DID_UPDATE: 'componentDidUpdate',
    COMPONENT_DID_MOUNT: 'componentDidMount',
    COMPONENT_WILL_UNMOUNT: 'componentWillUnmount',
    ON_TAB_CHANGE: 'onTabChange',
    ON_STATE_CHANGE: 'onStateChange'
};

//following event definitions are only for container
ConstantEventDefinition.EVENTS_DEFINITION_FOR_CONTAINER = [ConstantEventDefinition.COMPONENT_WILL_UPDATE,
    ConstantEventDefinition.COMPONENT_WILL_MOUNT, ConstantEventDefinition.COMPONENT_DID_UPDATE,
    ConstantEventDefinition.COMPONENT_DID_MOUNT, ConstantEventDefinition.COMPONENT_WILL_UNMOUNT,
    ConstantEventDefinition.ON_STATE_CHANGE];

module.exports = ConstantEventDefinition;