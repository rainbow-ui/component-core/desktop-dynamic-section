let ConstantKey = {
	//event in json
	EVENT_LIST:'EventList',
	EVENT_CODE_KEY: 'EventCode',
	EVENT_ACTION_KEY: 'CustomizedAction',
	EVENT_ON_CLICK:'onClick',
	EVENT_TABLE_TAIL_DELETE:'tableTailDelete',

	//property key in JSON from AVM
	COMPONENT_CODE: 'ComponentCode',
	PROPERTY_MAP: 'PropertyMap',
	TABLE_TAIL_DELETE:'tailDelete',
	TABLE_TAIL_DELETE_FLAG:'FinishTailDelete',
	TABLE_HEAD_TITLE:'headerTitle',
	EVENT_LIST: 'EventList',
	NODE_LIST: 'NodeList',
	CODE: 'Code',
	PARENT_NODE_ID: 'ParentNodeId',
	FIELD_BINDING: 'FieldBinding',
	BUSINESS_FIELD: 'BusinessField',
	CODE_TABLE_ID: 'CodeTableId',
	CODE_TABLE_NAME: 'CodeTableName',
	CONTEXT_PARAM_MAP: 'ContextParamMap',
    CONDITION_FIELDS_MAP: 'ConditionFieldsMap',
	NAME: 'Name',
	DATA_FILTER_EXPRESSION: 'DataFilterExpression',
	TEMP_CODE_TABLE_NAME:'TempCodeTableName',
    
	//component attribute
	TARGET_SECTION_CODE: 'targetSectionCode',
	TARGET_TABLE_CODE: 'targetTableCode',
	EVENT_TARGET_CODE: 'eventTargetCode',
	CODE_TABLE: 'codeTable',
	CLEAN_FROM_CLIENT: 'cleanFromClient',
	REAL_CLEAN_FROM_CLIENT: 'CleanFromClient',
	DEFAULT_VALUE: 'defaultValue',
	VALUE: 'value',
	VALUE_TOSTRING:'valueTostring',
	TITLE:'title',
	CONFIRMTEXT:'confirmText',
	CANCELTEXT:'cancelText',
	MESSAGE:'message',
	ID:'id',
	ONCONFIRM:'onConfirm',
	ONCANCEL:'onCancel',

	//property
	PROP_NAME: 'name',
	PROP_PATH: 'path',
	PRODUCT_CODE: 'productCode',
	PRODUCT_VERSION:'productVersion',
	PAGE_NAME: 'pageName',
	CONFIGGROUP_CODE:'configGroupCode',
	LANGUAGE_ID:'languageId',
	SYS_I18N_KEY:'system_i18nKey',

	//data key in JSON
	DATA_MODEL_NAME: 'DataModelName',
	DATA_OBJECT_CODE: 'DataObjectCode',
	SCOPE_MODEL_NAME: 'ScopeModelName',
	SCOPE_OBJECT_CODE: 'ScopeObjectCode',
	PARENT_MODEL_NAME: 'ParentModelName',
	PARENT_OBJECT_CODE: 'ParentObjectCode',
	DATA_X_PATH:'DataFilterExpression',
	DATA_MODEL_NAME_CUSTOMIZATION:'Customization',
	CUSTOMIZATION_FIELD_BINDING: "CuzFieldBinding",
	CUSTOMIZATION_OBJECT_RELATION: "cuzObjectRelation",
	FIELD_VALUEBINDING:'ValueBinding',
	TEMP_OBJECT:'Temp',
	PARENTDIRECT_MODELNAME_OBJECTCODE:'DirectParentType',
	VALUE_OPTIONS:'ValueOptions',
	SINGLE: "single",

	//context
	DEFAULT_CONTEXT: -1,
	POLICYLOB_ELEMENT_CODE:-8,

	//Session key
	SESSION_KEY_PRODUCTID:'ProductID',
	SESSION_KEY_PRODUCT:'Product',
    SESSION_KEY_CURRENT_PRODUCT:'CurrentProduct',
    SESSION_KEY_PAGE:'Page',
    SESSION_KEY_CODETABLE:'CodeTable',
	SESSION_KEY_ALLCODETABLES:'AllCodeTables',
	SESSION_KEY_PA_STUDIO:'PA_Common_Studio',

	//Store Key
    COMM_PK: "@pk",
    COMM_TYPE: "@type",
    COMM_UUID: "BusinessUUID",
    COMM_CONNECTOR: "_",
    COMM_TYPE_CONNECTOR: "-",
	POLICY:'Policy',
    POLICY_OBJECTCODE:'POLICY',
    POLICY_ROOT_TYPE:"Policy-POLICY",
    VERSION_FOR_CLIENT:"versionForClient",
    DD_BUSINESS_OBJECT_ID:"BusinessObjectId",
    CHILD_ELEMENTS: "ChildElements",
	MODEL_NAME:'ModelName',
	OBJECT_CODE:'ObjectCode',
    FIELDS:"Fields",
    DD_SCHEMA_DATA_KEY:"DD_SCHEMA_DATA_KEY",
	STORE_ALLSCHEMAOBJ:"ALLSCHEMAOBJECT",
	ALLSCHEMA_KEY:"ALLSCHEMAKEY",
    POLICYSCHEMA_KEY:"POLICYSCHEMA",
    PRODUCT_CONTEXT_TYPE: -2,
    ALL_BUSINESS_OBJECT_CACHE:"0",
    BUSINESS_OBJECT_MATCHED_CACHE:"8",
    AUTO_CREATED:"AutoGenerate",
    COMM_AUTO_CREATED: "AutoCreated",
    YES_FLG:"Y",
    IS_DIRTY_FLG:"IsDirty",
    RELATION_NAME:"RelationName",
    RELATION_TYPE:"RelationType",
    RELATION_TYPE_ONE:-2,
    STORE_DEFAULT_VALUE:"DefaultValue",
    ALLSCHEMAOBJ:"allSchemaObjects",
	POLICYSCHEMADATA:"policySchemaData",

	//Styple Key
	CSS_STYLE:"style",
	CSS_ICON:"icon",
	TABLE_ICON_CONFIG:'iconConfig',
	HEADER_TITLE:'headerTitle',
	STYLE_CLASS:'styleClass',

	//Action Key
	LISTENKEY:"Listen",
	DOMAIN_OBJECT:"domainObject",
	DOMAIN_OBJECT_LIST:"domainObjectList",

	//Policy Key 
	ENDORSEMENT_CHECK_KEY:["PolicyNo","IssueDate"],
	POLICYSTATUS:"PolicyStatus",
	//Coding Component
	CODING_COMPONENT: "codingComponent",

	//model 
	POLICY_RISK:'PolicyRisk',
    MASS_POLICY_COVERAGE:'MassPolicyCoverage',
    POLICY_LIMIT:'PolicyLimitDeductible',
    POLICY_AGREEMENT:'PolicySpecialAgreement',
    POLICY_CT_FORM:'PolicyCoverageForm',
    POLICY_BENEFIT:'PolicyBenefit',
    POLICY_COVERAGE:'PolicyCoverage',
    POLICY_CT_GROUP:'PolicyCoverageGroup',
    POLICY_FORM:'PolicyForm',
    POLICY_PERIL:'PolicyPeril',
    POLICY_SUB_COVERAGE:'PolicySubCoverage'
};

ConstantKey.SKIP_ATTRIBUTE = [ConstantKey.TARGET_SECTION_CODE, ConstantKey.TARGET_TABLE_CODE, ConstantKey.CLEAN_FROM_CLIENT, ConstantKey.EVENT_TARGET_CODE];

ConstantKey.HAVE_ELEMENT_CODE = [ConstantKey.POLICY_RISK, ConstantKey.MASS_POLICY_COVERAGE, ConstantKey.POLICY_LIMIT, ConstantKey.POLICY_AGREEMENT, ConstantKey.POLICY_CT_FORM, ConstantKey.POLICY_BENEFIT, ConstantKey.POLICY_COVERAGE, ConstantKey.POLICY_CT_GROUP, ConstantKey.POLICY_FORM, ConstantKey.POLICY_PERIL,ConstantKey.POLICY_SUB_COVERAGE];

module.exports = ConstantKey;