var ConstantEvent = {
	//event code
	CREATE: 'CREATE',
	EDIT: 'EDIT',
	DELETE: 'DELETE',
	SAVE: 'SAVE',
	OK: 'OK',
	CANCEL: 'CANCEL',
	CREATE_INLINE: 'CREATE_INLINE',
	CALCULATE: 'CALCULATE'
};

ConstantEvent.DEFAULT_EVENTS = [ConstantEvent.CREATE, ConstantEvent.EDIT, ConstantEvent.DELETE, ConstantEvent.SAVE, ConstantEvent.CANCEL, ConstantEvent.CREATE_INLINE, ConstantEvent.CALCULATE];

module.exports = ConstantEvent;