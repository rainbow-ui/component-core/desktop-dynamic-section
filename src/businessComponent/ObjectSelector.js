import {UISwitch, UICard} from 'rainbowui-desktop-core';
import {ComponentContext} from 'rainbow-desktop-cache';
import Binder from 'react-binding';
import ConstantKey from '../constant/ConstantKey';
import CleanFromClientConverter from '../utils/CleanFromClientCoverter';
import StoreAdapter from '../utils/StoreAdapter';
import StoreAction from '../utils/StoreAction';
import PropTypes from 'prop-types';

export default class ObjectSelector extends React.Component {
	constructor(props) {
		super(props);
		this.onChangeListener = this.onChangeListener.bind(this);
		let {policy, modelObject, parentObj, isDialog, dialogObject} = this.props;
		let domainObject = {}, scopeObject = {};
		try {
			if (isDialog === true && !_.isEmpty(dialogObject)) {
				[domainObject, scopeObject] = this.generateDomainObject(modelObject, dialogObject, parentObj);
			} else if (!_.isEmpty(policy)) {
				[domainObject, scopeObject] = this.generateDomainObject(modelObject, policy, parentObj);
			}
		} catch(ex) {
			console.error(`Error in calling generateDomainObject. node:${this.props.node.Code}`);
			throw ex;
		}
		this.state = {
			domainObject: domainObject,
			scopeObject: scopeObject,
			isDialog: isDialog
		};
		// DataContext.put("ObjectSelectorId",this.props.id);
	}

	componentWillMount() {
		//PolicyStore.listen(this.onChangeListener);
        StoreAdapter.listen(this.onChangeListener);
	}

	componentWillUnmount() {
		//PolicyStore.unlisten(this.onChangeListener);
        StoreAdapter.unlisten(this.onChangeListener);
	}

	onChangeListener(state) {
		//var policy = state.policy;
		var policy = this.props.policy;
		try {
			if (this.props.modelObject && this.state.isDialog !== true) {
				let [domainObject, scopeObject] = this.generateDomainObject(this.props.modelObject, policy, this.props.parentObj);
				this.setState({policy: policy, domainObject: domainObject, scopeObject: scopeObject});
			} else if (this.props.modelObject && this.state.isDialog === true) {
				let [domainObject, scopeObject] = this.generateDomainObject(this.props.modelObject, this.props.dialogObject, this.props.parentObj);
				this.setState({policy: policy, domainObject: domainObject, scopeObject: scopeObject});
			}
		} catch(ex) {
			console.error(`Error in calling generateDomainObject. node:${this.props.node.Code}`);
			throw ex;
		}
	}

	generateDomainObject({modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode}, policy, parentObj) {
		let domainObject, scopeObject;
		try {
			//scopeObject = PolicyStore.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
            scopeObject = StoreAdapter.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
			if (!parentModelName || !parentObjectCode) {
				//domainObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
                domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
			} else {
				if (parentModelName === this.props.pageSchema[ConstantKey.DATA_MODEL_NAME]) {
					//domainObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
                    domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
				} else {
					//let scopeObjectSchema = PolicySchemaStore.getBusinessObjectByObjectId(scopeObject.BusinessObjectId, scopeObject);
                    let scopeObjectSchema = StoreAdapter.getBusinessObjectByObjectId(scopeObject.BusinessObjectId, scopeObject);
					//let parentObjTmp = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy, scopeObjectSchema.ModelName, scopeObjectSchema.ObjectCode);
                    let parentObjTmp = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy, scopeObjectSchema.ModelName, scopeObjectSchema.ObjectCode);
					//domainObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode, parentObjTmp);
                    domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode, parentObjTmp);
				}
			}
		} catch(ex) {
			console.error(`Error in getting domain object by model and code. modelName: ${modelName}, objectCode: ${objectCode}, scopeModelName: ${scopeModelName}, scopeObjectCode: ${scopeObjectCode}, policy: ${JSON.stringify(policy)}`);
			throw ex;
		}
		return [domainObject, scopeObject];
	}

	render() {
		let visibled = this.getProperty('visibled');
		if (visibled === 'false' || visibled === false) {
			return null;
		}

		return this.getSectionElement(this.props.children);
	}

	/**
	 * Get section element
	 */
	getSectionElement(children){
		let collapse = 'true', //0: collapse, 1: expanded,
		 	node = this.props.node;

		//let parentObject = PolicyStore.getParentDomainObjectByChildDomainObject(this.state.domainObject, this.state.scopeObject);
		let parentObject = StoreAdapter.getParentDomainObjectByChildDomainObject(this.state.domainObject, this.state.scopeObject);
		if (node[ConstantKey.PROPERTY_MAP][ConstantKey.CLEAN_FROM_CLIENT]) {
			if (this.state.domainObject && !this.state.domainObject[ConstantKey.REAL_CLEAN_FROM_CLIENT]) {
				let isSaved = true;
				if (parentObject && parentObject.TempData && parentObject.TempData[ConstantKey.AUTO_CREATED] === 'Y') {
					isSaved = false;
				}
				if (isSaved) {
					if (this.state.domainObject.TempData && this.state.domainObject.TempData[ConstantKey.AUTO_CREATED] === 'Y') {
						this.state.domainObject[ConstantKey.REAL_CLEAN_FROM_CLIENT] = 'Y';
					} else {
						this.state.domainObject[ConstantKey.REAL_CLEAN_FROM_CLIENT] = 'N';
					}
				} else {
					if (node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE]) {
						// console.log('defaultValuedefaultValue', node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE]);
						this.state.domainObject[ConstantKey.REAL_CLEAN_FROM_CLIENT] = CleanFromClientConverter.not(node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE]);
					} else {
						this.state.domainObject[ConstantKey.REAL_CLEAN_FROM_CLIENT] = 'N';
					}
				}
			}
		}
		if (this.state.domainObject[ConstantKey.REAL_CLEAN_FROM_CLIENT] === 'Y') {
			collapse = 'false';
		} else {
			collapse = 'true';
		}

		return (
			<UICard
				styleClass={this.props.styleClass}
				outline={this.props.outline}
				title={this.props.title}
				subTitle={this.props.subTitle}
				width={this.props.width}
				footer={this.props.footer}
				collapse={collapse}
				collapseIcon='true'
				functions={this.buildTableFunction.apply(this)}>
				{children}
			</UICard>
		);
	}

	buildTableFunction() {
		return (
			<UISwitch
				refName={this.props.refName}
				parent={this.props.parent}
				ref={this.props.refName}
				id={this.props.id + "_switchObject"}
				enabled={this.getProperty("enabled")}
				disabled={this.getProperty("disabled")}
				styleClass={this.getProperty("styleClass")}
				size={this.props.size}
				onColor={this.getProperty("onColor")}
				offColor={this.getProperty("offColor")}
				onText={this.getProperty("onText")}
				offText={this.getProperty("offText")}
				animate={this.getProperty("animate")}
				defaultValue={this.getProperty("defaultValue")}
				io={this.getProperty("io")}
				helpText={this.getProperty("helpText")}
				widthAllocation={this.getProperty("widthAllocation")}
				required={this.getProperty("required")}
				colspan={this.getProperty("colspan")}
				onChange={this.onItemChange.bind(this)}
				valueLink={Binder.bindToState(this, 'domainObject', this.props.fieldBinding.field, this.props.fieldBinding.converter)}
				modelValue={this.state.domainObject}
			/>
		);
	}

	getComponentValue() {
		let value = null, {defaultValue} = this.props;

		if (this.props.valueLink) {
			value = this.props.valueLink.value;
		}

		if (defaultValue != null) {
			value = defaultValue;
			if (this.props.valueLink) {
				this.props.valueLink.requestChange(value);
			}
		}

		return value;
	}

	setComponentValue(event) {
		let inputValue = event.getNewValue();

		// handler valueLink & value
		if (this.props.valueLink) {
			this.props.valueLink.requestChange(inputValue);
		}
	}

	/**
	 * Get collapse status
	 */
	getCollapseStatus(index){
		return (index == parseInt(this.props.activeIndex)) ? "true" : "false";
	}

	onItemChange(event) {
		this.setComponentValue(event);
		this.getProperty("onChange");
	}

	getValue() {
		let {defaultValue} = this.props;
		return this.isFunction(defaultValue) ? defaultValue(this) : defaultValue;
	}

	getProperty(propertyName) {
		let property = this.props[propertyName];

		if (this.isFunction(property)) {
			return property(this);
		}

		return property;
	}

	isFunction( obj ) {
		return jQuery.type(obj) === "function";
	}
};


/**
 * ObjectSelector component prop types
 */
ObjectSelector.propTypes = {
	id: PropTypes.string.isRequired,
	styleClass: PropTypes.oneOf(["default", "primary", "success", "info", "warning", "danger"]),
	onColor: PropTypes.string,
	offColor: PropTypes.string,
	onText: PropTypes.string,
	offText: PropTypes.string,
	size: PropTypes.string,
	animate: PropTypes.bool,
	valueLink: PropTypes.shape({
		value: PropTypes.string.isRequired,
		requestChange: PropTypes.func.isRequired
	}),
	fieldBinding: PropTypes.string,
	value: PropTypes.string,
	defaultValue: PropTypes.string,
	io: PropTypes.oneOf(["in", "out"]),
	helpText: PropTypes.string,
	style: PropTypes.string,
	widthAllocation: PropTypes.string,
	required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	enabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	visibled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	colspan: PropTypes.string,
	panelTitle: PropTypes.string
};

/**
 * Get ObjectSelector component default props
 */
ObjectSelector.defaultProps = {
	activeIndex: 1,
	styleClass:"default"
};

