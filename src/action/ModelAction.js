//import {PolicySchemaStore} from 'unicorn-ui-policy-logic';
//import {InstanceDataUtils} from 'unicorn-ui-basic-logic';
import ConstantKey from '../constant/ConstantKey';
import StoreAdapter from '../utils/StoreAdapter';

export default class ModelAction{
	getModelObject(node, policy, containerScopeModelObject) {
		var result = {};
		if (node[ConstantKey.DATA_MODEL_NAME]) result.modelName = node[ConstantKey.DATA_MODEL_NAME];
		if (node[ConstantKey.DATA_OBJECT_CODE]) result.objectCode = node[ConstantKey.DATA_OBJECT_CODE];
		if (node[ConstantKey.SCOPE_MODEL_NAME]) result.scopeModelName = node[ConstantKey.SCOPE_MODEL_NAME];
		if (node[ConstantKey.SCOPE_OBJECT_CODE]) result.scopeObjectCode = node[ConstantKey.SCOPE_OBJECT_CODE];
		if (node[ConstantKey.PARENT_MODEL_NAME]) result.parentModelName = node[ConstantKey.PARENT_MODEL_NAME];
		if (node[ConstantKey.PARENT_OBJECT_CODE]) result.parentObjectCode = node[ConstantKey.PARENT_OBJECT_CODE];
		if (node[ConstantKey.DATA_X_PATH]) result.dataPath = node[ConstantKey.DATA_X_PATH];
		
		if (!result.scopeModelName) {
			let scopeModelName, scopeObjectCode;
			if (result.modelName === 'Policy') {
				scopeModelName = 'Policy';
				scopeObjectCode = null;
			}
			if (!scopeModelName) {
				scopeModelName = containerScopeModelObject.scopeModelName || 'PolicyLob';
				scopeObjectCode = containerScopeModelObject.scopeObjectCode;
			}
			if (!scopeObjectCode) {
				if (scopeModelName === 'Policy') {
					let policySchema = StoreAdapter.getBusinessObjectByObjectId(policy.BusinessObjectId);
					scopeObjectCode = policySchema.ObjectCode;
				} else {
					let scopeObjectList = StoreAdapter.getDirectChildInstanceByModelName(policy, scopeModelName);
					if (scopeObjectList && scopeObjectList.length) {
						let policyLobSchema = StoreAdapter.getBusinessObjectByObjectId(scopeObjectList[0].BusinessObjectId);
						scopeObjectCode = policyLobSchema.ObjectCode;
					} else { //dialog
						let policySchema = StoreAdapter.getBusinessObjectByObjectId(policy.BusinessObjectId);
						scopeModelName = policySchema.ModelName;
						scopeObjectCode = policySchema.ObjectCode;
					}
				}
			}
			_.assign(result, {scopeModelName, scopeObjectCode});
		}
		return result;
	}

	getBusinessObjectByModelName(modelName, schemaObject, result) {
		result = result || [];
		if (schemaObject.ModelName === modelName){
			result.push(schemaObject);
		}
		let childElement = schemaObject.ChildElements;
		if (childElement) {
			for (let i in childElement) {
				let childArray = childElement[i];
				for (let j = 0; j < childArray.length; j++) {
					let subChildElement = childArray[j];
					let businessObject = this.getBusinessObjectByModelName(modelName, subChildElement, result);
					businessObject.length && result.concat(businessObject);
				}
			}
		}
		return result;
	}

	isSameModel(modelObject1, modelObject2) {
		return (modelObject1[ConstantKey.DATA_MODEL_NAME] === modelObject2[ConstantKey.DATA_MODEL_NAME]
			&& modelObject1[ConstantKey.DATA_OBJECT_CODE] === modelObject2[ConstantKey.DATA_OBJECT_CODE]
			&& modelObject1[ConstantKey.SCOPE_MODEL_NAME] === modelObject2[ConstantKey.SCOPE_MODEL_NAME]
			&& modelObject1[ConstantKey.SCOPE_OBJECT_CODE] === modelObject2[ConstantKey.SCOPE_OBJECT_CODE])
	}

	isNull(modelObject) {
		if (!modelObject) return true;
		if (!modelObject.modelName || !modelObject.objectCode) return true;
		return false;
	}
}