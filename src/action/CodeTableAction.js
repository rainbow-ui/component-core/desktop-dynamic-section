import { CodeTable } from 'rainbowui-desktop-core';
import { SessionContext,LocalContext } from 'rainbow-desktop-cache';
import template from 'url-template';
import ConstantKey from '../constant/ConstantKey';
import config from '../config/globalConfig';
import CodeTableCache from '../utils/CodeTableCache';
import AjaxUtil from '../ajax/AjaxUtil';

export default class CodeTableAction {
	getEmptyCodeTable() {
		return new CodeTable([]);
	}

	objKeySort(obj) {
		var newkey = Object.keys(obj).sort();
		var newObj = {};
		for (var i = 0; i < newkey.length; i++) {
			newObj[newkey[i]] = obj[newkey[i]];
		}
		return newObj;
	}

	getCodeTableById(codeTableId) {
		let result = CodeTableCache.get(codeTableId);
		if (!result) {
			let url = template.parse(config.getCodeTableById).expand({ codeTableId: codeTableId }),
				self = this, result = [];
			AjaxUtil.doGet(url, null, {
				async: false,
				done: function done(data) {
					if (!data) throw 'Error when loading codetable:' + codeTableId + '.' + data;
					result = data.map(codeTableValue => self.convertCodeTableValue(codeTableValue));
					CodeTableCache.add(codeTableId, result);
				},
				fail: function (err) {
					throw 'Error when loading codetable:' + codeTableId + '.' + err;
				}
			});
		}
		return result;
	}

	getCodeTableByNameContext(name, contextId,conditionMap) {
		if (conditionMap) {
			conditionMap = this.objKeySort(conditionMap);
		}
		let key = conditionMap?"C" + "_" + name + "_" + JSON.stringify(conditionMap):"C" + "_" + name;
		let result = LocalContext.get(key);
		let migrationData = SessionContext.get('migrationData');
		if (!result) {
			let url = config.getCodeTableByName;
			let param = {}
			param.CodeTableName = name;
			if (!migrationData) {
				param.ConditionMap = conditionMap?conditionMap:{};
			}
			let self = this;
			AjaxUtil.doPost(url, param , {
				async: false,
				done: function done(data) {
					if (!data) throw 'Error when loading codetable:' + name + '.' + data;
					let aimData = data.BusinessCodeTableValueList?data.BusinessCodeTableValueList:[];
					result = aimData.map(codeTableValue => self.convertCodeTableValue(codeTableValue));
					LocalContext.put(key,result);
				},
				fail: function (err) {
					throw 'Error when loading name:' + name + ', context:' + contextId + '.' + err;
				}
			});
		}
		return result;
	}

	getCodeTableByNameProduct(name, productId) {
		let result = SessionContext.get(ConstantKey.SESSION_KEY_CODETABLE + "_" + name + "_" + productId);
		if (!result) {
			let url = config.getCodeTableByNameProduct + '/' + encodeURIComponent(name) + '/' + encodeURIComponent(productId),
				self = this;
			AjaxUtil.doPost(url, {}, {
				async: false,
				done: function done(data) {
					if (!data) throw 'Error when loading codetable:' + name + '.' + data;
					result = data.map(codeTableValue => self.convertCodeTableValue(codeTableValue));
					SessionContext.put(ConstantKey.SESSION_KEY_CODETABLE + "_" + name + "_" + productId, result);
				},
				fail: function (err) {
					throw 'Error when loading name:' + name + ', context:' + productId + '.' + err;
				}
			});
		}
		return result;
	}

	convertCodeTableValue(codeTableValue) {
		let result = {},
			keys = Object.keys(codeTableValue);
		keys.forEach(key => {
			let resultKey = key;
			if (key === 'Id') {
				resultKey = 'id';
			} else if (key === 'Description') {
				resultKey = 'text';
			}
			result[resultKey] = codeTableValue[key];
		});
		return result;
	}

	getDataTableRecordListByName(dataTableName) {
		let result = CodeTableCache.get(dataTableName);
		if (!result) {
			let url = template.parse(config.getDataTableRecordListByName).expand({ dataTableName: dataTableName });
			AjaxUtil.doGet(url, null, {
				async: false,
				done: function done(data) {
					if (!data) throw 'Error when loading codetable:' + dataTableName + '.' + data;
					result = data;
					CodeTableCache.add(dataTableName, result);
				},
				fail: function (err) {
					throw 'Error when loading codetable:' + dataTableName + '.' + err;
				}
			});
		}
		return result;
	}
	
	// getCodeTable(codeTableId, domainObject, conditionFields, filterExpression) {
	// 	let codetableValueList, filteredValueList;
	// 	codetableValueList = CodeTableCache.get(codeTableId + '');
	// 	if (codetableValueList && !(domainObject instanceof Array)) {
	// 		filteredValueList = this.filterCodeTable(codetableValueList, domainObject, conditionFields, filterExpression);
	// 	} else if (domainObject instanceof Array) {
	// 		filteredValueList = codetableValueList;
	// 	}
	// 	if (!codetableValueList || !codetableValueList.length) {
	// 		return;
	// 	}
	// 	return filteredValueList;
	// }

	// filterCodeTable(codeTableValueList, domainObject, conditionFieldsMap, filterExpression) {
	// 	let conditionFieldsMapKeys = conditionFieldsMap ? Object.keys(conditionFieldsMap) : [];
	// 	let filterResult = codeTableValueList.filter(codeTableValue => {
	// 		if (!codeTableValue.ConditionFields) {
	// 			return true;
	// 		}
	// 		let conditionFields = codeTableValue.ConditionFields;
	// 		let conditionResult = conditionFields.filter(conditionField => {
	// 			let keys = Object.keys(conditionField);
	// 			let isEqual = true;
	// 			keys.forEach(key => {
	// 				if (conditionFieldsMapKeys && conditionFieldsMapKeys.length) {
	// 					conditionFieldsMapKeys.forEach(conditionFieldsMapKey => {
	// 						let conditionFieldCodeTableKey = conditionFieldsMap[conditionFieldsMapKey];
	// 						if (conditionFieldCodeTableKey === key) {
	// 							//这里是!=，不需要完全匹配
	// 							if (domainObject[conditionFieldsMapKey] != conditionField[key]) isEqual = false;
	// 						}
	// 					});
	// 				} else {
	// 					//这里是!=，不需要完全匹配
	// 					if (domainObject[key] != conditionField[key]) isEqual = false;
	// 				}
	// 			});
	// 			return isEqual;
	// 		});
	// 		if (conditionResult && conditionResult.length) return true;
	// 		return false;
	// 	});

	// 	/*if (!filterResult.length) {
	// 		filterResult = codeTableValueList;
	// 	}*/
	// 	if (filterExpression) {
	// 		filterResult = filterResult.filter(codeTableValue => {
	// 			let id = codeTableValue.id, text = codeTableValue.text, filterResult = false;
	// 			if (codeTableValue.ConditionFields) {
	// 				codeTableValue.ConditionFields.forEach(conditionField => {
	// 					let keys = Object.keys(conditionField);
	// 					let evalStr = keys.reduce((result, key) => {
	// 						if (_.isNumber(conditionField[key]) || _.isBoolean(conditionField[key])) return `let ${key} = ${conditionField[key]};`;
	// 						return `let ${key} = '${conditionField[key]}';`;
	// 					}, '');
	// 					let func = new Function(evalStr + ` return ${filterExpression}`);
	// 					if (func() === true) filterResult = true;
	// 				});
	// 			}
	// 			return filterResult;
	// 		});
	// 	}
	// 	return filterResult;
	// }

	// getAllCodeTablesFromNode(node) {
	// 	// if (SessionContext.get("CodeTable_Load_"+node[ConstantKey.COMM_PK])) return
	// 	let codeTableIdList = this.getAllCodeTableIdsFromNode(node);
	// 	if  (!codeTableIdList  || !codeTableIdList.length)  return;
	// 	let codeTableIds = codeTableIdList.join(',').trim(),
	// 		url = template.parse(config.getMultiCodeTables).expand({ codeTableIds: codeTableIds }),
	// 		self = this;
	// 	AjaxUtil.doGet(url, null, {
	// 		async: false,
	// 		done: function done(data) {
	// 			if (!data) throw 'Error when loading codetable:' + codeTableIdList + '.' + data;
	// 			CodeTableCache.init();
	// 			let keys = Object.keys(data);
	// 			keys.forEach(key => {
	// 				let codeTableValueList = data[key];
	// 				codeTableValueList = codeTableValueList.map(codeTableValue => {
	// 					return self.convertCodeTableValue(codeTableValue)
	// 				});
	// 				CodeTableCache.add(key, codeTableValueList)
	// 			});
	// 			SessionContext.put("CodeTable_Load_" + node[ConstantKey.COMM_PK], true)
	// 		},
	// 		fail: function (err) {
	// 			throw 'Error when loading codetable:' + codeTableIdList + '.' + err;
	// 		}
	// 	});
	// }

	// getAllCodeTableIdsFromNode(node, result) {
	// 	result = result || [];
	// 	node[ConstantKey.FIELD_BINDING] && node[ConstantKey.FIELD_BINDING][ConstantKey.CODE_TABLE_ID] && result.push(node[ConstantKey.FIELD_BINDING][ConstantKey.CODE_TABLE_ID]);
	// 	if (node[ConstantKey.PROPERTY_MAP] && !isNaN(parseInt(node[ConstantKey.PROPERTY_MAP][ConstantKey.CODE_TABLE]))) {
	// 		if (!node[ConstantKey.PROPERTY_MAP][ConstantKey.CODE_TABLE].indexOf(',')) result.push(node[ConstantKey.PROPERTY_MAP][ConstantKey.CODE_TABLE])
	// 	}
	// 	if (node[ConstantKey.NODE_LIST] && node[ConstantKey.NODE_LIST].length) {
	// 		let nodeList = node[ConstantKey.NODE_LIST];
	// 		nodeList.forEach(subNode => {
	// 			this.getAllCodeTableIdsFromNode(subNode, result);
	// 		});
	// 	}
	// 	return result;
	// }
}

	