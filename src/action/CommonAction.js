import { DateUtil, UrlUtil,Util } from 'rainbow-desktop-tools';
import { SessionContext,LocalContext,PageContext } from 'rainbow-desktop-cache';
import template from 'url-template';
import ConstantKey from '../constant/ConstantKey';
import ConstantNode from '../constant/ConstantNode';
import config from '../config/globalConfig';
import AjaxUtilDS from '../ajax/AjaxUtil';
import rainbowConfig from 'config';
import {UIConfirmDialog} from 'rainbowui-desktop-core';
import StoreAction from '../utils/StoreAction';
import StoreAdapter from '../utils/StoreAdapter';
import ContainerMapUtils from '../utils/ContainerMapUtils';
import {SchemaUtil} from "rainbow-desktop-sdk";
export default class CommonAction {
	getProductCodeVersion(avmPage) {
		return avmPage[ConstantKey.CONTEXT_PARAM_MAP];
	}

	getProductId(productCode, productVersion) {
		
		let productId = SessionContext.get(ConstantKey.SESSION_KEY_PRODUCTID+"_"+productCode+"_"+productVersion);
		if(!productId){
            AjaxUtil.show();
			let	settings = {
					async: false,
					done: function done(data) {
						productId = data;
					}
				},
				url = config.getProduct + '?productCode=' + productCode + '&productVersion=' + productVersion;
			AjaxUtilDS.doGet(url, null, settings);
            SessionContext.put(ConstantKey.SESSION_KEY_PRODUCTID+"_"+productCode+"_"+productVersion, productId);
            AjaxUtil.hide();
		}
		return productId;
	}

	getProduct(productId) {
		let product = SessionContext.get(ConstantKey.SESSION_KEY_PRODUCT+"_"+productId);
		if(!product){
			let	settings = {
					async: false,
					done: function done(data) {
						product = data;

					}
				},
				url = config.getProductById + productId;
			AjaxUtilDS.doGet(url, null, settings);
			const getProductMasterUrl = template.parse(config.getMasterProduct).expand({productMasterId:product.ProductMasterId});
			const _settings = {
				async: false,
				done: function done(productMaster) {
					product["ProductMaster"] = productMaster;
				}
			}
			AjaxUtilDS.doGet(getProductMasterUrl, null, _settings);
            SessionContext.put(ConstantKey.SESSION_KEY_PRODUCT+"_"+productId, product);
		}
		return product;
	}

	getCurrentProductByProductCode(productCode) {
		let result = SessionContext.get(ConstantKey.SESSION_KEY_CURRENT_PRODUCT+"_"+productCode);
		if(!result) {
            let param = {
                productCode: productCode,
                effectiveDate: DateUtil.getCurrentDateTime()
            };
            let url = template.parse(config.getProductByDate).expand(param);
            let settings = {
                async: false,
                done: function (data) {
                    result = data;
                },
                fail: function (err) {
                    throw `Product Not Found by product code: ${param.productCode}, effective date: ${param.effectiveDate}`;
                }
            };
            AjaxUtilDS.doGet(url, null, settings);
            SessionContext.put(ConstantKey.SESSION_KEY_CURRENT_PRODUCT+"_"+productCode,result);
        }
		return result;
	}

	getCurrentParameterValue(key) {
		const reg = new RegExp(key + "=([^&]*)(&|$)");
		const r = window.location.hash.substr(1).match(reg);
		if (r != null) return decodeURI(r[1]); return null;
	}

	getPageById(pageId) {
		let result = SessionContext.get(ConstantKey.SESSION_KEY_PAGE+"_"+pageId);
		if(!result) {
            let settings = {
                    async: false,
                    done: function done(data) {
                        if (!data) {
                            throw 'Page Not Found by id ' + pageId;
                        }
                        result = data;
                    },
                    fail: function (err) {
                        throw 'Page Not Found by id ' + pageId;
                    }
                },
                url = template.parse(config.getPageById).expand({pageId: pageId});
            AjaxUtilDS.doGet(url, null, settings);
            SessionContext.put(ConstantKey.SESSION_KEY_PAGE+"_"+pageId,result);
        }
		return result;
	}

	getPageByNameProduct(pageName, productCode, productVersion ,needTransCode) {
        let isNotGetPage = SessionContext.get("IS_NOT_GET_PAGE"+"_"+productCode+"_"+productVersion+"_"+pageName);
		let result = SessionContext.get(ConstantKey.SESSION_KEY_PAGE+"_"+productCode+"_"+productVersion+"_"+pageName);
		if(!result&&!isNotGetPage) {
            let settings = {
                    async: false,
                    done: function done(data) {
                        if (!data) {
                            result=null
                            console.log( 'Page Not Found by productCode ' + productCode + ' productVersion ' + productVersion + ' pageName ' + pageName)
                        }
                        result = data;
                        SessionContext.put("IS_NOT_GET_PAGE"+"_"+productCode+"_"+productVersion+"_"+pageName,true);
                    },
                    fail: function (err) {
                        if(err.status===500){
                            result=null
                            SessionContext.put("IS_NOT_GET_PAGE"+"_"+productCode+"_"+productVersion+"_"+pageName,false);
                            console.log( 'Page Not Found by productCode ' + productCode + ' productVersion ' + productVersion + ' pageName ' + pageName)
                        }
                    }
                },
                param = {
                    pageName: pageName,
                    productCode: productCode,
					productVersion: productVersion,
					needTransCode:needTransCode
                };
            if (!productCode || !productVersion) {
                param = {
                    pageName: pageName,
                    contextId: ConstantKey.DEFAULT_CONTEXT
                }
            }
            let url = template.parse(config.getPageByNameProduct).expand(param);
            AjaxUtilDS.doGet(url, null, settings);
            SessionContext.put(ConstantKey.SESSION_KEY_PAGE+"_"+productCode+"_"+productVersion+"_"+pageName,result);
        }
		return result;
	}

	generateContainerProps(context, node) {
		let modelObject;
		if (context.state.isDialog === true && node[ConstantKey.COMPONENT_CODE] !== ConstantNode.DIALOG && context.state.dialogObject) {
			modelObject = context.modelAction.getModelObject(node, context.state.dialogObject, context.state.pageScopeModelObject);
		} else if(node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
		    modelObject = context.props.cuzObject
            if(!modelObject&&context.props.cuzObjectTemplate) modelObject = context.props.cuzObjectTemplate
        }else{
			modelObject = context.modelAction.getModelObject(node, context.state.policy, context.state.pageScopeModelObject);
		}

		let attribute = {
			policy: context.state.policy,
			node: node,
			modelObject: modelObject,
			parentObj: {
				modelName: context.props.modelName,
				objectCode: context.props.objectCode,
				scopeModelName: context.props.scopeModelName,
				scopeObjectCode: context.props.scopeObjectCode,
				parentObj: context.state.domainObject
			},
			ref: context.generateRef(node) + '_container',
			id: context.generateId(node),
			pageSchema: context.state.pageSchema,
			codingComponent: context.props.codingComponent,
			codingParameter: context.props.codingParameter,
			isDialog: context.state.isDialog === true,
			dialogObject: context.state.dialogObject,
			dialogModelObject: context.state.dialogModelObject,
			pageScopeModelObject: context.state.pageScopeModelObject,
            cuzObject: context.props.cuzObject?context.props.cuzObject:null,
            cuzObjectTemplate:context.props.cuzObjectTemplate?context.props.cuzObjectTemplate:null,
            cuzObjectList:context.props.cuzObjectList?context.props.cuzObjectList:null,
            //belows are for unicorn-ui-rule-runtime
			modelName: modelObject?modelObject.modelName:null,
			objectCode: modelObject?modelObject.objectCode:null,
			scopeModelName: modelObject?modelObject.scopeModelName || modelObject.modelName:null,
			scopeObjectCode: modelObject?modelObject.scopeObjectCode || modelObject.objectCode:null,
            refName: context.generateRef(node),
            node: node,
            pageName: context.props.pageName,
            productCode: context.props.productCode
		};

		return attribute;
	}

	covertContainerEvents(eventList) {
		let result = {};
		if (!eventList) return result;
		eventList.forEach(event => result[event[ConstantKey.EVENT_CODE_KEY]] = event[ConstantKey.EVENT_ACTION_KEY]);
		return result;
	}

    getProductElementLob(productId){
        let result = SessionContext.get(ConstantKey.SESSION_KEY_PAGE+"_"+productId);
        if(!result) {
            let settings = {
                    async: false,
                    done: function done(data) {
                        if (!data) {
                            throw 'Product Element Not Found by id ' + productId;
                        }
                        result = data;
                    },
                    fail: function (err) {
                        throw 'Product Element Not Found by id ' + productId;
                    }
                }, 
				url = template.parse(config.getProductElementListByProductIdAndTypeId).expand({productId:productId,elementTypeId:ConstantKey.POLICYLOB_ELEMENT_CODE});
            AjaxUtilDS.doGet(url, null, settings);
            SessionContext.put(ConstantKey.SESSION_KEY_PAGE+"_"+productId,result);
        }
        return result[0];
	}

    getEntirePolicyByPolicyId(policyId, async) {
        let resultPolicy = null;
        const url = template.parse(config.getEntirePolicyByPolicyId).expand({policyId:policyId})

        if (async) {
            const deferred = $.Deferred();
            let settings = {
                async: true,
                done: function (data) {
                    deferred.resolve(data);
                },
                fail: function (jqXHR) {
                    deferred.reject(jqXHR);
                }
            };
            AjaxUtilDS.doGet(url, null, settings);
            return deferred.promise();

        } else {
            let settings = {
                async: false,
                done: function (data) {
                    resultPolicy = data;
                }
            };
            AjaxUtilDS.doGet(url, null, settings);
            console.assert(resultPolicy !== null, "resultPolicy is null.");
            return resultPolicy;
        }
    }

    getI18nDataByPageNameandContext(pageInfo,productInfo){
        if (PageContext.get("I18N_Load_"+pageInfo[ConstantKey.COMM_PK])) return
        let param = {}
        let result;
        let  pageName = pageInfo.Name;
        if(!pageInfo||!productInfo){
            console.assert(!pageName,"param data missing for i18n !")
        }else{
            param[ConstantKey.PAGE_NAME]=pageInfo.Name;
            param[ConstantKey.LANGUAGE_ID]=sessionStorage.getItem(ConstantKey.SYS_I18N_KEY);
            param[ConstantKey.PRODUCT_CODE]=pageInfo["BusinessContextId"]==-1?"Default":productInfo.ProductCode;
            param[ConstantKey.PRODUCT_VERSION]=pageInfo["BusinessContextId"]==-1?"Default":productInfo.ProductVersion;
            param[ConstantKey.CONFIGGROUP_CODE]=ConstantKey.SESSION_KEY_PA_STUDIO;
            let setting = {
                async:false,
                done:function(data){
                    result = data
                },
                fail:function(error){
                    //throw 'Can not Fetch i18n Data by pageid ' + pageName
                    // toastr["error"]('Can not Fetch i18n Data by pageName ' + pageName, "error")
                }
            }
            let url = config.getI18nDataByPageNameandContext
            AjaxUtilDS.doPost(url,param,setting)
            let resultkey = Object.keys(result)
            if(resultkey==0){
                //console.assert(1==0,'No i18n data for page : ' + pageName)
                // toastr["error"]('No i18n data for page : ' + pageName, "错误")
            }else {
                const localLang = LocalContext.get(rainbowConfig.DEFAULT_LOCALSTORAGE_I18NKEY);
                let projectPathKey = buildKey(currentUrl);
                const i18n_cache_key = 'i18n_Cache_'+ localLang + "_" + projectPathKey;
                let i18n_cache_data = LocalContext.get(i18n_cache_key);
                if(i18n_cache_data){
                    //let i18nData = JSON.parse(i18n_cache_data);
                    let i18nData = i18n_cache_data;
                    resultkey.forEach(function (item) {
                        i18n[item] = result[item]
                        i18nData[item] = result[item]
                    })
                    LocalContext.put(i18n_cache_key,i18nData);
                    PageContext.put("I18N_Load_"+pageInfo[ConstantKey.COMM_PK],true);
                } else {
                    let i18nData = null;
                    if (rainbowConfig.DEFAULT_I18N_CONFIGURATION_GROUP instanceof Array) {
                        let i18nUrl = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "I18N", "LOAD_UILABELS_CONFIG_GROUPS");
                        $.ajax({
                            method: "POST",
                            url: i18nUrl,
                            async: false,
                            contentType: "application/json;charset=UTF-8",
                            data: JSON.stringify(rainbowConfig.DEFAULT_I18N_CONFIGURATION_GROUP),
                            xhrFields: {withCredentials: true},
                            crossDomain: true,
                            beforeSend: function (xhr) {
                                let authorization = SessionContext.get("Authorization");
                                if (authorization == null || authorization == undefined || authorization == "") {
                                    console.log("login fail")
                                } else {
                                    xhr.setRequestHeader("Authorization", 'Bearer ' + authorization.substr(13).split("&")[0])
                                }
                            },
                            success: function (d) {
                                i18nData = d;
                                if (rainbowConfig.DEFAULT_I18N_CACHE) {
                                    LocalContext.put(i18n_cache_key, i18nData);
                                    // SessionContext.put("i18n_Cache",  JSON.stringify(i18nData));
                                }
                            },
                            error: function () {
                                console.log("i18n data load fail")
                            }
                        })
                    } else {
                        let i18nUrl = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "I18N", "LOAD_UILABELS") + "?configGroupCode=" + rainbowConfig.DEFAULT_I18N_CONFIGURATION_GROUP;
                        $.ajax({
                            method: "GET",
                            url: i18nUrl,
                            async: false,
                            contentType: "application/json;charset=UTF-8",
                            xhrFields: {withCredentials: true},
                            crossDomain: true,
                            beforeSend: function (xhr) {
                                let authorization = SessionContext.get("Authorization");
                                if (authorization == null || authorization == undefined || authorization == "") {
                                    console.log("login fail")
                                } else {
                                    xhr.setRequestHeader("Authorization", 'Bearer ' + authorization.substr(13).split("&")[0])
                                }
                            },
                            success: function (d) {
                                i18nData = d;
                                if (Util.parseBool(rainbowConfig.DEFAULT_I18N_CACHE)) {
                                    LocalContext.put(i18n_cache_key, i18nData);
                                }
                            },
                            error: function () {
                                console.log("i18n data load fail")
                            }
                        })
                    }
                }
            }
        }
    }

	getPolicySchema(productId){
		const key = `${ConstantKey.DD_SCHEMA_DATA_KEY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${productId}`;
		//const schemaobjkey = `${ConstantKey.STORE_ALLSCHEMAOBJ}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${productId}`
        
        let defSchema = PageContext.get(key);
		//let schemaobj = SessionContext.get(schemaobjkey);
        
        // PageContext.put(ConstantKey.POLICYSCHEMA_KEY,key,true);
        //SessionContext.put(ConstantKey.ALLSCHEMA_KEY,schemaobjkey,true);
        if (defSchema) {
            let resultKey = defSchema.RootObjectId;
            let result = PageContext.get(resultKey);
            PageContext.put(ConstantKey.POLICYSCHEMA_KEY,resultKey,true);            
            if(result){
                return
            }else{
                PageContext.put(resultKey, SchemaUtil.convertSchema(defSchema), true);                
            }
        }else{
            let setting = {
				async:false,
				done:function (data) {
                    defSchema = data
                    let resultKey = defSchema.RootObjectId;
                    PageContext.put(ConstantKey.POLICYSCHEMA_KEY,resultKey,true);                                
                    PageContext.put(resultKey,  SchemaUtil.convertSchema(defSchema), true);
                    PageContext.put(key, defSchema);
                },
				fail:function (error) {
					throw 'Current Schema Fetch error by id ' + productId;
            	}	
			};
			let url = template.parse(config.getPolicySchema).expand({modelName:ConstantKey.POLICY,objectCode:ConstantKey.POLICY_OBJECTCODE,contextType:ConstantKey.PRODUCT_CONTEXT_TYPE,productId:productId})
            AjaxUtilDS.doGet(url,null,setting);
        }
	}

    saveAndDeleteFields(object){
        if (object["Fields"]) {
            delete object["Fields"];
        }
        const childs = object["ChildElements"];
        if (childs) {
            _.each(_.keys(childs), (key) => {
                if (_.isArray(childs[key])) {
                    _.each(childs[key], (child) => {
                        this.saveAndDeleteFields(child);
                    });
                } else {
                    this.saveAndDeleteFields(childs[key]);
                }
            });
        }
    }

    schemaFlat(object,resultlist,currentParentType){
        if (object[ConstantKey.COMM_PK]&&object[ConstantKey.COMM_TYPE]) {
            if(currentParentType){
                object[ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]=currentParentType
            }
            resultlist.push(object);
        }
        const childElementObjects = object[ConstantKey.CHILD_ELEMENTS];
        if (!_.isEmpty(childElementObjects)) {
            let currentParentType = object[ConstantKey.COMM_TYPE]
            _.forEach(childElementObjects, function (childSchemaList) {
                _.forEach(childSchemaList, function (childSchema) {
                    this.schemaFlat(childSchema, resultlist,currentParentType);
                }.bind(this));
            }.bind(this));
        }
    }

    dialogConfirmDelete(targetdata,dialogId) {
        // let isEndorsement = EndorsementFunc.isEndo(Policy);
        // if(isEndorsement){
        //     policy[ConstantKey.POLICYSTATUS] = 4;
        // }
        if (typeof (targetdata) === 'string'){
            targetdata = JSON.parse(targetdata);
        }
        StoreAction.deleteDispatch(targetdata, UIC.TableScopeObject, Policy);
        UIConfirmDialog.hide(dialogId);
        if(UIC.CustomActionEvent){
            let func = UIC.CustomActionEvent;
            if (_.isString(func)) eval(`(function(){\n${func}\n}.bind(this)())`);
        }
    }

    getTableScopeObject(targetTable, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy){
        let targetTemplate = ContainerMapUtils.get(targetTable).props.cuzObjectTemplate;
        let orgModelName = ContainerMapUtils.get(targetTable).props.node.DataModelName;
        let orgObjectCode = ContainerMapUtils.get(targetTable).props.node.DataObjectCode;
        let scopeObject = policy;
		if (targetTemplate&&orgModelName==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&orgObjectCode==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
			scopeModelName = targetTemplate.scopeModelName
			scopeObjectCode = targetTemplate.scopeObjectCode
			parentModelName = targetTemplate.parentModelName
			parentObjectCode = targetTemplate.parentObjectCode
		}
		if (parentModelName && parentObjectCode && scopeModelName && scopeObjectCode) {
            scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy,scopeModelName, scopeObjectCode)
		} else	if (scopeModelName && scopeObjectCode) {
            scopeObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(scopeModelName, scopeObjectCode, policy);
        }
        UIC.TableScopeObject = scopeObject;
    }
    
    getTargetData(aimElement, allDataset) {
        if (aimElement) {
            allDataset.push(aimElement.dataset)
        }
        const childElementObjects = aimElement.parentElement;
        if (!_.isEmpty(childElementObjects)) {
            // _.forEach(childElementObjects, function(childSchemaList) {
            //     _.forEach(childSchemaList, function(childSchema) {
            if (childElementObjects && childElementObjects.dataset && !childElementObjects.dataset.value) {
                this.getTargetData(childElementObjects, allDataset)
            }
            allDataset.push(childElementObjects.dataset)
            //     }.bind(this))
            // }.bind(this))
        }
    }
}
