export default class CleanFromClientCoverter{
    static parse(input) {
        return this.not(input);
    }

    static format(input) {
        return this.not(input);
    }

    static not(val) {
        if (val === 'Y') {
            return 'N';
        }
        return 'Y';
    }
}