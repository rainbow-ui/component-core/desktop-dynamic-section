import { PolicyStore , ObjectStore } from 'rainbow-desktop-sdk';
import ConstantKey from '../constant/ConstantKey';
import { SessionContext, LocalContext, PageContext} from 'rainbow-desktop-cache';
import CommonAction from '../action/CommonAction';
import PubSub from 'pubsub-js';
import SchemaCache from '../utils/SchemaCache';
import EndorsementFunc from '../event/EndorsementFunc';
var _ = require("lodash");

module.exports = {
   
    /**
     * Core Method
     * */
    
    getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCodes, parentObject, scopeObject) {
        console.assert(modelName, "modelName is null.");
        // the scope object is the default parent.
        if (!scopeObject) {
            scopeObject = parentObject;
        }
        let objectCodeArray = [];
        if (objectCodes) {
            objectCodeArray = objectCodes.split(',');
        }

        const scopeBusinessObject = this.getBusinessObjectByObjectId(
            scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID]);

        const parentBusinessObject = this.getBusinessObjectByBusinessObjectIdInScopeBusinessObject(
        parentObject[ConstantKey.DD_BUSINESS_OBJECT_ID], scopeBusinessObject, parentObject);

        console.assert(parentBusinessObject, "parentBusinessObject is null.");

        // valid the parent domain instance is under the scope object
        let matchedParentDomainObject = null;
        if (parentObject) {
            matchedParentDomainObject = parentObject;
        } else {
            matchedParentDomainObject = this.getDomainObjectByBusinessObjectInScopeDomainObject(
                parentBusinessObject, scopeObject, scopeBusinessObject, this.getAllSchemaObjects());
        }
        console.assert(matchedParentDomainObject, "matchedParentDomainObject is null.");
        console.assert( this.getBusinessUUID(matchedParentDomainObject) === this.getBusinessUUID(parentObject),
            "The parent object is not matched with the parent domain object in the store.");
        let allMatchedBusinessObjects = this.getAllBusinessObjectByModelNameAndCodesInScopeBusinessObject(
            modelName, objectCodeArray, parentBusinessObject);
        
        if(allMatchedBusinessObjects&&allMatchedBusinessObjects.length>1)allMatchedBusinessObjects=_.uniq(allMatchedBusinessObjects,allMatchedBusinessObjects[0]["@pk"])
        let allMatchedDomainObjects = [];
        if (_.isEmpty(allMatchedBusinessObjects)) {
            return [];
        } else {
            let allSchemaObjects = this.getAllSchemaObjects()
            for(let i = 0;i<allMatchedBusinessObjects.length;i++){
                let matchedDomainObjects = this.getAllDomainObjectByBusinessObjectInScopeDomainObject(allMatchedBusinessObjects[i], matchedParentDomainObject, parentBusinessObject, allSchemaObjects)
                if(!_.isEmpty(matchedDomainObjects)){
                    for(let j = 0;j<matchedDomainObjects.length;j++){
                        allMatchedDomainObjects.push(matchedDomainObjects[j])
                    }
                }
            }
            // _.forEach(allMatchedBusinessObjects, function (matchedBusinessObject) {
            //     let matchedDomainObjects = this.getAllDomainObjectByBusinessObjectInScopeDomainObject(
            //         matchedBusinessObject, matchedParentDomainObject, parentBusinessObject, this.getAllSchemaObjects());
            //     if (!_.isEmpty(matchedDomainObjects)) {
            //         _.forEach(matchedDomainObjects, function (matchedDomainObject) {
            //             allMatchedDomainObjects.push(matchedDomainObject);
            //         });
            //     }
            // });
        }
        return allMatchedDomainObjects;
    },
    
    getDomainObjectByModelAndCodeAlwaysInComponentState (modelName, objectCode,policy, scopeModelName, scopeObjectCode, nodeObject,parentObject) {
        // console.assert(modelName !== null, "modelName is null");
        // console.assert(objectCode !== null, "objectCode is null");
        // console.assert(policy !== null, "componentRootInstance is null");
        // //console.time("getDomainObjectByModelAndCodeAlwaysInComponentState...");
        // this.commonAction = new CommonAction();
        // let currentSchema = PolicyStore.getPolicySchema(policy);
        // if (currentSchema == null) {
        //     this.commonAction.getPolicySchema(policy.ProductId);
        // }
        // const paramObject = this.paramConvertWithScope(modelName, objectCode, scopeModelName, scopeObjectCode)
        //
        // let existObject = PolicyStore.getChild(paramObject, policy);
        // if (!_.isEmpty(existObject) || existObject != null) {
        //     if (_.size(existObject) === 1) {
        //         return existObject[0];
        //     } else {
        //         console.assert(1 === 0, "There are more than one object exist.");
        //     }
        // } else {
        //     let freshObject = PolicyStore.initChild(paramObject, policy);
        //     console.assert(Object.keys(freshObject).length !== 0, "current modelname " + modelName + " current objectCode " + objectCode + " not contain in current schema")
        //     PolicyStore.setChild(freshObject, policy, paramObject);
        //     return freshObject
        // }
        console.assert(modelName !== null, "modelName is null");
        console.assert(objectCode !== null, "objectCode is null");
        console.assert(policy !== null, "componentRootInstance is null");
        console.assert(nodeObject !== null, "componentRootInstance is null");
        //console.time("getDomainObjectByModelAndCodeAlwaysInComponentState...");
        let scopeObject;
        if (scopeModelName && scopeObjectCode) {
            scopeObject = this.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy,nodeObject);
        } else {
            scopeObject = policy;
        }
        console.assert(scopeObject !== null, "scopeObject is null");

        if (!parentObject) {
            const scopeBusinessObjectId = scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
            //console.log("scopeBusinessObjectId:", scopeBusinessObjectId);
            //console.time("PolicySchemaStore.getBusinessObjectByObjectId...");
            const scopeBusinessObject = this.getBusinessObjectByObjectId(scopeBusinessObjectId);
            //console.timeEnd("PolicySchemaStore.getBusinessObjectByObjectId...");

            //console.time("PolicySchemaDataUtils.getBusinessObjectByModelNameAndCodeInScopeBusinessObject...");
            const targetBusinessObject = this.getBusinessObjectByModelNameAndCodeInScopeBusinessObject(modelName, objectCode, scopeBusinessObject,nodeObject);
            //console.timeEnd("PolicySchemaDataUtils.getBusinessObjectByModelNameAndCodeInScopeBusinessObject...");

            //console.time("PolicyDataUtils.getDomainObjectByBusinessObjectInScopeDomainObject...");
            const matchedDomainInstance = this.getDomainObjectByBusinessObjectInScopeDomainObject(targetBusinessObject, scopeObject, scopeBusinessObject, this.getAllSchemaObjects());
            //console.timeEnd("PolicyDataUtils.getDomainObjectByBusinessObjectInScopeDomainObject...");

            if (matchedDomainInstance) {
                //console.timeEnd("matched case getDomainObjectByModelAndCodeAlwaysInComponentState...");
                return matchedDomainInstance;
            } else {
                let createdInstance = this.createNewInstanceByBusinessObjectAndAttachToParent(targetBusinessObject, scopeObject, scopeBusinessObject,policy);
                this.changeVersion(policy);
                // policy[ConstantKey.IS_DIRTY_FLG] = true;
                // if (policy[ConstantKey.DIRTY_OBJECT]) {
                //     policy[ConstantKey.DIRTY_OBJECT].push(createdInstance);
                // } else {
                //     policy[ConstantKey.DIRTY_OBJECT] = [createdInstance];
                // }

                //console.timeEnd("created case getDomainObjectByModelAndCodeAlwaysInComponentState...");
                return createdInstance;
            }
        } else {
            let allMatchedDomainObjects = this.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCode, parentObject, scopeObject);
            if (!_.isEmpty(allMatchedDomainObjects)) {
                if (_.size(allMatchedDomainObjects) === 1) {
                    return allMatchedDomainObjects[0];
                } else {
                    console.assert(1 === 0, "There are more than one object matched.");
                }
            } else {
                return this.createNewInstanceByModelNameAndObjectAndAttachToParent(modelName, objectCode, parentObject, scopeObject, policy);
            }

        }

    },

    /**
     * Listen Method
     * */
    
    listen(result){
        let subscribeResult = PubSub.subscribe(ConstantKey.LISTENKEY,result);
        if(!subscribeResult){
            console.assert(1==0,'Listen fail !');
        }
    },

    unlisten(result){
        //PubSub.clearAllSubscriptions()
        PubSub.unsubscribe(result);
    },
    
    /**
     * Action Method
     * */

    addDomainObjectInstantly(payload) {
        const modelName = payload.modelName;
        const objectCode = payload.objectCode;
        const scopeObject = payload.scopeObject;
        const policy = payload.policy;
        const scopeBusinessObjectId = scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
        const scopeBusinessObject = this.getBusinessObjectByObjectId(scopeBusinessObjectId);
        const targetBusinessObject = this.getBusinessObjectByModelNameAndCodeInScopeBusinessObject(modelName, objectCode, scopeBusinessObject);
        const createdChild = this.createNewInstanceByBusinessObjectAndAttachToParent(targetBusinessObject, scopeObject, scopeBusinessObject);
        if (policy) {
            this.changeVersion(policy);
        } else {
            this.changeVersion(scopeObject);
        }
        payload['domainObject'] = createdChild;

        let addPublishResult = PubSub.publish(ConstantKey.LISTENKEY,'success!')
        if(!addPublishResult){
            console.assert(1==0,'Add Publish fail !')
        }
        return createdChild;
    },

    saveDomainObject(payload) {
        //console.time("policyStore.saveDomainObject");
        const scopeObject = payload.scopeObject;
        console.assert(scopeObject, "scopeObject is null.");
        const domainObject = payload.domainObject;
        console.assert(domainObject, "domainObject is null.");
        const policy = payload.policy;

        //console.time("policyStore.after refreshPolicyData");
        const businessObjectId = domainObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
        console.assert(businessObjectId, "businessObjectId is null.");
        const scopeBusinessObject = this.getBusinessObjectByObjectId(scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID]);
        const childBusinessObject = this.getBusinessObjectByBusinessObjectIdInScopeBusinessObject(businessObjectId, scopeBusinessObject, domainObject);
        const modelName = childBusinessObject[ConstantKey.MODEL_NAME];
        const objectCode = childBusinessObject[ConstantKey.OBJECT_CODE];

        // const parentDomainObject = this.getDomainObjectByBusinessObjectInScopeDomainObject(parentBusinessObject, scopeObject, scopeBusinessObject, this.getAllSchemaObjects());
        const parentDomainObject = this.getParentDomainObjectByChildDomainObject(domainObject, scopeObject);
        // const parentDomainObject = this.getParentDomainObjectByModelNameAndObjectCode(modelName, objectCode, scopeObject);
        console.assert(parentDomainObject, "parentDomainObject is null.");

        const childInstanceArray = this.getDirectChildInstanceByBusinessObject(parentDomainObject, childBusinessObject);
        delete domainObject[ConstantKey.COMM_AUTO_CREATED];
        if (_.isEmpty(childInstanceArray)) {
            this.attachChildToParent(parentDomainObject, domainObject, childBusinessObject);
        } else {
            // one to many case
            if (_.isArray(childInstanceArray)) {
                const matchedChild = _.find(childInstanceArray, function (child) {
                    return domainObject[ConstantKey.COMM_UUID] === child[ConstantKey.COMM_UUID];
                    // return this.getBusinessUUID(child) === this.getBusinessUUID(domainObject);
                });

                if (matchedChild) {
                    const index = _.indexOf(childInstanceArray, matchedChild);
                    childInstanceArray[index] = domainObject;
                } else {
                    childInstanceArray.push(domainObject);
                }
            } else {
                // one to one case
                const childKey = this.getDirectChildInstanceKeyByBusinessObject(childBusinessObject);
                parentDomainObject[childKey] = domainObject;
            }
        }

        // if policy is not null then refresh to policy store
        if (policy) {
            //console.time("policyStore.refreshPolicyData");
            // this.refreshPolicyData(policy);
            PolicyStore.setPolicy(policy)
            //console.timeEnd("policyStore.refreshPolicyData");
        } else {
            this.changeVersion(scopeObject);
        }

        let savePublishResult = PubSub.publish(ConstantKey.LISTENKEY,'success!')
        if(!savePublishResult){
            console.assert(1==0,'Save Publish fail !')
        }

    },

    deleteDomainObject(payload) {
        let isEndorsement = EndorsementFunc.isEndo(policy);
        const scopeObject = payload.scopeObject;
        console.assert(scopeObject, "scopeObject is null.");
        const domainObject = payload.domainObject;
        console.assert(domainObject, "domainObject is null.");
        const policy = payload.policy;

        const parentDomainObject = this.getParentDomainObjectByChildDomainObject(domainObject, scopeObject);
        const targetBusinessObject = this.getBusinessObjectByObjectId(domainObject[ConstantKey.DD_BUSINESS_OBJECT_ID]);
        let childDomainObjects = this.getDirectChildInstanceByBusinessObject(parentDomainObject, targetBusinessObject);
        console.assert(childDomainObjects, "There is no matched child.");

        // one to many case
        if (_.isArray(childDomainObjects)) {
            const matchedChild = _.find(childDomainObjects, function (child) {
                if(domainObject["TempData"]&&domainObject["TempData"]["uuid"]&&child["TempData"]&&child["TempData"]["uuid"]){
                    return domainObject["TempData"]["uuid"] === child["TempData"]["uuid"]
                }
                return domainObject[ConstantKey.COMM_UUID] === child[ConstantKey.COMM_UUID]
                //return this.getBusinessUUID(domainObject) === this.getBusinessUUID(child);
            });

            console.assert(matchedChild, "There is no matched child.");
            const index = _.indexOf(childDomainObjects, matchedChild);
            if(isEndorsement){
                if(childDomainObjects[index][ConstantKey.POLICYSTATUS]==1){
                    childDomainObjects.splice(index, 1);
                }else if(childDomainObjects[index][ConstantKey.POLICYSTATUS] == 4){
                    delete childDomainObjects[index][ConstantKey.POLICYSTATUS];
                    delete childDomainObjects[index].operationStatus;
                }else{
                    childDomainObjects[index][ConstantKey.POLICYSTATUS] = 4
                    childDomainObjects[index].operationStatus = "deleteRow";
                }
            }else{
                childDomainObjects.splice(index, 1);
            }
        } else {
            // one to one case
            const childKey = this.getDirectChildInstanceKeyByBusinessObject(targetBusinessObject);
            delete parentDomainObject[childKey];
        }
        // if policy is not null then refresh to policy store
        if (policy) {
            //this.refreshPolicyData(policy);
            PolicyStore.setPolicy(policy)
        } else {
            this.changeVersion(scopeObject);
        }

        let deletePublishResult = PubSub.publish(ConstantKey.LISTENKEY,'success!')
        if(!deletePublishResult){
            console.assert(1==0,'Delete Publish fail !')
        }

    },

    savePolicy(policy){

    },
    
    /**
     * Attachchild Method
     * */
    
    getDirectChildInstanceKeyByBusinessObject(businessObject){
        console.assert(businessObject !== null, "businessObject is null.");
        const key = businessObject[ConstantKey.RELATION_NAME];
        console.assert(key !== null, "key is null.");
        return key;
    },

    getDirectChildInstanceByBusinessObject(parentInstanceObject, businessObject) {
        const childKey = this.getDirectChildInstanceKeyByBusinessObject(businessObject);
        return parentInstanceObject[childKey];
    },
    
    attachChildToParent(parentDomainInstance, childDomainInstance, childBusinessObject){
        let childInstances = this.getDirectChildInstanceByBusinessObject(parentDomainInstance, childBusinessObject);
        if (_.isEmpty(childInstances)) {
            const childKey = this.getDirectChildInstanceKeyByBusinessObject(childBusinessObject);
            const relationType = childBusinessObject[ConstantKey.RELATION_TYPE];
            if (relationType === ConstantKey.RELATION_TYPE_ONE) {
                let childModelInstanceList = [];
                childModelInstanceList.push(childDomainInstance);
                parentDomainInstance[childKey] = childModelInstanceList;
            } else {
                parentDomainInstance[childKey] = childDomainInstance;
            }
        } else {
            childInstances.push(childDomainInstance);
        }
    },
    
    /**
     * Create Method
     * */

    setDefaultValueForNewDomainObjectByBusinessObject(domainObject, businessObject) {
        console.assert(domainObject, "domainObject is null.");
        console.assert(businessObject, "businessObject is null.");
        const fields = businessObject[ConstantKey.FIELDS];
        if (fields) {
            const keys = Object.keys(fields);
            _.each(keys, function (key) {
                let field = fields[key];
                if (field[ConstantKey.STORE_DEFAULT_VALUE]) {
                    domainObject[key] = field[ConstantKey.STORE_DEFAULT_VALUE];
                }
            }.bind(this));
        }
    },

    autoCreateChildDomainObject(parentDomainInstance, parentBusinessObject, allBusinessObject){
        // get all child business object it the childElements property
        const childElementObjects = parentBusinessObject[ConstantKey.CHILD_ELEMENTS];
        if (!_.isEmpty(childElementObjects)) {
            const keys = Object.keys(childElementObjects);
            if (keys) {
                for (let index = 0; index < _.size(keys); index++) {
                    let key = keys[index];
                    let childElementList = childElementObjects[key];
                    if (childElementList) {
                        for (let childIndex = 0; childIndex < _.size(childElementList); childIndex++) {
                            let childSchemaObject = childElementList[childIndex];
                            if (childSchemaObject[ConstantKey.AUTO_CREATED] !== ConstantKey.YES_FLG) {
                                continue;
                            }
                            let domainChildInstance = this.getDomainObjectByBusinessObjectInScopeDomainObject(childSchemaObject, parentDomainInstance, parentBusinessObject, allBusinessObject);
                            if (domainChildInstance) {
                                continue;
                            } else {
                                // auto create child
                                domainChildInstance = this.createNewDomainChildInstanceAndAttachToParent(parentDomainInstance, childSchemaObject, allBusinessObject);
                            }
                        }
                    }
                }
            }
        }
        return;
    },

    createNewDomainObjectByBusinessObject(parentDomainObject, businessObject, allBusinessObject) {
        let domainObject = {};
        domainObject[ConstantKey.DD_BUSINESS_OBJECT_ID] = businessObject[ConstantKey.COMM_PK];
        // domainObject[ConstantKey.COMM_TYPE] = businessObject[ConstantKey.MODEL_NAME] + ConstantKey.COMM_TYPE_CONNECTOR + businessObject[ConstantKey.OBJECT_CODE];
        this.buildBusinessUUID(parentDomainObject, domainObject, businessObject);
        // if(businessObject[ConstantKey.MODEL_NAME]!= "Policy"&&businessObject[ConstantKey.MODEL_NAME]!= "PolicyLob"&&businessObject[ConstantKey.MODEL_NAME]!= "PolicyCoverage"){
        //     domainObject["ProductElementCode"] = businessObject[ConstantKey.OBJECT_CODE]
        // }
        if(ConstantKey.HAVE_ELEMENT_CODE.indexOf(businessObject[ConstantKey.MODEL_NAME]) >= 0){
            domainObject["ProductElementCode"] = businessObject[ConstantKey.OBJECT_CODE]
        }
        let tempData = {};
        tempData[ConstantKey.AUTO_CREATED] = ConstantKey.YES_FLG;
        tempData["tempType"] = businessObject[ConstantKey.COMM_TYPE];
        domainObject["TempData"] = tempData;
        this.setDefaultValueForNewDomainObjectByBusinessObject(domainObject, businessObject);
        if (businessObject[ConstantKey.CHILD_ELEMENTS]) {
            this.autoCreateChildDomainObject(domainObject, businessObject, allBusinessObject);
        }
        return domainObject;
    },

    createNewDomainChildInstanceAndAttachToParent(parentDomainObject, businessObject, allBusinnessObject){
        const childDomainInstance = this.createNewDomainObjectByBusinessObject(parentDomainObject, businessObject, allBusinnessObject);
        console.assert(childDomainInstance !== null, "childDomainInstance is null.");
        this.attachChildToParent(parentDomainObject, childDomainInstance, businessObject);
        ObjectStore.setModelObjectUUID(Policy);
        return childDomainInstance;
    },

    createNewInstanceByBusinessObjectAndAttachToParent(businessObject, scopeObject, scopeBusinessObject, policy) {
        //console.log("createNewInstanceByBusinessObjectAndAttachToParent...");
        //console.debug("businessObject:", businessObject);
        //console.debug("scopeObject:", scopeObject);
        //console.debug("scopeBusinessObject:", scopeBusinessObject);
        let isEndorsement = EndorsementFunc.isEndo(policy);  
        const parentBusinessObject = this.getParentBusinessObjectByChildBusinessObject(businessObject, scopeBusinessObject);

        //console.debug("parentBusinessObject:", parentBusinessObject);

        const matchedParentDomainInstance = this.getDomainObjectByBusinessObjectInScopeDomainObject(
            parentBusinessObject, scopeObject, scopeBusinessObject, this.getAllSchemaObjects());

        //console.debug("matchedParentDomainInstance:", matchedParentDomainInstance);
        let childDomainInstance = null;
        if (matchedParentDomainInstance) {

            //console.debug("matched...:", matchedParentDomainInstance);

            childDomainInstance = this.createNewDomainObjectByBusinessObject(matchedParentDomainInstance, businessObject, this.getAllSchemaObjects());

            //console.debug("childDomainInstance...:", childDomainInstance);
            if (policy) {
                this.changeVersion(policy);
            } else {
                this.changeVersion(scopeObject);
            }

            this.attachChildToParent(matchedParentDomainInstance, childDomainInstance, businessObject);
            if(isEndorsement){
                childDomainInstance[ConstantKey.POLICYSTATUS] = 1;
                childDomainInstance.operationStatus = "addRow";
            }
            return childDomainInstance;

        } else {
            //console.debug("no matched...:");
            let createdParentDomainInstance = null;
            do {
                createdParentDomainInstance = this.createNewInstanceByBusinessObjectAndAttachToParent(parentBusinessObject, scopeObject, scopeBusinessObject,policy);
            } while (createdParentDomainInstance[ConstantKey.DD_BUSINESS_OBJECT_ID] !== parentBusinessObject[ConstantKey.COMM_PK]);

            this.changeVersion(policy);
            // double check for auto-created case
            childDomainInstance = this.getDomainObjectByBusinessObjectInScopeDomainObject(businessObject, scopeObject, scopeBusinessObject, this.getAllSchemaObjects());

            if (!childDomainInstance) {
                childDomainInstance = this.createNewDomainObjectByBusinessObject(createdParentDomainInstance, businessObject, this.getAllSchemaObjects());
                //console.debug("childDomainInstance...:", childDomainInstance);
                this.attachChildToParent(createdParentDomainInstance, childDomainInstance, businessObject);
            }
            if(isEndorsement){
                childDomainInstance[ConstantKey.POLICYSTATUS] = 1;
                childDomainInstance.operationStatus = "addRow";
            }
            return childDomainInstance;
        }
    },

    createNewInstanceByModelNameAndObjectAndAttachToParent(modelName, objectCode, parentObject, scopeObject, policy) {
        const scopeBusinessObject = this.getBusinessObjectByObjectId(scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID]);
        console.assert(scopeBusinessObject, "scopeBusinessObject is null.");
        const parentBusinessObject = this.getBusinessObjectByObjectId(parentObject[ConstantKey.DD_BUSINESS_OBJECT_ID]);
        console.assert(parentBusinessObject, "parentBusinessObject is null.");

        const objectCodes = [objectCode];
        const matchedBusinessObjects = this.getAllBusinessObjectByModelNameAndCodesInScopeBusinessObject(modelName, objectCodes, scopeBusinessObject, parentObject);
        //console.assert(matchedBusinessObjects.length === 1, "There is no matched child business object or more than one.");

        const childBusinessObject = matchedBusinessObjects[0];
        //console.debug("childBusinessObject:", childBusinessObject);

        const childDomainInstance = this.createNewDomainChildInstanceAndAttachToParent(parentObject, childBusinessObject, this.getAllSchemaObjects());
        if (policy) {
            this.changeVersion(policy);
        } else {
            this.changeVersion(scopeObject);
        }

        console.assert(childDomainInstance, "childDomainInstance is null.");

        return childDomainInstance;
    },

    /**
     * Schema Method
     * */

    getAllSchemaObjects() {
        let currentSchemaKey = PageContext.get(ConstantKey.POLICYSCHEMA_KEY)
        let currentSchema = PageContext.get(currentSchemaKey);
        let ElementId = currentSchema["ElementId"];
        let allSchemaObjects=SchemaCache.get(ElementId)?SchemaCache.get(ElementId):[]
        if(allSchemaObjects.length < 1){
            this.getBusinessObjectsCascade(currentSchema, allSchemaObjects)
            if(allSchemaObjects.length > 0){
                SchemaCache.add(ElementId, allSchemaObjects)
                return allSchemaObjects;
            }else{
            console.assert(1==0, "Can not find all schema object!");
            }
        }else{
            return allSchemaObjects;
        }
    },

    /**
     * DomainObject Method
     * */

    getParentDomainObjectByChildDomainObject(childDomainObject, scopeObject) {

        const scopeBusinessObjectId = scopeObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
        const scopeBusinessObject = this.getBusinessObjectByObjectId(scopeBusinessObjectId);
        console.assert(scopeBusinessObject, "scopeBusinessObject is null.");
        const childBusinessObjectId = childDomainObject[ConstantKey.DD_BUSINESS_OBJECT_ID];
        const childBusinessObject = this.getBusinessObjectByBusinessObjectIdInScopeBusinessObject(childBusinessObjectId, scopeBusinessObject, childDomainObject);
        console.assert(childBusinessObject, "childBusinessObject is null.");
        const parentBusinessObject = this.getParentBusinessObjectByChildBusinessObject(childBusinessObject, scopeBusinessObject);
        console.assert(parentBusinessObject, "parentBusinessObject is null.");
        const parentDomainObject = this.getDomainObjectByBusinessObjectInScopeDomainObject(parentBusinessObject, scopeObject, scopeBusinessObject, this.getAllSchemaObjects());
        console.assert(parentDomainObject, "parentDomainObject is null.");
        return parentDomainObject;
    },
    
    getInstanceObjectsCascade(instanceObject, businessObject, resultObjectList, allBusinessObject,directParentId) {

        console.assert(instanceObject, "instanceObject is null.");
        console.assert(businessObject, "businessObject is null.");

        if (!_.find(resultObjectList, function (resultObject) {
            return this.getBusinessUUID(resultObject) === this.getBusinessUUID(instanceObject);
        }.bind(this))) {
        // if(directParentType){
        //     if(instanceObject&&!instanceObject["TempData"]){
        //         instanceObject["TempData"] = {}
        //     }
        //     instanceObject["TempData"][ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]=directParentType
        // }
        if(directParentId){
            if(instanceObject&&!instanceObject["TempData"]){
                instanceObject["TempData"] = {}
            }
            instanceObject["TempData"]["DirectParentId"]=directParentId
        }
        resultObjectList.push(instanceObject);
    }

        // get all reference keys
        const childReferenceKeys = this.getChildrenBusinessObjectKeyByBusinessObject(businessObject);

        // get all instance children
        if (childReferenceKeys) {
            let allChildrenInstances = [];
            _.each(childReferenceKeys, function (childKey) {
                let childrenWithKey = instanceObject[childKey];
                if (childrenWithKey) {
                    allChildrenInstances.push(childrenWithKey);
                }

            }.bind(this));

            allChildrenInstances = _.flatten(allChildrenInstances);

            if (allChildrenInstances) {
                for (let childInstanceIndex = 0; childInstanceIndex < allChildrenInstances.length; childInstanceIndex++) {
                    let childInstance = allChildrenInstances[childInstanceIndex];
                    console.assert(childInstance, "childInstance is null.");
                    // let directParentType = instanceObject[ConstantKey.COMM_TYPE];
                    // let directParentType = instanceObject&&instanceObject["TempData"]?instanceObject["TempData"]["ProductElementCode"]:null;
                    let directParentId = instanceObject.BusinessObjectId;
                    let childBusinessObjectId = childInstance[ConstantKey.DD_BUSINESS_OBJECT_ID];
                    console.assert(childBusinessObjectId, "childBusinessObjectId is null.");
                    let childBusinessObject = this.getBusinessObjectById(childBusinessObjectId, allBusinessObject);
                    console.assert(childBusinessObject, "childBusinessObject is null.");
                    this.getInstanceObjectsCascade(childInstance, childBusinessObject, resultObjectList, allBusinessObject,directParentId);
                }
            }
        }
    },

    getDomainObjectByBusinessObjectInScopeDomainObject(businessObject, scopeObject, scopeBusinessObject, allBusinessObject) {
        console.assert(businessObject !== null, "businessObject is null.");
        console.assert(scopeObject !== null, "scopeObject is null.");
        console.assert(scopeBusinessObject !== null, "scopeBusinessObject is null.");

        //console.time(" in PolicyDataUtils.getDomainObjectByBusinessObjectInScopeDomainObject");
        let matchedDomainInstances = this.getAllDomainObjectByBusinessObjectInScopeDomainObject(
            businessObject, scopeObject, scopeBusinessObject, allBusinessObject);
        //console.timeEnd(" in PolicyDataUtils.getDomainObjectByBusinessObjectInScopeDomainObject");
        console.assert(_.size(matchedDomainInstances) < 2, "There are more than one instance object.");
        if (!_.isEmpty(matchedDomainInstances)) {
            if(matchedDomainInstances.length>1){
                matchedDomainInstances = _.filter(matchedDomainInstances, function (resultObject) {
                    return resultObject["TempData"]["DirectParentId"] === businessObject["DirectParentId"]                                            
                    // return resultObject["TempData"][ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]===businessObject[ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]
            });
            }
            return matchedDomainInstances[0];
        } else {
            return null;
        }
    },

    getAllDomainObjectByBusinessObjectInScopeDomainObject(businessObject, scopeObject, scopeBusinessObject, allBusinessObject){
        console.assert(businessObject !== null, "businessObject is null. ");
        console.assert(scopeObject !== null, "scopeObject is null.");
        console.assert(scopeBusinessObject !== null, "scopeBusinessObject is null.");
        let allDomainObject = [];
            this.getInstanceObjectsCascade(scopeObject, scopeBusinessObject, allDomainObject, allBusinessObject);

        //console.timeEnd(" in PolicyDataUtils.getAllDomainObjectByBusinessObjectInScopeDomainObject");
        if (!_.isEmpty(allDomainObject)) {
            let matchedDomainInstances = _.filter(allDomainObject, function (domainObject) {
                return domainObject[ConstantKey.DD_BUSINESS_OBJECT_ID] === businessObject[ConstantKey.COMM_PK];
            });
            return matchedDomainInstances;
        }
    },

    /**
     * Locate Method
     * */


    locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, componentPolicy,nodeObject) {
        console.assert(scopeModelName, "scopeObjectCode is null.");
        console.assert(scopeObjectCode, "scopeObjectCode is null.");
        console.assert(nodeObject, "nodeObject is null.");
        const policyBusinessObjectId = componentPolicy[ConstantKey.DD_BUSINESS_OBJECT_ID];
        const policyBusinessObject = this.getBusinessObjectByObjectId(policyBusinessObjectId);
        const targetBusinessObject = this.getBusinessObjectByModelNameAndCodeInScopeBusinessObject(scopeModelName, scopeObjectCode, policyBusinessObject,nodeObject);
        console.assert(targetBusinessObject, "targetBusinessObject is null.");
        const matchedScopeInstance = this.getDomainObjectByBusinessObjectInScopeDomainObject(targetBusinessObject, componentPolicy, policyBusinessObject, this.getAllSchemaObjects());
        console.assert(matchedScopeInstance, "matchedScopeInstance is null.");
        return matchedScopeInstance;
    },

    /**
     * BusinessObject Method
     * */

    getBusinessObjectByObjectId(businessObjectId) {
        const allBusinessObject = this.getAllSchemaObjects();
        if (!_.isEmpty(allBusinessObject)) {
            const matchedBusinessObject = _.find(allBusinessObject, function (businessObject) {
                return businessObject[ConstantKey.COMM_PK] === businessObjectId;
            });
            console.assert(matchedBusinessObject, "There is no matched business object.");
            return matchedBusinessObject;
        }
    },

    getBusinessObjectsCascade(schemaBusinessObject, resultObjectList, directParentType,directParentId) {
        console.assert(schemaBusinessObject !== null, "schemaBusinessObject is null.");
        if (schemaBusinessObject) {
            if(directParentType){
                schemaBusinessObject[ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]=directParentType
                schemaBusinessObject["DirectParentId"]=directParentId                
            }
            resultObjectList.push(schemaBusinessObject);
        }
        const childElementObjects = schemaBusinessObject[ConstantKey.CHILD_ELEMENTS];
        if (!_.isEmpty(childElementObjects)) {
            let directParentType = schemaBusinessObject[ConstantKey.COMM_TYPE]
            let directParentId = schemaBusinessObject[ConstantKey.COMM_PK]            
            _.forEach(childElementObjects, function (childSchemaList) {
                _.forEach(childSchemaList, function (childSchema) {
                    this.getBusinessObjectsCascade(childSchema, resultObjectList,directParentType,directParentId);
                }.bind(this));
            }.bind(this));
        }
    },

    getAllBusinessObjectInScopeBusinessObjectByCache(scopeBusinessObject){
        //const allCacheKey = scopeBusinessObject[ConstantKey.COMM_PK] + ConstantKey.COMM_CONNECTOR + ConstantKey.ALL_BUSINESS_OBJECT_CACHE;
        //let allSchemaObjects = []
        //let allSchemaObjects = SessionContext.get(SessionContext.get(ConstantKey.ALLSCHEMA_KEY));
        let allSchemaObjects = this.getAllSchemaObjects()
        console.assert(_.size(allSchemaObjects)!==0,"Allshcemaobject fetch fail!")
        //if (PolicySchemaCache[allCacheKey]) {
        //    allSchemaObjects = PolicySchemaCache[allCacheKey];
        //} else {
        //this.getBusinessObjectsCascade(scopeBusinessObject, allSchemaObjects);
        //PolicySchemaCache[allCacheKey] = allSchemaObjects;
        //}
        return allSchemaObjects;
    },
    
    getParentBusinessObjectByChildBusinessObject(childBusinessObject, scopeBusinessObject) {
        let policySchemaData = {};
        let policySchemaDataKey = PageContext.get(ConstantKey.POLICYSCHEMA_KEY);
        policySchemaData = PageContext.get(policySchemaDataKey);
        console.assert(policySchemaData!==null,"PolicySchemaData fetch fail!");
        if(policySchemaData==null){
            this.commonAction.getPolicySchema(policy.ProductId);
            policySchemaData = PolicyStore.getPolicySchema(policy);
        }

        // if the scope business object is null,then set the default value
        if (!scopeBusinessObject) {
            scopeBusinessObject = policySchemaData;
        }

        let allScopeSchemaObjects = [];

        const childBusinessObjectId = childBusinessObject[ConstantKey.COMM_PK];
        const childBusinessObjectDparent = childBusinessObject[ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE];
        //console.debug("childBusinessObject:", childBusinessObject);
        //console.debug("childBusinessObjectId:", childBusinessObjectId);

        // get all schema object
        this.getBusinessObjectsCascade(scopeBusinessObject, allScopeSchemaObjects);

        console.assert(_.size(allScopeSchemaObjects), "There is no schema object");
        if (!_.isEmpty(allScopeSchemaObjects)) {

            let matchedParentBusinessObject = _.filter(allScopeSchemaObjects, function (schemaObject) {
                //if (schemaObject[UiLogicConstants.CHILD_ELEMENTS]) {
                if (schemaObject[ConstantKey.CHILD_ELEMENTS]) {
                    let childBusinessObjects = _.flatten(_.values(schemaObject[ConstantKey.CHILD_ELEMENTS]));
                    // try to match by child element
                    let matchedChildBusinessObject = _.find(childBusinessObjects, function (childSchemaObject) {
                        //console.debug("childSchemaObject:", childSchemaObject);
                        return childBusinessObjectId === childSchemaObject[ConstantKey.COMM_PK];
                    });

                    if (matchedChildBusinessObject) {
                        //console.debug("**matchedChildBusinessObject:", matchedChildBusinessObject);
                        return matchedChildBusinessObject;
                    }

                }
            });

            //console.debug("matchedParentBusinessObject:", matchedParentBusinessObject);

            // just find unique parent business object, it is fine
            if(matchedParentBusinessObject.length>1){
                matchedParentBusinessObject = _.filter(matchedParentBusinessObject,function (eachMatch) {
                    return eachMatch[ConstantKey.COMM_TYPE
                        ]==childBusinessObjectDparent;
                })
            }
            console.assert(_.size(matchedParentBusinessObject) === 1, "The parent can not be found or find more than one.");
            return matchedParentBusinessObject[0];
        }


    },

    getBusinessObjectByBusinessObjectIdInScopeBusinessObject(businessObjectId, scopeBusinessObject,businessObject) {
        console.assert(businessObjectId, "businessObjectId is null.");
        console.assert(businessObjectId, "businessObject is null.");
        console.assert(scopeBusinessObject, "scopeBusinessObject is null.");
            const allSchemaObjects = this.getAllBusinessObjectInScopeBusinessObjectByCache(scopeBusinessObject);
            if (!_.isEmpty(allSchemaObjects)) {
                let matchedBusinessObject = _.filter(allSchemaObjects, function (businessObject) {
                    return businessObject[ConstantKey.COMM_PK] === businessObjectId;
                });
                if(matchedBusinessObject.length>1){
                    matchedBusinessObject = _.filter(matchedBusinessObject,function (tempMatchedObj) {
                        return tempMatchedObj[ConstantKey.RELATION_NAME].indexOf(ConstantKey.TEMP_OBJECT)<0
                    })
                }
                if(businessObject&&businessObject["TempData"]&&businessObject["TempData"]["DirectParentId"]&&matchedBusinessObject.length>1){
                    matchedBusinessObject = _.filter(matchedBusinessObject,function (resultObject) {
                        // return resultObject[ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]===businessObject["TempData"][ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]
                        return resultObject["DirectParentId"] === businessObject["TempData"]["DirectParentId"]                        
                    })
                }
                console.assert(_.size(matchedBusinessObject) === 1, "There is no matched business object or more than one.");
                //PolicySchemaCache[cacheKey] = matchedBusinessObject[0];
                return matchedBusinessObject[0];
            }
    },

    getBusinessObjectByModelNameAndCodeInScopeBusinessObject(modelName, objectCode, scopeBusinessObject,nodeObject) {
        console.assert(modelName, "Model name is null.");
        console.assert(objectCode, "Object code is null.");
        console.assert(scopeBusinessObject, "scopeBusinessObject is null.");
        //console.assert(nodeObject, "nodeObject is null.");
        let nodeObjectArray,nodeObjectTran,nodeObjectTranStr;
        if(!_.isEmpty(nodeObject)&&nodeObject["DataFilterExpression"]){
            nodeObjectArray = nodeObject["DataFilterExpression"].split('->');
            nodeObjectTran=nodeObjectArray.slice(-3,-2);
            nodeObjectTranStr=nodeObjectTran.toString();
        }
        
        //let cacheKey = modelName + PolicyCacheConstants.CONNECT_KEY + objectCode + PolicyCacheConstants.CONNECT_KEY + scopeBusinessObject[UiLogicConstants.COMM_PK] + PolicyCacheConstants.CONNECT_KEY + PolicyCacheConstants.BUSINESS_MODEL_CODE_MATCHED_CACHE;
        //if (PolicySchemaCache[cacheKey]) {
        //    return PolicySchemaCache[cacheKey];
        //} else {
            const allSchemaObjects = this.getAllBusinessObjectInScopeBusinessObjectByCache(scopeBusinessObject);
            if (!_.isEmpty(allSchemaObjects)) {
                let matchedBusinessObject = _.filter(allSchemaObjects, {ModelName: modelName, ObjectCode: objectCode});
                if(!nodeObject){
                    if(_.size(matchedBusinessObject)>1) matchedBusinessObject =_.filter(matchedBusinessObject,{DirectParentType:scopeBusinessObject["@type"]})
                    return matchedBusinessObject[0];
                }
                if(_.size(matchedBusinessObject)>1) matchedBusinessObject =_.filter(matchedBusinessObject,{DirectParentType:nodeObjectTranStr})
                //console.warn(_.size(matchedBusinessObject) === 1, "There is more than one matched business object(modelName:" + modelName + ", objectCode:" + objectCode + ")"); 
                //const matchedBusinessObject = _.filter(allSchemaObjects, {ModelName: modelName, ObjectName: objectCode});
                //console.assert(_.size(matchedBusinessObject) === 1, "There is no matched business object or more than one.(modelName:" + modelName + ", objectCode:" + objectCode + ")");
                //PolicySchemaCache[cacheKey] = matchedBusinessObject[0];
                return matchedBusinessObject[0];
            }
        //}

    },

    getBusinessObjectByModelNameAndCodeOnlyOne(modelName, objectCode, scopeBusinessObject,nodeObject) {
        console.assert(modelName, "Model name is null.");
        console.assert(objectCode, "Object code is null.");
        console.assert(scopeBusinessObject, "scopeBusinessObject is null.");
        let nodeObjectArray,nodeObjectTran,nodeObjectTranStr;
        if(!_.isEmpty(nodeObject)&&nodeObject["DataFilterExpression"]){
            nodeObjectArray = nodeObject["DataFilterExpression"].split('->');
            nodeObjectTran=nodeObjectArray.slice(-3,-2);
            nodeObjectTranStr=nodeObjectTran.toString();
        }
            const allSchemaObjects = this.getAllBusinessObjectInScopeBusinessObjectByCache(scopeBusinessObject);
            if (!_.isEmpty(allSchemaObjects)) {
                let matchedBusinessObject = _.filter(allSchemaObjects, {ModelName: modelName, ObjectCode: objectCode});
                if(nodeObject){
                    if(_.size(matchedBusinessObject)>1) matchedBusinessObject =_.filter(matchedBusinessObject,{DirectParentType:nodeObjectTranStr})
                }
                //console.warn(_.size(matchedBusinessObject) === 1, "There is more than one matched business object(modelName:" + modelName + ", objectCode:" + objectCode + ")"); 
                    return matchedBusinessObject[0];
            }
    },

    getAllBusinessObjectByModelNameAndCodesInScopeBusinessObject(modelName, objectCodes, scopeBusinessObject, parentObject){
        console.assert(modelName, "modelName is null");
        console.assert(scopeBusinessObject, "scopeBusinessObject is null");
        //const cacheKey = modelName + PolicyCacheConstants.CONNECT_KEY + objectCodes + PolicyCacheConstants.CONNECT_KEY + scopeBusinessObject[UiLogicConstants.COMM_PK] + PolicyCacheConstants.ALL_BUSINESS_OBJECT_MATCHED_CACHE;

        //if (PolicySchemaCache[cacheKey]) {
        //    return PolicySchemaCache[cacheKey];
        //} else {
        const allSchemaObjects = this.getAllBusinessObjectInScopeBusinessObjectByCache(scopeBusinessObject);
        let matchedBusinessObject = [];
        if (!_.isEmpty(allSchemaObjects)) {
            // the object is not null, return code matched ones,else return all by model name
            if (!_.isEmpty(objectCodes)) {
                matchedBusinessObject = _.filter(allSchemaObjects, function (businessObject) {
                    return modelName === businessObject[ConstantKey.MODEL_NAME] && _.includes(objectCodes, businessObject[ConstantKey.OBJECT_CODE]);
                });

            } else {
                matchedBusinessObject = _.filter(allSchemaObjects, function (businessObject) {
                    return modelName === businessObject[ConstantKey.MODEL_NAME];
                });
            }

        }
        //PolicySchemaCache[cacheKey] = matchedBusinessObject;
        if(matchedBusinessObject.length>1){
            let parb = parentObject||scopeBusinessObject;
            let parentObjectType = parb[ConstantKey.COMM_TYPE]
            matchedBusinessObject = _.filter(matchedBusinessObject,function(lastitem){
                return lastitem[ConstantKey.PARENTDIRECT_MODELNAME_OBJECTCODE]==parentObjectType
            })
        }
        return matchedBusinessObject;
        //}
    },

    /**
     * Convert param Method
     * */

    paramConvert(modelName, objectCode){
        console.assert(modelName !== null, "modelName is null");
        console.assert(objectCode !== null, "objectCode is null");
        let paramObj = {}
        paramObj[ConstantKey.MODEL_NAME] = modelName;
        paramObj[ConstantKey.OBJECT_CODE] = objectCode;
        return paramObj
    },
    
    paramConvertForProduct (productCode,productVersion){
        console.assert(productCode !== null, "productCode is null");
        console.assert(productVersion !== null, "productVersion is null");
        let paramObj = {};
        paramObj[ConstantKey.PRODUCT_CODE] = productCode;
        paramObj[ConstantKey.PRODUCT_VERSION] = productVersion;
        return paramObj;
    },
    
    paramConvertWithScope (modelName, objectCode, scopeModelName, scopeObjectCode) {
           console.assert(modelName !== null, "modelName is null");
           console.assert(objectCode !== null, "objectCode is null");
           let paramObj = {};
           if (!scopeObjectCode && !scopeModelName) {
               paramObj[ConstantKey.PARENT_MODEL_NAME] = 'Policy';
               paramObj[ConstantKey.PARENT_OBJECT_CODE] = 'POLICY';
           } else {
               paramObj[ConstantKey.MODEL_NAME] = modelName;
               paramObj[ConstantKey.OBJECT_CODE] = objectCode;
               paramObj[ConstantKey.PARENT_MODEL_NAME] = scopeModelName;
               paramObj[ConstantKey.PARENT_OBJECT_CODE] = scopeObjectCode;
           }
           return paramObj
       },

    /**
     * Version Method
     * */

    changeVersion(domainObject){
        //changeSeq++;
        domainObject[ConstantKey.VERSION_FOR_CLIENT] = new Date().getTime() + "" //+ changeSeq;
    },

    /**
     * UUID Method
     * */

    getBusinessUUID(objectInstance) {
        if (objectInstance) {
            const pk = objectInstance[ConstantKey.COMM_PK];
            if (pk) {
                return pk;
            }
            let businessUUD = objectInstance[ConstantKey.COMM_UUID];

            if (!businessUUD) {
                businessUUD = objectInstance[ConstantKey.DD_BUSINESS_OBJECT_ID] + ConstantKey.COMM_CONNECTOR + Math.random() + ConstantKey.COMM_CONNECTOR + new Date().getTime();
                objectInstance[ConstantKey.COMM_UUID] = businessUUD;
                return businessUUD;
            }
            return businessUUD;
        }
    },
    
    getBusinessObjectById(objectId, allSchemaObjects) {
        if (!_.isEmpty(allSchemaObjects)) {
            const matchedBusinessObject = _.find(allSchemaObjects, function (schemaObject) {
                return schemaObject[ConstantKey.COMM_PK] === objectId;
            });
            console.assert(matchedBusinessObject, "There is no matched business object");
            return matchedBusinessObject;
        }
    },

    getChildrenBusinessObjectKeyByBusinessObject(schemaBusinessObject) {
        if(!schemaBusinessObject){return}
        const childElementObjects = schemaBusinessObject[ConstantKey.CHILD_ELEMENTS];
        if (childElementObjects) {
            return Object.keys(childElementObjects);
        }
    },

    getBusinessObjectByModelNameAndCode(modelName, objectCode, allSchemaObjects) {
        console.assert(modelName, "Model name is null");
        console.assert(objectCode, "Object code is null");

        if (!_.isEmpty(allSchemaObjects)) {
            const matchedBusinessObject = _.filter(allSchemaObjects, {ModelName: modelName, ObjectCode: objectCode});
            return matchedBusinessObject;
        }
    },
    
    buildBusinessUUID(parentInstanceObject, childInstanceObject, childBusinessObject) {
        console.assert(parentInstanceObject);
        console.assert(childInstanceObject);
        console.assert(childInstanceObject[ConstantKey.DD_BUSINESS_OBJECT_ID]);

        // if there the business UUID already exists
        if (!this.getBusinessUUID(childInstanceObject)) {
            console.assert(childBusinessObject, "The business object can not be found.");
            let existingChildInstance = this.getDirectChildInstanceByBusinessObject(parentInstanceObject, childBusinessObject);
            if (_.isEmpty(existingChildInstance)) {
                childInstanceObject[ConstantKey.COMM_UUID] = childInstanceObject[ConstantKey.DD_BUSINESS_OBJECT_ID] + Math.random() + ConstantKey.COMM_CONNECTOR + ConstantKey.COMM_CONNECTOR + new Date().getTime();
            } else {
                childInstanceObject[ConstantKey.COMM_UUID] = childInstanceObject[ConstantKey.DD_BUSINESS_OBJECT_ID] + Math.random() + ConstantKey.COMM_CONNECTOR + ConstantKey.COMM_CONNECTOR + new Date().getTime();
            }
        }

        return this.getBusinessUUID(childInstanceObject);
    }
    
}