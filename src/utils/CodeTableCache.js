let CodeTableCache = function() {
	let codeTableMap;

	let add = function(id, codeTable) {
		codeTableMap.set(id, codeTable);
	};

	let init = function() {
		if(!codeTableMap){
			codeTableMap = new Map();
		}
	};

	let get = function(id) {
		if(!codeTableMap){
			codeTableMap = new Map();
		}
		return codeTableMap.get(id);
	};

	let getMap = function() {
		return codeTableMap;
	};

	return {
		init,
		add,
		get,
		getMap
	}
}();

module.exports = CodeTableCache;
