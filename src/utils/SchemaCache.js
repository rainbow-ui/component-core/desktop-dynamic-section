let SchemaCache = function() {
	let SchemaMap;

	let add = function(ElementId, Schema) {
		SchemaMap.set(ElementId, Schema);
	};

	let init = function() {
		if(!SchemaMap){
			SchemaMap = new Map();
		}
	};

	let get = function(ElementId) {
		return SchemaMap.get(ElementId);
	};

	let getMap = function() {
		return SchemaMap;
	};

	return {
		init,
		add,
		get,
		getMap
	}
}();

module.exports = SchemaCache;
