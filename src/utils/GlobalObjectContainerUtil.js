let GlobalObjectContainerUtil = function () {
    let init = function () {
        let globalKey = arguments[0];
        let globalValue = arguments[1];
        let simpleFlag = arguments[2];
        if (simpleFlag) {
            global[globalKey] = globalValue;
        } else {
            if (!global[globalKey]) {
                global[globalKey] = globalValue;
            }
        }
    }

    return {
        init
    }
}();

module.exports = GlobalObjectContainerUtil; 
