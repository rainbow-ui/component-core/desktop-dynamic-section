let ContainerMapUtils = function() {
    let containerMap;

    let add = function(id, container) {
        containerMap.set(id, container);
    };

    let init = function() {
        if(!containerMap){
            containerMap = new Map();
        }
    };

    let get = function(id) {
        return containerMap.get(id);
    };

    let getMap = function() {
        return containerMap;
    };

    return {
        init,
        add,
        get,
        getMap
    }
}();

module.exports = ContainerMapUtils;