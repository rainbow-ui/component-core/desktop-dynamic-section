/**
 * Created by jack.sun on 7/14/2017.
 */
let ComponentTransformationUtils = function () {
    let transformation = function(node){
        let componentcode = node.ComponentCode
        switch (componentcode) {
            case 'PANEL':
                node.ComponentCode = 'CARD';
                node.PropertyMap.styleClass = '';
                node.PropertyMap.title = node.PropertyMap.panelTitle;
                //node.PropertyMap.collapse = node.PropertyMap.toggleable;
                node.PropertyMap.collapse = true;
                node.PropertyMap.collapseIcon = true;
                delete node.PropertyMap.headerable;
                delete node.PropertyMap.panelTitle;
                delete node.PropertyMap.toggleable;
                return node;
                break;

            case 'DATA_TABLE':
                delete node.PropertyMap.headerTitle;
                break;

            case 'OBJECT_SELECTOR':
                node.ComponentCode = 'SELECTOR';
                node.PropertyMap.styleClass = '';
                node.PropertyMap.title = node.PropertyMap.panelTitle;
                delete node.PropertyMap.collapse;
                delete node.PropertyMap.collapseIcon;
                delete node.PropertyMap.panelTitle;
                break;
        }
        return node;
    }

    return{
        transformation
    };
}();

module.exports = ComponentTransformationUtils;
