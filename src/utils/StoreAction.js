import ConstantKey from '../constant/ConstantKey';
import StoreAdapter from '../utils/StoreAdapter';

module.exports = {
    
    addDispatch(modelName,objectCode,scopeObject,policy){
        let payload = {};
        payload.modelName = modelName;
        payload.objectCode = objectCode;
        payload.scopeObject = scopeObject;
        payload.policy = policy;
        StoreAdapter.addDomainObjectInstantly(payload);
    },

    saveDomainObjectDispatch(domainObject, scopeObject, policy) {
        let payload = {};
        payload.domainObject = domainObject;
        payload.scopeObject = scopeObject;
        payload.policy = policy;
        StoreAdapter.saveDomainObject(payload);
    },
    
    deleteDispatch(domainObject, scopeObject, policy){
        let payload = {};
        payload.domainObject = domainObject;
        payload.scopeObject = scopeObject;
        payload.policy = policy;
        StoreAdapter.deleteDomainObject(payload)
    },
    
    savePolicyDispatch(policy) {
        StoreAdapter.savePolicy(policy);
        let savePolicyPublishResult = PubSub.publish(ConstantKey.LISTENKEY,'success!')
        if(!savePolicyPublishResult){
            console.assert(1==0,'Save Policy Publish fail !')
        }
    }
}

