import {SessionContext, LocalContext, PageContext} from 'rainbow-desktop-cache';
import CommonAction from '../action/CommonAction';
import ConstantKey from '../constant/ConstantKey';

export default class StoreDataSource {
    constructor(){
        let allSchemaObject = [];
        let policySchemaData = {};
        this.commonAction = new CommonAction();
    }

    prepareCheck(productId){
        this.commonAction.getPolicySchema(productId);        
        /*//let allSchemaObjectKey = `${ConstantKey.STORE_ALLSCHEMAOBJ}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${productId}`
        let policySchemaDataKey = `${ConstantKey.DD_SCHEMA_DATA_KEY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${productId}`;
        
        //SessionContext.put(ConstantKey.ALLSCHEMA_KEY,allSchemaObjectKey,true);
        // PageContext.put(ConstantKey.POLICYSCHEMA_KEY,policySchemaDataKey,true);
        
        //let targetall = SessionContext.get(allSchemaObjectKey);
        let defSchema = PageContext.get(policySchemaDataKey);
        let targetpolicy;
        if(defSchema){
            let resultKey = defSchema.RootObjectId;
            targetpolicy = PageContext.get(resultKey); 
            PageContext.put(ConstantKey.POLICYSCHEMA_KEY,resultKey,true);                       
        }
        // if(!targetall){
        //     this.getAllSchemaObject(productId)
        // }
        if(!targetpolicy){
            this.getPolicySchemaData(productId)
        }*/
    }

    // getAllSchemaObject(productId) {
    //     const key = `${ConstantKey.STORE_ALLSCHEMAOBJ}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${productId}`;
    //     this.allSchemaObject = SessionContext.get(key)
    //     if(!this.allSchemaObject){
    //         this.commonAction.getPolicySchema(productId)
    //         this.allSchemaObject = SessionContext.get(key);
    //     }
    //     return this.allSchemaObject
    // }

    // getPolicySchemaData(productId){
    //     const key = `${ConstantKey.DD_SCHEMA_DATA_KEY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${ConstantKey.POLICY}${ConstantKey.COMM_CONNECTOR}${productId}`;
    //     this.policySchemaData = PageContext.get(key)
    //     if(!this.policySchemaData){
    //         this.commonAction.getPolicySchema(productId);
    //         this.policySchemaData = PageContext.get(key);
    //     }
    //     return this.policySchemaData
    // }
}