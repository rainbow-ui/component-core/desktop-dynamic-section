import ConstantKey from '../constant/ConstantKey';

export default class DynamicSectionUtil {
	findNodeRecursively(node, nodeCode) {
		if (node[ConstantKey.CODE] === nodeCode) {
			return node;
		}
		if (node[ConstantKey.NODE_LIST]) {
			let nodeList = node[ConstantKey.NODE_LIST];
			let length = nodeList.length;
			for (let i = 0; i < length; i++) {
				let node = nodeList[i];
				let foundNode = this.findNodeRecursively(node, nodeCode);
				if (foundNode) return foundNode;
			}
		}
	}
}