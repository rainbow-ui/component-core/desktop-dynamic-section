import {PolicyStore,EndorsementStore} from 'rainbow-desktop-sdk';
import Generator from './DynamicSectionGenerator';
import CommonAction from '../action/CommonAction';
import CodeTableAction from '../action/CodeTableAction';
import ModelAction from '../action/ModelAction';
import env from '../config/env';
import ContainerMapUtils from '../utils/ContainerMapUtils';
import ConstantKey from '../constant/ConstantKey';
import ConstantModel from '../constant/ConstantModel';
import AjaxUtilDs from '../ajax/AjaxUtil';
import LoadingLayer from '../element/LoadingLayer';
import StoreAdapter from '../utils/StoreAdapter';
import StoreDataSource from '../utils/StoreDataSource';
import { SessionContext } from 'rainbow-desktop-cache';
import {UIMessageHelper} from "rainbowui-desktop-core";
import PropTypes from 'prop-types';
import SchemaCache from '../utils/SchemaCache';
import GlobalObjectContainerUtil from '../utils/GlobalObjectContainerUtil';
import ConstantNode from '../constant/ConstantNode';

export default class DynamicSection extends React.Component {

	/**
	 * Get env (url config)
	 */
	getEnv() {
		if (!env.getPublicServerUrl()) {
			var config = SessionContext.get("project_config");
			//var config = AjaxUtilDs.getJSON('./config.json');
			if (config) {
				var PublicServerUrl = config["UI_API_GATEWAY_PROXY"];
				if (PublicServerUrl.charAt(PublicServerUrl.length - 1) != '/') {
					PublicServerUrl += '/';
				}
				env.setPublicServerUrl(PublicServerUrl);
			}
		}
	}

	constructor(props) {
		super(props);
		this.getEnv();
		this.commonAction = new CommonAction();
		this.codeTableAction = new CodeTableAction();
		this.modelAction = new ModelAction();
		this.storeDataSource = new StoreDataSource();
        this.state = {
            policy: {},
            cuzObject:{},
            domainObject: {},
            scopeObject: {},
            codingIndex: 0,
            pageSchema: {},
            pageScopeModelObject: {},
            pageDom: {},
			isLoading: true,
			forceEnabled:this.props.forceEnabled
        };
	}

	initSchema(){
        let schema = this.getPageSchema();
        if(schema){
            if (schema.ComponentCode == 'CELL') {
                schema.PropertyMap['type'] = 'row';
            }
            ContainerMapUtils.init();
            let policyId, policy = {};
            if (this.props.policy) {
                policy = this.props.policy;
                let techProductId = this.props.policy.TechProductId;
                this.storeDataSource.prepareCheck(techProductId);
                // this.productElement = this.commonAction.getProductElementLob(productId);
                // this.productInformation = this.commonAction.getProduct(productId);
                let productInfo = this.commonAction.getProductCodeVersion(schema);
                if(productInfo){
                    this.commonAction.getI18nDataByPageNameandContext(schema,productInfo);
                }
                // productId && (this.product = this.commonAction.getProduct(productId));
            } else {
                policyId = this.props.policyId || this.commonAction.getCurrentParameterValue('policyId');
                if (policyId) {
                    policy = this.commonAction.getEntirePolicyByPolicyId(policyId);
                }
                let productInfo = this.commonAction.getProductCodeVersion(schema);
                if (productInfo) {
                    let productId = this.commonAction.getProductId(productInfo.ProductCode, productInfo.ProductVersion);
                    this.storeDataSource.prepareCheck(productId);
                    // this.product = this.commonAction.getProduct(productId);
                    // this.productElement = this.commonAction.getProductElementLob(productId);
                    // this.productInformation = this.commonAction.getProduct(productId);
                    this.commonAction.getI18nDataByPageNameandContext(schema,productInfo);
                }
			}
			let UICTabledeleteDisable = typeof global.TableTailDeleteDisable !== 'undefined'?global.TableTailDeleteDisable:false;
			sessionStorage.setItem('TableTailDeleteDisable',UICTabledeleteDisable)
            GlobalObjectContainerUtil.init('Policy',this.props.policy,true);
			GlobalObjectContainerUtil.init('forceEnabled',this.props.forceEnabled,true);
            GlobalObjectContainerUtil.init('UIC',{});
            SchemaCache.init();
            this.state.pageSchema = schema;
            this.state.cuzObject = this.props.cuzObject;
            this.state.policy = this.props.policy;
            this.state.domainObject = this.props.policy;
			this.state.scopeObject = this.props.policy;
			UIC.dialogConfirmDelete = this.commonAction.dialogConfirmDelete;
			UIC.getTableScopeObject = this.commonAction.getTableScopeObject;
			UIC.getTargetData = this.commonAction.getTargetData;
        }
    }

	componentWillUpdate(newState) {
		_.assign(this.state.policy, newState.policy);
    }
	
	componentDidUpdate() {
		if(!_.isEmpty(this.state.policy)){
            PolicyStore.setPolicy(this.state.policy);
		}
	}
	
	componentDidMount() {
        setTimeout(() => {
			this.initSchema();
            this.setState ({
                isLoading: false
			});
		}, 100);
	}

	componentWillReceiveProps(nextProps) {
		this.props = nextProps;
		this.initSchema();
		this.setState ({
			isLoading: false,
			forceEnabled: nextProps.forceEnabled	
		});
	} 

	generatePage() {
		var context = this,
			schema = this.state.pageSchema,
        	result;
		if(Object.keys(schema).length!=0){
			// this.codeTableAction.getAllCodeTablesFromNode(schema);
			this.generateDeleteConfirmDialog(schema["NodeList"]);
            _.extend(context, Generator);
            result = Generator.generateRoot.apply(context, [schema]);	
		}
		return result;
	}

	onChangeListener(state) {
		// this.setState({policy: state.policy, domainObject: state.policy});
		_.assign(this.state.policy, state.policy);
	}

    render() {
        if (this.state.isLoading) {
			// return <div/>;
			return this.renderLoading();
        } else {
            return this.renderPage();
        }
	}
	
    renderLoading() {
        let outerStyle = {height: '600px',paddingTop:'200px'};
        return (<div><LoadingLayer outerStyle={outerStyle}/></div>);
    }
    renderPage(){
		this.state.pageDom = this.generatePage();
		return (<div>{this.state.pageDom}</div>);
	}

	getPageSchema(){
		if (this.props.schema) {
			return this.props.schema;
		}
		try {
			if (this.props.pageId) {
				return this.commonAction.getPageById(this.props.pageId);
			}
			if (this.props.pageName) {
				return this.commonAction.getPageByNameProduct(this.props.pageName, this.props.productCode, this.props.productVersion ,this.props.needTransCode);
			}
		} catch (e) {
			console.error(e);
			throw e;
		}
	}

	generateDeleteConfirmDialog(node){
		let count = 0;
		let dynamincTableDialogId = 'DeleteConfirmDialog' + this.props.productCode + this.props.pageName;
		if(node&&node.length>0){
			node.forEach(element => {
				if(element.ComponentCode=="CONFIRM_DIALOG"){
					if(element.PropertyMap&&element.PropertyMap.id==dynamincTableDialogId){
						count++;
					}
				}
			});
		}
		if(count==0){
			let dleteConfirmDialogNode = {}
			//let dleteConfirmDialogNodeEvent = {}
			dleteConfirmDialogNode[ConstantKey.COMPONENT_CODE] = ConstantNode.CONFIRM_DIALOG;
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP] = {};
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.ID] = dynamincTableDialogId;
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.TITLE] = 'studioDialogDelete';
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.CONFIRMTEXT] = 'studioDialogConfirm';
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.CANCELTEXT] = 'studioDialogCancel';
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.MESSAGE] = 'studioDialogDeleteConfirm';
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.STYLE_CLASS] = 'danger';
			dleteConfirmDialogNode[ConstantKey.PROPERTY_MAP][ConstantKey.ONCONFIRM] = "UIC.dialogConfirmDelete(UIC.targetdata,this.props.id)";
			node.push(dleteConfirmDialogNode);
		}
		return node;			
	}

	static refreshPolicy(policy) {
		PolicyStore.setPolicy(policy);
	}
};

/**
 * DynamicSection component prop types
 */
DynamicSection.propTypes = {
    cuzObject:PropTypes.object,
	schema: PropTypes.object,
	pageName: PropTypes.string,
	pageId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	productCode: PropTypes.string,
	productVersion: PropTypes.string,
	policyId: PropTypes.number,
	policy: PropTypes.object,
	codingComponent: PropTypes.object,
	codingParameter: PropTypes.object,
	id: PropTypes.string,
	modelName: PropTypes.string,
	objectCode: PropTypes.string,
	scopeModelName: PropTypes.string,
	scopeObjectCode: PropTypes.string,
	forceEnabled: PropTypes.string
};

DynamicSection.defaultProps = {
	id: 'main',
	modelName: 'Policy',
	objectCode: 'POLICY',
	scopeModelName: 'Policy',
	scopeObjectCode: 'POLICY'
};