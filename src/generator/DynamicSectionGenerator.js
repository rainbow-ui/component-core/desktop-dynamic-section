import {LocalContext} from 'rainbow-desktop-cache';
import {Util,DateUtil} from 'rainbow-desktop-tools';
import ConstantNode from '../constant/ConstantNode';
import ConstantKey from '../constant/ConstantKey';
import ContainerFactory from '../template/ContainerFactory';
import ComponentFactory from '../template/ElementFactory';
import LookupDSL from '../event/lookup/UnicornDSL';
import FreshSectionFunc from '../event/FreshSectionFunc';
import CodeTableFunc from '../event/CodeTableFunc';
import LookupModelFunc from '../event/LookUpModelFunc';
import EndorsementFunc from '../event/EndorsementFunc';
import ActionDefinition from '../event/ActionDefinition';
import {UIMessageHelper} from "rainbowui-desktop-core";
import StoreAdapter from '../utils/StoreAdapter';
import {PolicyStore,EndorsementStore} from 'rainbow-desktop-sdk';
//import {UiRuleCacheService} from 'unicorn-ui-rule-runtime';
import ComponentTransformationUtils from '../utils/ComponentTransformationUtils';
import rainbowConfig from 'config';
//var Babel = require('@babel/standalone');


module.exports = {
    //generate page
    generateRoot: function(node) {
        node = _.cloneDeep(node); 
        let children;
        let element = ComponentFactory.getComponent(node, this);
        let context = this;
        _.extend(context, element);

        let [component, props] = element.getComponent.apply(context, null);
        children = this.generateSubNode(node[ConstantKey.NODE_LIST], node);

        return React.createElement(component, props, children);
    },

    //generate sub node for container
    generateSubNode: function(nodeList, node){
        var subReactNode = [];
        if (nodeList) {
            nodeList.forEach(subNode => {
                var component = this.generateComponent(subNode, node);
                component && subReactNode.push(component);
            });
        }
        return subReactNode;
    },

    generateComponent: function(node, parent) {
        var component, props, children, element, isContainer = false;
        // if the node/component are only for property. do not process  it here.
        // if(this.state.forceEnabled && this.state.forceEnabled == 'Enabled'){
        //     node[ConstantKey.PROPERTY_MAP]["disabled"] = false;
        // }
        if(this.state.forceEnabled && this.state.forceEnabled == 'Disabled'){
            let readOnly = node[ConstantKey.PROPERTY_MAP]["ignorePageReadOnly"];
            let isReadOnly = false;
            if(readOnly){
                if (_.isString(readOnly)) {
                    let regExp = this.propertyRegExpr();
                    isReadOnly = eval('(' + readOnly.match(regExp)[1] + ')');
                }
            }
            if(!Util.parseBool(isReadOnly)){
                node[ConstantKey.PROPERTY_MAP]["disabled"] = true;
            }
        } 
        if(node[ConstantKey.COMPONENT_CODE]=='SELECT'){
            this.replaceProperty(node);
        }

        // if(node&&node.ComponentCode == 'BUTTON'&&node[ConstantKey.PROPERTY_MAP]&&node[ConstantKey.PROPERTY_MAP]["value"]){
		// 	let localLang = LocalContext.get(rainbowConfig.DEFAULT_LOCALSTORAGE_I18NKEY);
        //     let projectPathKey = buildKey(currentUrl);
        //     const i18n_cache_key = 'i18n_Cache_'+ localLang + "_" + projectPathKey;
        //     let i18nCache = LocalContext.get(i18n_cache_key);
        //     let value = node[ConstantKey.PROPERTY_MAP]["value"]
		// 		let trans = i18nCache[value];
		// 		if(trans&&trans=="新增"){
		// 			node[ConstantKey.PROPERTY_MAP]["className"] = "addFlag";
		// 		}
		// }

        if (ConstantNode.COMPONENTS_AS_SUB_COMP.indexOf(node[ConstantKey.COMPONENT_CODE]) >= 0) return;

        if (node[ConstantKey.NODE_LIST] && [ConstantKey.NODE_LIST].length) isContainer = true;
        //if (isContainer && node[ConstantKey.DATA_MODEL_NAME] && node[ConstantKey.DATA_OBJECT_CODE]) {
        if (isContainer && node[ConstantKey.DATA_MODEL_NAME] && node[ConstantKey.DATA_OBJECT_CODE]
            && ConstantNode.CONTAINER_COMPONENT.indexOf(node[ConstantKey.COMPONENT_CODE]) >= 0) {
            //兼容1.0的page
            if(node[ConstantKey.COMPONENT_CODE]=='PANEL') {
                ComponentTransformationUtils.transformation(node);
            }else if(node[ConstantKey.COMPONENT_CODE]=='DATA_TABLE'){
                ComponentTransformationUtils.transformation(node);
            }else if(node[ConstantKey.COMPONENT_CODE]=='OBJECT_SELECTOR'){
                ComponentTransformationUtils.transformation(node);
            }
            component = ContainerFactory.getContainer(node, this);
        } else if (node[ConstantKey.COMPONENT_CODE] === ConstantNode.SUB_PAGE) {
            component = ContainerFactory.getContainer(node, this);
        } else {
            //兼容1.0的page
            if(node[ConstantKey.COMPONENT_CODE]=='PANEL') {
                ComponentTransformationUtils.transformation(node);
            }else if(node[ConstantKey.COMPONENT_CODE]=='DATA_TABLE'){
                ComponentTransformationUtils.transformation(node);
            }else if(node[ConstantKey.COMPONENT_CODE]=='OBJECT_SELECTOR'){
                ComponentTransformationUtils.transformation(node);
            }
            element = ComponentFactory.getComponent(node, this);
            var context = this;
            _.extend(context, element);
            component = element.getComponent.apply(context, null);

            let tempComponent = component[0]?component[0]:component.type
            let tempProps = component[1]?component[1]:component.props

            //generate sub components
            if (isContainer) {
                if (node[ConstantKey.COMPONENT_CODE] === ConstantNode.DATA_TABLE_COLUMN) {
                    if (!tempProps.render) {
                        children = this.generateSubNode(node[ConstantKey.NODE_LIST], node);
                        if (children.length > 0) children = children[0];
                    }
                } else {
                    children = this.generateSubNode(node[ConstantKey.NODE_LIST], node);
                }
            }
            if (!tempComponent) {
                console.warn('coding component');
                return;
            }
            component = React.createElement(tempComponent, tempProps, children)
        }        
        return component;
    },

    propertyRegExpr() {
		return /^{(.*)}$/;
	},

    replaceProperty(node){
        let regExp = this.propertyRegExpr();
        let result = false;
        if(node[ConstantKey.PROPERTY_MAP]&&node[ConstantKey.PROPERTY_MAP]["multiSelect"]){
            let multiSelect = node[ConstantKey.PROPERTY_MAP]["multiSelect"];
            if (typeof (multiSelect) === 'string' && regExp.test(multiSelect)) {
                result = eval('(' + multiSelect.match(regExp)[1] + ')')
            }else {
				result = multiSelect;
			}
            delete node[ConstantKey.PROPERTY_MAP]["multiSelect"];
            node[ConstantKey.PROPERTY_MAP]["multi"] = result;
        }
    },

    generateRef(node) {
        // if (node[ConstantKey.FIELD_BINDING] && node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD]
        //     && node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME]) {
        if(node[ConstantKey.FIELD_BINDING]&&node[ConstantKey.FIELD_BINDING][ConstantKey.NAME]){
            //return node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME];
            return node[ConstantKey.FIELD_BINDING][ConstantKey.NAME]
        }
        return node[ConstantKey.CODE];
    },

    generateId(node) {
        return node[ConstantKey.CODE];
    },

    freshSection: function(node, policy){
        let eventTargetSection = node[ConstantKey.PROPERTY_MAP][ConstantKey.EVENT_TARGET_CODE];
        if (eventTargetSection) {
            let eventTargetSectionArr = JSON.parse(eventTargetSection);
            eventTargetSectionArr.length && FreshSectionFunc.refreshSection(JSON.parse(eventTargetSection), policy);
        }
    },

    callElementEvent: function(name, func){
        //var output = Babel.transform(func, { presets: ['es2015'] }).code;
        let [policy, domainObject, dialogObject, storeAdapter, messageHelper, policystore, endorsementStore] = [this.state.policy, this.state.domainObject, this.state.dialogObject, StoreAdapter, UIMessageHelper, PolicyStore, EndorsementStore],
            self = this,
            result = {},
            isEndo = () =>EndorsementFunc.isEndo(policy),
            getEndoType = () =>EndorsementFunc.getEndoType(),
            makeUpCodeTable = (codeTableName,conditionFieldArray) => CodeTableFunc.makeUpCodeTable(codeTableName,conditionFieldArray),
            buildStandardCodeTable = (orgCodeTable) => CodeTableFunc.buildStandardCodeTable(orgCodeTable),
            transformCodeTable = (codeTableInfo,conditionFieldInfo) => CodeTableFunc.transformCodeTable(codeTableInfo,conditionFieldInfo),
            getDataTableTargetByName = (dataTableName,conditionInfo) => CodeTableFunc.getDataTableTargetByName(dataTableName,conditionInfo),
            lookup = expression => LookupDSL.executeUiLookupDsl(expression, domainObject, policy),
            lookupModel = (modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject) => LookupModelFunc.lookupModel(modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject, policy),
            getCurrentTableRecord = () => LookupModelFunc.getCurrentTableRecord(this.state.policy),
            refreshSection = containerCode => FreshSectionFunc.refreshSection(containerCode, policy),
            getCodeTable = codeTableId => CodeTableFunc.getCodeTable(codeTableId),
            getCodeTableByName = (codeTableName, productId) => CodeTableFunc.getCodeTableByName(codeTableName, productId),
            callServiceRule = (eventCode, model) => UiRuleCacheService.callServiceRule(eventCode, model),
            [modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode] = [],
            [targetDialog, targetTable] = [];
        //below actions are only button
        if (this.node[ConstantKey.COMPONENT_CODE] === ConstantNode.BUTTON) {
            [targetDialog, targetTable] = [this.node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_SECTION_CODE], this.node[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_TABLE_CODE]];
            if (!this.modelObject[this.node[ConstantKey.CODE]]) this.getTargetSectionModel(targetDialog, targetTable, this.node);
            [modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode] = this.modelObject[this.node[ConstantKey.CODE]];
        }
        let CREATE = () => ActionDefinition.create(targetDialog, modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy),
            CREATE_INLINE = () => ActionDefinition.create_inline(targetDialog, targetTable, modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy, dialogObject),
            EDIT = () => ActionDefinition.edit(targetTable, targetDialog),
            DELETE = () => ActionDefinition.delete(targetDialog, targetTable, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, policy, dialogObject),
            SAVE = () => {
                if (targetDialog) {
                    return ActionDefinition.saveDialog(targetDialog, this.props.modelObject.scopeModelName, this.props.modelObject.scopeObjectCode, this.props.modelObject.parentModelName,
                        this.props.modelObject.parentObjectCode, domainObject, policy);
                } else {
                    return ActionDefinition.savePolicy(policy);
                }
            },
            CALCULATE = () => ActionDefinition.calculate(policy),
            CANCEL = () => ActionDefinition.cancel(targetDialog);
        result[name] = function(event){
            if (_.isString(func)) eval(`(function(){\n${func}\n}.bind(this)())`);
            else if (_.isFunction(func)) func.bind(this)();
            this.freshSection(this.node, policy);
        }.bind(this);
        return result;
    },

    callContainerEvent: function(func){
        //var output = Babel.transform(func, { presets: ['es2015'] }).code;
        let [policy, domainObject, dialogObject, storeAdapter, messageHelper, policystore, endorsementStore] = [this.state.policy, this.state.domainObject, this.state.dialogObject, StoreAdapter, UIMessageHelper, PolicyStore, EndorsementStore],
            isEndo = () =>EndorsementFunc.isEndo(policy),
            getEndoType = () =>EndorsementFunc.getEndoType(),
            makeUpCodeTable = (codeTableName,conditionFieldArray) => CodeTableFunc.makeUpCodeTable(codeTableName,conditionFieldArray),
            buildStandardCodeTable = (orgCodeTable) => CodeTableFunc.buildStandardCodeTable(orgCodeTable),
            transformCodeTable = (codeTableInfo,conditionFieldInfo) => CodeTableFunc.transformCodeTable(codeTableInfo,conditionFieldInfo),
            getDataTableTargetByName = (dataTableName,conditionInfo) => CodeTableFunc.getDataTableTargetByName(dataTableName,conditionInfo),
            lookup = expression => LookupDSL.executeUiLookupDsl(expression, domainObject, policy),
            lookupModel = (modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject) => LookupModelFunc.lookupModel(modelName, objectCode, scopeModelName, scopeObjectCode,nodeObject, policy),
            refreshSection = containerCode => FreshSectionFunc.refreshSection(containerCode, policy),
            getCodeTable = codeTableId => CodeTableFunc.getCodeTable(codeTableId),
            getCodeTableByName = (codeTableName, productId) => CodeTableFunc.getCodeTableByName(codeTableName, productId),
            callServiceRule = (eventCode, model) => UiRuleCacheService.callServiceRule(eventCode, model);
        eval(`(function(){\n${func}\n}.bind(this)())`);
    }
};
