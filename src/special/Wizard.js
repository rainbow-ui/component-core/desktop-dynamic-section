import SpecialComponent from './../special/SpecialComponent';
import ConstantKey from '../constant/ConstantKey';
import ConstantNode from '../constant/ConstantNode';
import BaseContainer from '../container/BaseContainer';

export default class Wizard extends SpecialComponent {

    constructor() {
        super();
    }

    processComp(node, component, context) {
        let wizardStepButtonNode;
        let length = node[ConstantKey.NODE_LIST].length;
        for (let i = 0; i < length; i++) {
            let subNode = node[ConstantKey.NODE_LIST][i];
            if (subNode[ConstantKey.COMPONENT_CODE] === ConstantNode.WIZARD_STEP_BUTTON) {
                wizardStepButtonNode = subNode;
                break;
            }
        }
        if (wizardStepButtonNode) {
            var subComp = this.createPropsComp(subNode);
            component.props.wizardStepButton = subComp;
        }
    }

    createPropsComp(node) {

        class component extends BaseContainer{
            constructor(props) {
                super(props);
            }

            generateSubComponent(node) {
                var result = [];
                node[ConstantKey.NODE_LIST].forEach(subNode => {
                    var generator = require('../generator/DynamicSectionGenerator');
                    var context = _.extend(this, generator);
                    var subComponent = generator.generateComponent.apply(context, [subNode, node]);
                    result.push(subComponent);
                });
                return result;
            }

            render() {
                var result = this.generateSubComponent(node);
                return <div>{result}</div>
            }
        };
        return component;
    }
};
