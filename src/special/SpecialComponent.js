import ConstantKey from '../constant/ConstantKey';

export default class DynamicSection {
	createPropsComp(node) {
		var result = [];
		node[ConstantKey.NODE_LIST].forEach(subNode => {
			var subComponent = this.generateComponent(subNode, node);
			result.push(subComponent);
		});
		class component extends React.Component{
			constructor(props) {
				super(props);
			}

			render() {
				return <div>{result}</div>
			}
		}
		return component;
	}

};