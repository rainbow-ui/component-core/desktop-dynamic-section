import BaseContainer from "./BaseContainer";
import SpecialWizard from "../special/Wizard";

export default class Wizard extends BaseContainer {

    constructor(props) {
        super(props);
    }

    generateContainer(node) {
        var context = this;
        var Generator = require('../generator/DynamicSectionGenerator');
        _.extend(context, Generator);
        var component = Generator.generateRoot.apply(context, [node]);
        var specialWizard = new SpecialWizard();
        specialWizard.processComp(node, component, this);
        return component;
    }
};
