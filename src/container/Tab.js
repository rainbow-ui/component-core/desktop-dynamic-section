import BaseContainer from './BaseContainer';
import ConstantKey from '../constant/ConstantKey';
import ConstantNode from '../constant/ConstantNode';

export default class Tab extends BaseContainer {

    constructor(props) {
        super(props);
        this.bindValue();
    }

    onChangeListener(state) {
        let policy = this.state.policy;
        try {
            if (this.props.modelObject && this.state.isDialog !== true) {
                let [domainObject,scopeObject] = this.generateDomainObject(this.props.modelObject, policy, this.props.parentObj);
                this.setState({policy: policy, domainObject: domainObject, scopeObject: scopeObject});
                this.bindValue();
            }
        } catch(ex) {
            console.error(`Error in calling generateDomainObject. node:${this.props.node.Code}`);
            throw ex;
        }
    }

    bindValue() {
        let node = this.props.node;
        if (!node[ConstantKey.FIELD_BINDING]) {
            return;
        }
        this.fieldBinding = node[ConstantKey.FIELD_BINDING][ConstantKey.BUSINESS_FIELD][ConstantKey.NAME];
        let subNodes = node[ConstantKey.NODE_LIST];
        if (!subNodes) return;

        let tabItemsList = subNodes.filter((tmpNode) => {
            return tmpNode[ConstantKey.COMPONENT_CODE] === ConstantNode.TAB_ITEM;
        });
        if (!tabItemsList || !tabItemsList.length) return;
        this.state.property = {tabItemList: tabItemsList};

        //if tab field is null, set defaultValue
        if (this.state.domainObject[this.fieldBinding] == null && node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE] != null) {
            this.state.domainObject[this.fieldBinding] = node[ConstantKey.PROPERTY_MAP][ConstantKey.DEFAULT_VALUE];
        }

        let selectedIndex = tabItemsList.findIndex(tabItem => {
            let tabValue = tabItem[ConstantKey.PROPERTY_MAP][ConstantKey.VALUE];
            return tabValue === this.state.domainObject[this.fieldBinding];
        });
        if (selectedIndex < 0) return;

        _.assign(this.state.property, {tabIndex: selectedIndex + 1});
    }
};