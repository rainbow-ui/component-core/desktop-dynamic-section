import CommonAction from '../action/CommonAction';
import BaseContainer from './BaseContainer';
import ConstantKey from '../constant/ConstantKey';

export default class SubPage extends BaseContainer {

    constructor(props) {
        super(props);

        this.commonAction = new CommonAction();

        let {productCode, pageName} = this.props.node[ConstantKey.PROPERTY_MAP];

        let pageSchema = this.getSubPage(productCode, pageName);
        this.node = pageSchema;
        this.state.pageSchema = pageSchema;
        this.state.pageScopeModelObject = {scopeModelName: this.props.modelObject.scopeModelName, scopeObjectCode: this.props.modelObject.scopeObjectCode};
    }

    getSubPage(productCode, pageName) {
        let product = this.commonAction.getCurrentProductByProductCode(productCode);
        let productVersion = product.ProductVersion;
        let pageSchema = this.commonAction.getPageByNameProduct(pageName, productCode, productVersion);
        return pageSchema;
    }

    render() {
        let {Policy, DomainObject} = this.state;
        super.render();
        return (
            this.generateContainer(this.state.pageSchema)
        );
    }
};
