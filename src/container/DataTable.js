import {UIDataTable,UIConfirmDialog} from 'rainbowui-desktop-core';
import BaseContainer from './BaseContainer';
import ButtonElement from '../element/Button';
import ConstantKey from '../constant/ConstantKey';
import ConstantNode from '../constant/ConstantNode';
import StoreAdapter from '../utils/StoreAdapter';
import EndorsementFunc from '../event/EndorsementFunc';

export default class DataTable extends BaseContainer {
	constructor(props) {
		super(props);
		this.getSelectedRecord = this.getSelectedRecord;
		// if(this.state.domainObject instanceof Object){
		// 	this.state.domainObjectList = [this.state.domainObject]; 
		// }else{
			// this.state.domainObjectList = this.state.domainObject; 
			// this.state.domainObject = this.props.cuzObjectList?this.props.cuzObjectList:[];
			this.state.domainObjectList = this.props.cuzObjectList?this.props.cuzObjectList:[];		
			//}
		//domainObjectList is required by unicorn-ui-rule-runtime
		this.state.hasCuzTemplate = false;
	}

	componentDidMount(){
		//nextState = this.state;
		let policy = this.state.policy;
		let domainObject = this.state.domainObject;
		if(Array.isArray(domainObject)){
		let count = 0
		domainObject.forEach((data)=>{
			if(data && (data.PolicyStatus == 4 || data.PolicyStatus == 3)){
				domainObject[count].operationStatus = "deleteRow";
			}else if(data&&data.PolicyStatus == 1){
				domainObject[count].operationStatus = "addRow";
			}
			count++;
		})
		this.setState({
			policy: policy,
			domainObject: domainObject,
			domainObjectList: domainObject
		})
		}
	}

	componentWillReceiveProps(nextProps) {
	//this.props = nextProps;
	var policy = nextProps.policy;
	try {
		if(nextProps.node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&nextProps.node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
			let domainObject = nextProps.cuzObjectList?nextProps.cuzObjectList:[];			
			//let domainObjectList = nextProps.cuzObjectList;
			let hasCuzTemplate = false
			if(nextProps&&nextProps.cuzObjectTemplate){
				hasCuzTemplate = true;
				let parentObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(nextProps.cuzObjectTemplate.parentModelName,nextProps.cuzObjectTemplate.parentObjectCode,policy,
					nextProps.cuzObjectTemplate.scopeModelName,nextProps.cuzObjectTemplate.scopeObjectCode)
				domainObject = StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(nextProps.cuzObjectTemplate.modelName,nextProps.cuzObjectTemplate.objectCode,parentObject)
				nextProps.cuzObjectList = domainObject;
				nextProps.cuzObject = domainObject;
			}
			this.state.domainObject = domainObject;
			this.state.domainObjectList = domainObject;
			if(Array.isArray(domainObject)){
				let count = 0
			domainObject.forEach((data)=>{
				if(data && (data.PolicyStatus == 4 || data.PolicyStatus == 3)){
					domainObject[count].operationStatus = "deleteRow";
				}else if(data&&data.PolicyStatus == 1){
					domainObject[count].operationStatus = "addRow";
				}
				count++;
			})
			}
			this.setState({
				policy: policy,
				domainObject: domainObject,
				domainObjectList: domainObject,
				hasCuzTemplate:hasCuzTemplate
			})
		}else{
			if (nextProps.modelObject && this.state.isDialog !== true) {
				let [domainObject, scopeObject] = this.generateDomainObject(nextProps.modelObject, policy, nextProps.parentObj);
				this.state.domainObject = domainObject;
				this.state.domainObjectList = domainObject;
				if(Array.isArray(domainObject)){
					let count = 0
				domainObject.forEach((data)=>{
					if(data && (data.PolicyStatus == 4 || data.PolicyStatus == 3)){
						domainObject[count].operationStatus = "deleteRow";
					}else if(data&&data.PolicyStatus == 1){
						domainObject[count].operationStatus = "addRow";
					}
					count++;
				})
				}
				this.setState({
					policy: policy,
					domainObject: domainObject,
					domainObjectList: domainObject,
					scopeObject: scopeObject
				});
			} else if (nextProps.modelObject && this.state.isDialog === true) {
				let [domainObject, scopeObject] = this.generateDomainObject(nextProps.modelObject, nextProps.dialogObject, nextProps.parentObj);
				this.setState({
					policy: policy,
					domainObject: domainObject,
					domainObjectList: domainObject,
					scopeObject: scopeObject
				});
			}
		}
	} catch(ex) {
		console.error(`Error in calling generateDomainObject. node:${nextProps.node.Code}`);
		throw ex;
	}
	}

	onChangeListener(state) {
		var policy = this.state.policy;
		try {
			if(this.props.node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&this.props.node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
				let domainObject =  this.props.cuzObjectList? this.props.cuzObjectList:[];
				//let domainObjectList = this.props.cuzObjectList;
				if(this.props&&this.props.cuzObjectTemplate){
					let parentObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(this.props.cuzObjectTemplate.parentModelName,this.props.cuzObjectTemplate.parentObjectCode,policy,
						this.props.cuzObjectTemplate.scopeModelName,this.props.cuzObjectTemplate.scopeObjectCode)
					domainObject = StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(this.props.cuzObjectTemplate.modelName,this.props.cuzObjectTemplate.objectCode,parentObject)
					this.props.cuzObjectList = domainObject;
					this.props.cuzObject = domainObject;
				}
				this.state.domainObject = domainObject;
				this.state.domainObjectList = domainObject;
				if(Array.isArray(domainObject)){
					let count = 0
				domainObject.forEach((data)=>{
					if(data && (data.PolicyStatus == 4 || data.PolicyStatus == 3)){
						domainObject[count].operationStatus = "deleteRow";
					}else if(data&&data.PolicyStatus == 1){
						domainObject[count].operationStatus = "addRow";
					}
					count++;
				})
				}
				this.setState({
                    policy: policy,
                    domainObject: domainObject,
                    domainObjectList: domainObject
				})
			}else{
                if (this.props.modelObject && this.state.isDialog !== true) {
                    let [domainObject, scopeObject] = this.generateDomainObject(this.props.modelObject, policy, this.props.parentObj);
                    this.state.domainObject = domainObject;
					this.state.domainObjectList = domainObject;
					if(Array.isArray(domainObject)){
						let count = 0
					domainObject.forEach((data)=>{
						if(data && (data.PolicyStatus == 4 || data.PolicyStatus == 3)){
							domainObject[count].operationStatus = "deleteRow";
						}else if(data&&data.PolicyStatus == 1){
							domainObject[count].operationStatus = "addRow";
						}
						count++;
					})
					}
                    this.setState({
                        policy: policy,
                        domainObject: domainObject,
                        domainObjectList: domainObject,
                        scopeObject: scopeObject
                    });
                } else if (this.props.modelObject && this.state.isDialog === true) {
                    let [domainObject, scopeObject] = this.generateDomainObject(this.props.modelObject, this.props.dialogObject, this.props.parentObj);
                    this.setState({
                        policy: policy,
                        domainObject: domainObject,
                        domainObjectList: domainObject,
                        scopeObject: scopeObject
                    });
                }
			}
		} catch(ex) {
			console.error(`Error in calling generateDomainObject. node:${this.props.node.Code}`);
			throw ex;
		}
	}

	generateDomainObject({modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode}, policy, parentObj) {
		let domainObject, scopeObject, nodeObject;
		nodeObject = this.props.node;
		try {
			scopeObject = this.getScopeObject(scopeModelName, scopeObjectCode, policy,nodeObject);
			let scopeObjectSchema = StoreAdapter.getBusinessObjectByObjectId(scopeObject.BusinessObjectId, scopeObject);
			if (!parentModelName || !parentObjectCode) {
				//domainObject = PolicyStore.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCode, scopeObject, scopeObject);
                domainObject = StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCode, scopeObject, scopeObject);
			} else {
				//let parentObjTmp = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy, scopeObjectSchema.modelName, scopeObjectSchema.objectCode);
                let parentObjTmp = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy, scopeObjectSchema.ModelName, scopeObjectSchema.ObjectCode,nodeObject);
				//domainObject = PolicyStore.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCode, parentObjTmp, scopeObject);
                domainObject = StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(modelName, objectCode, parentObjTmp, scopeObject);
			}
		} catch(ex) {
			console.error(`Error in getting domain object by model and code. modelName: ${modelName}, objectCode: ${objectCode}, policy: ${JSON.stringify(policy)}`);
			throw ex;
		}
		return [domainObject, scopeObject];
	}

	generateContainer(node) {
		var context = this;
		var Generator = require('../generator/DynamicSectionGenerator');
		_.extend(context, Generator);
        //set tail delete
        this.generateTailDelete(node)
		var component =  Generator.generateRoot.apply(context, [node]);
        _.extend(component.props, {value: this.state.domainObjectList});
		//set functions property
		var functionsProp = this.generateFunctionsProp(node);
		if (functionsProp) component.props.functions = functionsProp;
		return component;
	}

	generateFunctionsProp(node){
		var functionsProp;
		let length = node[ConstantKey.NODE_LIST].length;
		for (let i = 0; i < length; i++) {
			let subNode = node[ConstantKey.NODE_LIST][i];
			if (subNode[ConstantKey.COMPONENT_CODE] === ConstantNode.DATA_TABLE_FUNCTION) {
				functionsProp = this.generateSubFunctions(subNode);
				break;
			}
		}
		if (functionsProp) {
			return <div>{functionsProp}</div>
		}
	}

	generateSubFunctions(node) {
		var result = [];
		node[ConstantKey.NODE_LIST].forEach(subNode => {
			var component = this.generateComponent(subNode, node);
			component && result.push(component);
		});
		return result;
	}

	generateTailDelete(node){
		if(node[ConstantKey.PROPERTY_MAP][ConstantKey.TABLE_TAIL_DELETE]&&!node[ConstantKey.TABLE_TAIL_DELETE_FLAG]) {
            let tempTableColumnNode = {}
            let tempTableOperateNode = {}
            let tempTableOperateNodeEvent = {}

            tempTableColumnNode[ConstantKey.COMPONENT_CODE] = ConstantNode.DATA_TABLE_COLUMN
			tempTableColumnNode[ConstantKey.PROPERTY_MAP] = {}
			tempTableColumnNode[ConstantKey.PROPERTY_MAP][ConstantKey.HEADER_TITLE] = "TableTailDelete"
            tempTableColumnNode[ConstantKey.NODE_LIST] = []

            tempTableOperateNode[ConstantKey.COMPONENT_CODE] = ConstantNode.BUTTON
            tempTableOperateNode[ConstantKey.PROPERTY_MAP] = {}
            tempTableOperateNode[ConstantKey.PROPERTY_MAP][ConstantKey.TARGET_TABLE_CODE] = node[ConstantKey.CODE]
            tempTableOperateNode[ConstantKey.PROPERTY_MAP][ConstantKey.CSS_STYLE]="{{\"border\":\"none\",\"background-color\":\"transparent\"}}"
			let TableTailDeleteDisable = sessionStorage.getItem('TableTailDeleteDisable')
			if(TableTailDeleteDisable !== null){
				tempTableOperateNode[ConstantKey.PROPERTY_MAP].disabled = JSON.parse(TableTailDeleteDisable) 
			}
			//  if(!_.isEmpty(node[ConstantKey.PROPERTY_MAP][ConstantKey.TABLE_ICON_CONFIG])){
            //      tempTableOperateNode[ConstantKey.PROPERTY_MAP][ConstantKey.CSS_ICON]=node[ConstantKey.PROPERTY_MAP][ConstantKey.TABLE_ICON_CONFIG];
            //  }else{
                tempTableOperateNode[ConstantKey.PROPERTY_MAP][ConstantKey.CSS_ICON]="rainbow Trash"
            //}
            tempTableOperateNode[ConstantKey.EVENT_LIST]=[]
            tempTableOperateNodeEvent[ConstantKey.EVENT_CODE_KEY]=ConstantKey.EVENT_ON_CLICK;
			UIC.CustomActionEvent = null;
			if(node[ConstantKey.EVENT_LIST]){
				let tempEvent = {}
				let isCuzObject = node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&!this.state.hasCuzTemplate ? true : false;
                node[ConstantKey.EVENT_LIST].forEach(function (eachEvent) {
					if(eachEvent[ConstantKey.EVENT_CODE_KEY]==ConstantKey.EVENT_TABLE_TAIL_DELETE){
                        tempEvent[ConstantKey.EVENT_CODE_KEY]=ConstantKey.EVENT_ON_CLICK
                        if(eachEvent[ConstantKey.EVENT_ACTION_KEY]&&eachEvent[ConstantKey.EVENT_ACTION_KEY]=="DELETE"){
							if(isCuzObject){
								tempEvent[ConstantKey.EVENT_ACTION_KEY]=eachEvent[ConstantKey.EVENT_ACTION_KEY]
							}else{
								tempEvent[ConstantKey.EVENT_ACTION_KEY]="let aimdata=event.component.props.model;UIC.targetdata=aimdata;UIC.getTableScopeObject(this.props.id,this.node.ScopeModelName,this.node.ScopeObjectCode,this.node.ParentModelName,this.node.ParentObjectCode,Policy);this.onClickDangerShow(this.props.productCode, this.props.pageName);"
							}						
						}else if(eachEvent[ConstantKey.EVENT_ACTION_KEY]&&eachEvent[ConstantKey.EVENT_ACTION_KEY]!="DELETE"&&eachEvent[ConstantKey.EVENT_ACTION_KEY]!="EDIT"){
							// UIC.CustomActionEvent = eachEvent[ConstantKey.EVENT_ACTION_KEY];
							tempEvent[ConstantKey.EVENT_ACTION_KEY]="let aimdata=event.component.props.model;UIC.targetdata=aimdata;UIC.getTableScopeObject(this.props.id, this.props.node.ScopeModelName, this.props.node.ScopeObjectCode, this.props.node.ParentModelName, this.props.node.ParentObjectCode, Policy); if(this.props.node&&this.props.node.EventList&&this.props.node.EventList.length>0){this.props.node.EventList.forEach(function (eachEvent) {if(eachEvent && eachEvent.EventCode=='tableTailDelete'){ UIC.CustomActionEvent = eachEvent.CustomizedAction } })}if(UIC.isStudioDeleteDialog != true){this.onClickDangerShow(this.props.productCode, this.props.pageName) };";
						}else if(eachEvent[ConstantKey.EVENT_ACTION_KEY]&&eachEvent[ConstantKey.EVENT_ACTION_KEY]=="EDIT"){
							tempEvent[ConstantKey.EVENT_ACTION_KEY]=eachEvent[ConstantKey.EVENT_ACTION_KEY]
						}
					}
                })
				if(tempEvent[ConstantKey.EVENT_ACTION_KEY]){
                    tempTableOperateNode[ConstantKey.EVENT_LIST].push(tempEvent)
				}
			}

            tempTableColumnNode[ConstantKey.NODE_LIST].push(tempTableOperateNode)

			node[ConstantKey.TABLE_TAIL_DELETE_FLAG]=true
			node[ConstantKey.NODE_LIST].push(tempTableColumnNode);
        }
	}

	onClickDangerShow(productCode,pageName){
		//UIConfirmDialog.addData('DeleteConfirmDialog01',targetdata)
		UIConfirmDialog.show('DeleteConfirmDialog' + productCode + pageName)
	}

	getSelectedRecord() {
		var node = this.props.node;
		return UIDataTable.getSelectedRecord(this.generateId(node));
	} 
};