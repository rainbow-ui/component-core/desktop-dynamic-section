import {UIDialog} from 'rainbowui-desktop-core';
//import {PolicyStore} from "unicorn-ui-policy-logic"
import BaseContainer from './BaseContainer';
import StoreAdapter from '../utils/StoreAdapter';

export default class Dialog extends BaseContainer {

	constructor(props) {
		super(props);
		this.state.isDialog = true;
		this.state.dialogModelObject = this.props.modelObject;
		this.state.pageScopeModelObject = {
			scopeModelName: this.props.modelObject.modelName,
			scopeObjectCode: this.props.modelObject.objectCode
		};
	}

	componentWillMount() {
		//PolicyStore.listen(this.onChangeListener);
		StoreAdapter.listen(this.onChangeListener);
		this.eventList.componentWillMount && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentWillMount);
	}

	componentDidMount() {
		this.eventList.componentDidMount && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentDidMount);
	}

	componentWillUpdate() {
		this.eventList.componentWillUpdate && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentWillUpdate);
	}

	componentDidUpdate() {
		this.eventList.componentDidUpdate && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentDidUpdate);
	}

	componentWillUnmount() {
		//PolicyStore.unlisten(this.onChangeListener);
		StoreAdapter.unlisten(this.onChangeListener)
		this.eventList.componentWillUnmount && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentWillUnmount);
	}

	onChangeListener(state) {
		var policy = this.state.policy;
		this.setState({policy: policy});
	}

	showDialog(domainObject) {
		this.setState({domainObject: domainObject, dialogObject: domainObject});
		UIDialog.show(this.generateId(this.props.node));
	}

	hideDialog() {
		UIDialog.hide(this.generateId(this.props.node));
	}

	generateDomainObject({modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode}, policy, parentObj) {
		let domainObject, scopeObject;
		//scopeObject = PolicyStore.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
        scopeObject = StoreAdapter.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
		return [domainObject, scopeObject];
	}
};