import CommonAction from '../action/CommonAction';
import CodeTableAction from '../action/CodeTableAction';
import ModelAction from '../action/ModelAction';
import PolicyAction from '../action/PolicyAction';
import ConstantKey from '../constant/ConstantKey';
import ContainerMapUtils from '../utils/ContainerMapUtils';
import StoreAdapter from '../utils/StoreAdapter';
import StoreAction from '../utils/StoreAction';
import {PolicyStore} from 'rainbow-desktop-sdk';
import PropTypes from 'prop-types';
import {UIMessageHelper} from "rainbowui-desktop-core";
import ConstantNode from '../constant/ConstantNode';

export default class BaseContainer extends React.Component {

	constructor(props) {
		super(props);
		this.onChangeListener = this.onChangeListener.bind(this);
		let {policy, modelObject, parentObj, isDialog, dialogObject, pageScopeModelObject} = this.props;
		let domainObject = {}, scopeObject = {};
		try {
            if (isDialog === true && !_.isEmpty(dialogObject)) {
                [domainObject, scopeObject] = this.generateDomainObject(modelObject, dialogObject, parentObj);
            } else if(this.props.node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&this.props.node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
            	domainObject = this.props.cuzObject
			}else if (!_.isEmpty(policy)) {
                // PolicyStore.refreshPolicyData(policy);
                PolicyStore.setPolicy(policy);
                [domainObject, scopeObject] = this.generateDomainObject(modelObject, policy, parentObj);
            }
		} catch(ex) {
			console.error(`Error in calling generateDomainObject. node:${this.props.node.Code}`);
			throw ex;
		}

		this.commonAction = new CommonAction();
		this.codeTableAction = new CodeTableAction();
		this.modelAction = new ModelAction();
		this.policyAction = new PolicyAction();

		this.getDomainObject = this.getDomainObject;
		this.eventList = this.commonAction.covertContainerEvents(this.props.node[ConstantKey.EVENT_LIST]);

		ContainerMapUtils.add(this.props.node[ConstantKey.CODE], this);

		this.state = {
			policy: policy,
			domainObject: domainObject,
			scopeObject: scopeObject,
			pageSchema: this.props.pageSchema,
			isDialog: this.props.isDialog,
			dialogObject: dialogObject,
			dialogModelObject: this.props.dialogModelObject,
			pageScopeModelObject: pageScopeModelObject || {},
			forceEnabled:forceEnabled?forceEnabled:null
		};
	}

	componentWillMount() {
		//PolicyStore.listen(this.onChangeListener);
        StoreAdapter.listen(this.onChangeListener);
		this.eventList.componentWillMount && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentWillMount);
	}

	componentDidMount() {
		this.eventList.componentDidMount && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentDidMount);
	}

	componentWillUpdate() {
		this.reLocateDomainObject(arguments[0],arguments[1])
		this.eventList.componentWillUpdate && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentWillUpdate);
	}

	componentDidUpdate() {
		this.eventList.componentDidUpdate && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentDidUpdate);
	}

	componentWillUnmount() {
		//PolicyStore.unlisten(this.onChangeListener);
        StoreAdapter.unlisten(this.onChangeListener);
		this.eventList.componentWillUnmount && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.componentWillUnmount);
	}

	componentWillReceiveProps(nextProps) {
		//this.props = nextProps;
		let policy = nextProps.policy
		let domainObject = this.state.domainObject
		let scopeObject = this.state.scopeObject
		if(nextProps.node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&nextProps.node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
			if(nextProps.node[ConstantKey.COMPONENT_CODE]==ConstantNode.DATA_TABLE){
				if(nextProps&&nextProps.cuzObjectTemplate){
					let parentObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(nextProps.cuzObjectTemplate.parentModelName,nextProps.cuzObjectTemplate.parentObjectCode,policy,
						nextProps.cuzObjectTemplate.scopeModelName,nextProps.cuzObjectTemplate.scopeObjectCode)
					domainObject = StoreAdapter.getAllDomainObjectByModelNameAndObjectCodeUnderParentObjectInScopeObject(nextProps.cuzObjectTemplate.modelName,nextProps.cuzObjectTemplate.objectCode,parentObject)
					nextProps.cuzObjectList = domainObject;
					nextProps.cuzObject = domainObject;
				}
				domainObject = nextProps.cuzObjectList;
			}
			this.state.domainObject = domainObject;
			this.state.domainObjectList = domainObject;
		}
		this.setState ({policy: policy, domainObject: domainObject, scopeObject: scopeObject,forceEnabled:forceEnabled});
	} 

	reLocateDomainObject(newProps,newState){
		if(newProps.node&&newProps.node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&newProps.node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
			newState[ConstantKey.DOMAIN_OBJECT]=newProps.cuzObject
			newState.domainObjectList = newProps.cuzObjectList
		}else{
			if(Array.isArray(this.generateDomainObject(newProps.modelObject,newState.policy)[0])){
				newState[ConstantKey.DOMAIN_OBJECT_LIST]=this.generateDomainObject(newProps.modelObject,newState.policy)[0]
				newState[ConstantKey.DOMAIN_OBJECT]=this.generateDomainObject(newProps.modelObject,newState.policy)[0]
			}else{
				newState[ConstantKey.DOMAIN_OBJECT]=this.generateDomainObject(newProps.modelObject,newState.policy)[0]
			}
		}   
		// if(newProps.modelObject&&newState.policy&&(!(newProps.cuzObject&&newProps.cuzObject["@type"])||newProps.cuzObject["@type"]!=newProps.modelObject["@type"])){
		// 	if(Array.isArray(this.generateDomainObject(newProps.modelObject,newState.policy)[0])){
		// 	   newState[ConstantKey.DOMAIN_OBJECT_LIST]=this.generateDomainObject(newProps.modelObject,newState.policy)[0]
		// 	   newState[ConstantKey.DOMAIN_OBJECT]=this.generateDomainObject(newProps.modelObject,newState.policy)[0]
		// 	}else{
		// 	   newState[ConstantKey.DOMAIN_OBJECT]=this.generateDomainObject(newProps.modelObject,newState.policy)[0]
		// 	}
		//  }else if(newProps.modelObject&&newProps.cuzObject&&(!newState.policy||newProps.cuzObject["@type"]==newProps.modelObject["@type"])){
		// 	   newState[ConstantKey.DOMAIN_OBJECT]=newProps.cuzObject
		//    }      
	}

	onChangeListener(state) {
		//let policy = state.policy, domainObject, scopeObject = {};
		let policy = this.state.policy
		let domainObject = this.state.domainObject
		let scopeObject = this.state.scopeObject
		try {
			if(this.props.node[ConstantKey.DATA_MODEL_NAME]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION&&this.props.node[ConstantKey.DATA_OBJECT_CODE]==ConstantKey.DATA_MODEL_NAME_CUSTOMIZATION){
                domainObject = this.props.cuzObject
				scopeObject = this.props.cuzObject
				this.setState({policy: policy, domainObject: domainObject, scopeObject: scopeObject});
			}else{
                if (this.props.modelObject && this.state.isDialog !== true) {
                    [domainObject, scopeObject] = this.generateDomainObject(this.props.modelObject, policy, this.props.parentObj);
                    this.setState({policy: policy, domainObject: domainObject, scopeObject: scopeObject});
                    //this.setState({policy: policy});
                } else if (this.props.modelObject && this.state.isDialog === true) {
                    [domainObject, scopeObject] = this.generateDomainObject(this.props.modelObject, this.props.dialogObject, this.props.parentObj);
                    this.setState({policy: policy, domainObject: domainObject, scopeObject: scopeObject});
                    //this.setState({policy: policy});
                }
			}
		} catch(ex) {
			console.error(`Error in calling generateDomainObject. node:${this.props.node.Code}`);
			throw ex;
		}
	}

	generateContainer(node) {
		var context = this;
		var Generator = require('../generator/DynamicSectionGenerator');
		_.extend(context, Generator);
		return Generator.generateRoot.apply(context, [node]);
	}

	generateDomainObject({modelName, objectCode, scopeModelName, scopeObjectCode, parentModelName, parentObjectCode, dataPath}, policy, parentObj) {
		let domainObject, scopeObject,nodeObject;
		nodeObject = this.props.node;
		try {
			//scopeObject = PolicyStore.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
			scopeObject = StoreAdapter.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy,nodeObject);
			if (!parentModelName && !parentObjectCode) {
				//domainObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
                
				domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode,nodeObject);
			} else {
				if (parentModelName === this.props.pageSchema[ConstantKey.DATA_MODEL_NAME]) {
					//domainObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode);
                    
					domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode,nodeObject);
				} else {
					//let scopeObjectSchema = PolicySchemaStore.getBusinessObjectByObjectId(scopeObject.BusinessObjectId, scopeObject);
                    let scopeObjectSchema = StoreAdapter.getBusinessObjectByObjectId(scopeObject.BusinessObjectId, scopeObject);
					//let parentObjTmp = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy, scopeObjectSchema.modelName, scopeObjectSchema.objectCode);
                    let parentObjTmp = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(parentModelName, parentObjectCode, policy, scopeObjectSchema.ModelName, scopeObjectSchema.ObjectCode,nodeObject);
					//domainObject = PolicyStore.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode, parentObjTmp);
                    
					domainObject = StoreAdapter.getDomainObjectByModelAndCodeAlwaysInComponentState(modelName, objectCode, policy, scopeModelName, scopeObjectCode,nodeObject,parentObjTmp);
				}
			}
		} catch(ex) {
			console.error(`Error in getting domain object by model and code. modelName: ${modelName}, objectCode: ${objectCode}, scopeModelName: ${scopeModelName}, 
				scopeObjectCode: ${scopeObjectCode}, parentModelName: ${parentModelName}, parentObjectCode: ${parentObjectCode}, policy: ${JSON.stringify(policy)}`);
			throw ex;
		}
		return [domainObject, scopeObject];
	}

	getDomainObject() {
		return this.state.domainObject;
	}

	render() {
		//var dom = this.generateContainer(this.props.schema);
		this.eventList.onStateChange && require('../generator/DynamicSectionGenerator').callContainerEvent.call(this, this.eventList.onStateChange);
		return (
			this.generateContainer(this.props.node)
		);
	}

	getScopeObject(scopeModelName, scopeObjectCode, policy, nodeObject) {
		if (!scopeModelName || !scopeObjectCode) return policy;
		try {
			//let scopeObject = PolicyStore.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy);
			let scopeObject = StoreAdapter.locateScopeObjectInComponentPolicy(scopeModelName, scopeObjectCode, policy, nodeObject);
			return scopeObject;
		} catch(ex) {
			console.error(`Error when locate scope object. scopeModelName: ${scopeModelName}, scopeObjectCode: ${scopeObjectCode}`);
			throw ex;
		}
	}
};

BaseContainer.propTypes = {
	modelObject: PropTypes.object,
	policy: PropTypes.object,
	node: PropTypes.object,
	modelName: PropTypes.string,
	objectCode: PropTypes.string,
	scopeModelName: PropTypes.string,
	scopeObjectCode: PropTypes.string
};

BaseContainer.defaultProps = {
	id: 'main',
	modelName: 'Policy',
	objectCode: 'POLICY',
	scopeModelName: 'Policy',
	scopeObjectCode: 'POLICY'
};
