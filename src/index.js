'use strict';

module.exports = {
	DynamicSection: require('./generator/DynamicSection'),
	CommonAction: require('./action/CommonAction'),
	DynamicSectionEnv: require('./config/env'),
	CodeTableFunc: require('./event/CodeTableFunc')
};