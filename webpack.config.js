'use strict';

  var webpack = require('webpack'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
  path = require('path'),
  srcPath = path.join(__dirname, 'src');

  var ExtractTextPlugin = require("extract-text-webpack-plugin");
  var CopyWebpackPlugin=require('copy-webpack-plugin');
  var name = require('./package.json').name;
  var version = require('./package.json').version;
  var outputName = name+"-"+version;
  var resolveNpmPath = function(componentPath) {
  	return path.join(__dirname, 'node_modules', componentPath);
  };

module.exports = {
  target: 'web',
  cache: true,
  entry: {
    index: path.join(srcPath, 'index.js'),
  },

  resolve: {
    root: srcPath,
    extensions: ['', '.js', '.jsx', '.css'],
    alias: {
      react: resolveNpmPath('/react/dist/react.min.js'),
      underscore: resolveNpmPath('/underscore/underscore-min.js'),
      jquery: resolveNpmPath('/jquery/dist/jquery.min.js'),
      reactWithAddons: resolveNpmPath('/react/dist/react-with-addons.min.js')
    }
  },

  output: {
    path: path.join(__dirname, name),
    publicPath: '',
    filename: outputName+'.js',
    pathInfo: true
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        include:[
          path.resolve(__dirname,"src"),
          path.resolve(__dirname,"node_modules/unicorn-ui-basic-logic"),
		  path.resolve(__dirname,"node_modules/unicorn-ui-policy-logic"),
          path.resolve(__dirname,"node_modules/unicorn-ui-rule-runtime")],
        loaders: ["babel-loader"]
      },
      {
        test: /\.jsx$/, loaders: ['babel-loader']
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract("css-loader"),
      },
        { test: /\.gif/, loader: "file-loader" },
        { test: /\.jpg/, loader: "file-loader" },
        { test: /\.png/, loader: "file-loader" },
        { test: /\.woff2/,  loader: "file-loader" },
        { test: /\.woff/,   loader: "file-loader" },
        { test: /\.ttf/,    loader: "file-loader" },
        { test: /\.eot/,    loader: "file-loader" },
        { test: /\.svg/,    loader: "file-loader" }
    ]
  },



  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: 'RainbowUIConfig/template/index.html'
    }),
    new CopyWebpackPlugin([
      {from:"./src/html5shiv.js",to:"html5shiv.js"},
      {from:"./RainbowUIConfig/project_config.json",to:"project_config.json"},
      {from:"./RainbowUIConfig/template/login.js",to:"login.js"},
      {from:"./RainbowUIConfig/server.json",to:"server.json"},
      {from:"./RainbowUIConfig/template/jquery.min.js",to:"jquery.min.js"}
    ]),
    new ExtractTextPlugin(outputName+".css"),
    new webpack.NoErrorsPlugin(),
    new webpack.ProvidePlugin({
            React: 'react',
            $: 'jquery',
            jQuery: 'jquery',
             _: 'underscore',
            ReactWithAddons: 'reactWithAddons'
    })
  ],
  debug: true,
  devtool: "eval-cheap-module-source-map",
  devServer: {
    contentBase: '',
    historyApiFallback: true,
    host: 'localhost'
  }
};
